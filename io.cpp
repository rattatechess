/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Dom Oct 9 2005
    copyright            : (C) 2005 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdarg.h>
#include <string.h>
#include "engine.h"
#include "commands.h"
#include "search_gui.h"

void Engine::output(const char* fmt, ...)
{
    char buf[4096];
    int len;
    va_list va;
    va_start( va, fmt );
    len = MIN(vsnprintf( buf, 4096, fmt, va), 4096);
    va_end( va );

    fwrite(buf, 1, len, stdout);
    if(log)
    {
        fwrite(buf, 1, len, log);
        fflush(log);
    }
}

void Engine::run_command(int argc, char** argv)
{
    if(argv[0][0] == '#' || argv[0][0] == ';')
        return;

    CmdSet::iterator it = commands.lower_bound(argv[0]);
    if( it != commands.end() &&
            it->first.compare(0, strlen(argv[0]), argv[0]) == 0 )
    {
        Cmd& c = it->second;

        if(it->first != argv[0])
            output("%s\n", it->first.c_str());

        if(argc-1 < c.min())
            output("Error, not enough arguments (%d) for command '%s' (try 'help')\n", argc-1, it->first.c_str() );
        else if(argc-1 > c.max())
            output("Error, too many arguments (%d) for command '%s' (try 'help')\n", argc-1, it->first.c_str() );
        else
            c.run(argc-1, argv+1);
    }
    else
        output("Unknown command: '%s' (try 'help')\n", argv[0]);
}

void Engine::load_ini()
{
    FILE *f = fopen("rattatechess.ini", "r");
    if(!f)
        return;

    int argc = 0;
    char **argv;

    while( (argv = fget_tokenized(f, &argc)) )
    {
        if(!argc || strlen(argv[0])==0)
        {
            free(argv);
            continue;
        }
        run_command(argc, argv);
        free(argv);
    }

    fclose(f);
}

void Engine::process_input()
{
    int argc = 0;
    char **argv;
    Move mv;

    if(search_gui)
        search_gui->wait_input();
    argv = get_tokenized(&argc);

    if(!argv)
        exit(0);

    if(!argc || strlen(argv[0])==0)
    {
        free(argv);
        return;
    }

    /* is this a move? */
    mv = board.move_from_string(argv[0]);

    if(mv == Move::Illegal())
        output("Illegal move: %s\n", argv[0]);
    else if(mv == Move::Ambiguous())
        output("Illegal move (ambiguous): %s\n", argv[0]);
    else if(mv == Move::None()) /* not a move */
        run_command(argc, argv);
    else /* a valid move */
    {
        if(!io_xboard)
            print_moves(&mv,1);

        move(mv);

        if(!io_xboard)
            print_board();
    }

    free(argv);
}
