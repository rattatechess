/***************************************************************************
                 platform.h  -  Platoform dependent utilities
                             -------------------
    begin                : Fri Sep 28 2007
    copyright            : (C) 2007 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __PLATFORM_H__
#define __PLATFORM_H__

#include <stdio.h>
#include <inttypes.h>

/* map and unmap a file in memory. */
void *map_file(const char *file_name, uint32_t* size);
void unmap_file(void*addr, uint32_t size);

/* return the current time in centiseconds */
int current_time();

/* Initialize some information about stdin */
void init_stdin();

/* return if there is input available */
bool input_available();

/* execute and initialize another xboard engine (only for testing) */
bool start_engine(const char *prog, FILE **in, FILE **out);

/* get the cycle counter (only for profiling) */
inline uint64_t rdtsc()
{
    uint64_t r;
    asm volatile ("rdtsc\n\t" : "=A" (r));
    return r;
}

#endif //__PLATFORM_H__
