/***************************************************************************
                          engine.h  -  description
                             -------------------
    begin                : mer ott 23 2002
    copyright            : (C) 2002-2005 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __ENGINE_H__
#define __ENGINE_H__

#include <string.h>

#define PLAYING   1
#define _10       2
#define _01       3
#define _12       4

//#define PLAYING   1
#define ANALYZING 2
#define FORCING   3

#define TIME_LIMIT  1
#define DEPTH_LIMIT 2
#define NODES_LIMIT 3


#include <map>
#include <string>
#include <setjmp.h>
#include "board.h"

class SearchGui;
class SearchRoot;

class Engine
{
public:
    class Cmd;
    class Variable;

    typedef std::map<std::string, Cmd> CmdSet;
    typedef std::map<std::string, Variable> VariableSet;

    Board   board;

    SearchGui* search_gui;

    SearchRoot* current_root;

    Move*   mv_done;
    int     mv_done_num;

    SaveBuf *save_buf;
    int     save_buf_num;

    /* the max depth of the engine */
    int     st_depth;
    int     st_nodes;

    /* the color of the engine */
    uint8_t st_computer_color;

    /* the time the engine will use to think */
    int     time_best_csec;

    /* if there is a fixed time for a move, or else 0 */
    int     time_fixed;

    /* time move per section, time per section and inc... */
    int     time_mps;
    int     time_base;
    int     time_inc;

    /* clok and other clock in sec/100 */
    int     time_clock_csec;
    int     time_oth_clock_csec;

    /* flags for options of the engine */
    int     analysis_limit;

    bool     ponder;
    bool     post;
    //uint8_t  eng_color;
    uint64_t processed_nodes;
    int      max_think_time;

    /* flag for output behaviour */
    bool    io_xboard;
    bool    io_san;
    bool    io_colors;

    /* engine status */
    int         status;
    int         eng_status;
    const char *status_string;
    bool        thinking;
    int         start_think_time;
    uint8_t     analysis_color;

    FILE    *log;
    CmdSet   commands;
    VariableSet   variables;
    jmp_buf  back;

    HashEntry  *hash_table;
    void       *book_mmap;
    uint32_t    book_size;

    typedef std::map<HashKey, std::map<Move, int> > Lines;
    Lines lines;

    class OpeningInfo {
    public:
        const char *eco;
        const char *name;
        void set(const char* e, const char* n) {
            if(eco) free((void*)eco); if(name) free((void*)name);
            eco = e ? strdup(e) : NULL; name = n ? strdup(n) : NULL; }
        OpeningInfo(const OpeningInfo& o)
            : eco(o.eco ? strdup(o.eco) : NULL), name(o.name ? strdup(o.name) : NULL) {}
        OpeningInfo() : eco(NULL), name(NULL) {}
        ~OpeningInfo(){ if(eco) free((void*)eco); if(name) free((void*)name); }
    };
    typedef std::map<HashKey, OpeningInfo> Openings;
    Openings openings;

    char     opponent[256];
    int      w_rating;
    int      b_rating;

    static Engine* instance;

    /* variables */
    int v_book_creation_max_depth;
    int v_book_creation_max_alternatives;
    int v_book_creation_min_games;
    int v_book_creation_min_probability;
    int v_book_creation_weigh_win;
    int v_book_creation_weigh_draw;
    int v_book_creation_weigh_lose;

    int v_search_null_reduction;
    int v_search_threat_extension;
    int v_search_threat_threshold;

    int v_eval_draw;

    void cmd__check(int argc, char *argv[]);
    void cmd__attacks(int argc, char *argv[]);
    void cmd__see(int argc, char *argv[]);
    void cmd__shat(int argc, char *argv[]);
    void cmd__find_moves(int argc, char *argv[]);
    void cmd__gen_hash(int argc, char *argv[]);
    void cmd__gon(int argc, char *argv[]);
    void cmd__goff(int argc, char *argv[]);
    void cmd_quit(int argc, char *argv[]);
    void cmd_ping(int argc, char *argv[]);
    void cmd_bench(int argc, char *argv[]);
    void cmd_perft(int argc, char *argv[]);
    void cmd_perftall(int argc, char *argv[]);
    void cmd_sd(int argc, char *argv[]);
    void cmd_st(int argc, char *argv[]);
    void cmd_sn(int argc, char *argv[]);
    void cmd_otim(int argc, char *argv[]);
    void cmd_time(int argc, char *argv[]);
    void cmd_level(int argc, char *argv[]);
    void cmd_help(int argc, char *argv[]);
    void cmd_force(int argc, char *argv[]);
    void cmd_undo(int argc, char *argv[]);
    void cmd_remove(int argc, char *argv[]);
    void cmd_new(int argc, char *argv[]);
    void cmd_white(int argc, char *argv[]);
    void cmd_black(int argc, char *argv[]);
    void cmd_go(int argc, char *argv[]);
    void cmd_setboard(int argc, char *argv[]);
    void cmd_edit(int argc, char *argv[]);
    void cmd_load(int argc, char *argv[]);
    void cmd_save(int argc, char *argv[]);
    void cmd_load_pgn(int argc, char *argv[]);
    void cmd_epdtest(int argc, char *argv[]);
    void cmd_challenge(int argc, char *argv[]);
    void cmd_create_book(int argc, char *argv[]);
    void cmd_open_book(int argc, char *argv[]);
    void cmd_close_book(int argc, char *argv[]);
    void cmd_open_lines(int argc, char *argv[]);
    void cmd_close_lines(int argc, char *argv[]);
    void cmd_open_eco(int argc, char *argv[]);
    void cmd_close_eco(int argc, char *argv[]);
    void cmd_test(int argc, char *argv[]);
    void cmd_protover(int argc, char *argv[]);
    void cmd_accepted(int argc, char *argv[]);
    void cmd_rejected(int argc, char *argv[]);
    void cmd_xboard(int argc, char *argv[]);
    void cmd_analyze(int argc, char *argv[]);
    void cmd_exit(int argc, char *argv[]);
    void cmd_easy(int argc, char *argv[]);
    void cmd_hard(int argc, char *argv[]);
    void cmd_post(int argc, char *argv[]);
    void cmd_nopost(int argc, char *argv[]);
    void cmd_result(int argc, char *argv[]);
    void cmd_DOT(int argc, char *argv[]);
    void cmd_QUESTION(int argc, char *argv[]);
    void cmd_name(int argc, char *argv[]);
    void cmd_rating(int argc, char *argv[]);
    void cmd_var(int argc, char *argv[]);
    void cmd_varhelp(int argc, char *argv[]);
    void cmd_set(int argc, char *argv[]);

    void create_hash();
    void reset_hash();
    void make_old_hash();

    HashEntry* probe_hash(const HashKey& hk);
    void prefetch_hash(const HashKey& hk);
    void write_hash(const HashKey& hk, int16_t lo, int16_t up, int depth, int best_cont, bool no_good_moves);

    bool check_time();
    bool check_draw();
    bool check_repetition(int nfold);

    void print_stat();
    void move_now();
    static void sort_moves(Move* m,int num);
    static void incremental_sort_moves(Move* m,int num);

    void calc_best_time();


    friend class Board;

public:
    Engine();
    static Engine *eng();

    void run_command(int argc, char** argv);
    void load_ini();

    uint8_t color_to_move();
    uint8_t computer_color();
    int  get_status();
    int  move_number();

    void register_commands();
    void register_variables();

    void process_input();
    void output(const char* fmt, ...) PRINTF(2, 3);

    Move find_best_move();

    void move(Move mv);
    void retract_move();
    void undo();
    void start();
    void start(unsigned char color);
    void force();
    void analyze();
    void do_ponder();
    void new_game();
    uint64_t perft(int lev);
    void set_depth(int depth);
    void set_max_nodes(int nodes);
    void set_time_control(int mps, int basesec, int inc);
    void set_time_fixed(int time);
    void set_my_clock(int time);
    void set_other_clock(int time);

    /* io functions */
    void save_pgn(FILE *f, const char *result1, const char *result);
    void print_moves(Move* m, int num, bool nums=true, bool walk_ht=false);
    void print_board();        //prints colored board to stdout
    void read_board(char* brd, char* color, char* castle,
                    char* passing, int moves_to_draw, int num_moves);
    void read_board(char* str);//reads from f a Forsythe-Edwards notation board
    void read_board(FILE* f);  //reads from f a Forsythe-Edwards notation board
    void write_board(FILE* f); //writes to f a Forsythe-Edwards notation board
    int  load_pgn(FILE *f);
    int  run_epd_prog(const char* prog, int time, const char* file, bool quiet = true);
    void challenge(const char* prog, int time, const char* file);

    void create_book(int argc, char *argv[]);
    bool open_book(const char *f);
    void close_book();
    Move probe_book(Board *b);

    bool open_lines(const char *f);
    void close_lines();
    Move probe_lines(Board *b);

    bool open_openings(const char *f);
    void close_openings();
    OpeningInfo* probe_openings(Board *b);

    bool parse_command(char *str);

    void autotune();
};

#endif //__ENGINE_H__
