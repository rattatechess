/***************************************************************************
                          moveparse.cpp  -  description
                             -------------------
    begin                : mer ott 30 2002
    copyright            : (C) 2002-2007 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "engine.h"
#include "utils.h"
#include <string.h>

void Engine::print_moves(Move* m, int num, bool nums, bool walk_ht)
{
    int i,j;
    if(board.color_to_move==BLACK &&nums)
        output("%d. ...",board.num_moves+1);

    for(i=0;i<num;i++)
    {
        if(m[i].as_int == 0)
            break;

        if(board.color_to_move==WHITE && nums)
            printf(" %d.",board.num_moves+1);
        output(" %s", board.move_to_alg(MoveStr().data(), m[i]) );
        board.do_move(m[i], save_buf[save_buf_num++]);
    }

    if(walk_ht)
    {
        Move hash_mv[24];
        for(j=0;j<24-i;j++)
        {
            HashEntry *h = probe_hash( board.hash );
            if(!h || !h->best_mv || h->depth<0)
                break;

            /* don't follow the hash if there is draw for repetition :) */
            if( check_draw() )
            {
                output( " <DRAW>" );
                break;
            }

            hash_mv[j] = board.uncompress_move( h->best_mv );

            /* verify that the move in the hash is a legal move */
            Move tmp[255];
            int num_tmp = board.find_moves(tmp);
            bool ok = false;
            for(int q=0;q<num_tmp;q++)
            if(tmp[q] == hash_mv[j])
            {
                ok = true;
                break;
            }

            if(!ok)
                break;

            if(j==0)
                output(" |");

            if(board.color_to_move==WHITE && nums)
                output(" %d.",board.num_moves+1);
            output( " %s", board.move_to_alg(MoveStr().data(), hash_mv[j]) );
            board.do_move(hash_mv[j], save_buf[save_buf_num++]);
        }

        while(j--)
            board.undo_move(hash_mv[j], save_buf[--save_buf_num]);
    }

    while(i--)
        board.undo_move(m[i], save_buf[--save_buf_num]);

    output("\n");
}

/* TODO: load correctly the pgn that start with a fen */
int Engine::load_pgn(FILE *f)
{
    char buf[1024];

    board.set_as_default();
    mv_done_num     = 0;

    while(1)
    {
        if(!fgets(buf, 1024, f))
            return 0;
        if(buf[0]=='\n' || buf[0]=='\r' || buf[0]=='\t'
                || buf[0]==' ' || buf[0]=='[' || buf[0]=='#')
            continue;
        break;
    }

    while(1)
    {
        char *ptr = buf;
        ptr += strspn(ptr, ". \t\r\n");

        for(int i=0;;i++)
        {
            if(!*ptr)
                break;
            char *t = ptr;
            char *tmp = (ptr += strcspn(ptr, ". \t\r\n"));
            ptr += strspn(ptr, ". \t\r\n");
            *tmp = 0;

            /* is this the result? */
            if(!strcmp(t, "1-0"))
            {
                status = _10;
                return 1;
            }
            else if(!strcmp(t, "0-1"))
            {
                status = _01;
                return 1;
            }
            else if(!strcmp(t, "1/2-1/2") || !strcmp(t, "*"))
            {
                status = _12;
                return 1;
            }

            /* is this a move number? */
            char *e;
            strtol(t, &e, 10);
            if(e!=t)
                continue;

            Move mv = board.move_from_string(t);
            if(!mv.valid())
            {
                board.print_board();
                output("Error! \"%s\" %x\n", t, mv.as_int);
                return 0;   /* error */
            }

            mv_done[mv_done_num++] = mv;
            board.do_move(mv, save_buf[save_buf_num++]);
        }

        if(!fgets(buf, 1024, f))
            return 1;
    }
}

int Engine::run_epd_prog(const char* prog, int time, const char* file, bool quiet)
{
    char buf[1024];
    bool failed[8000];
    FILE *in = NULL;
    FILE *out = NULL;
    FILE *f;

    /* open the file with the tests */
    f = fopen(file, "r");
    if(!f)
    {
        output("Error, could not open %s for reading!\n", file);
        return 0;
    }

    if(!strcmp(prog, "self"))
    {
        if(quiet)
            post = false;
        set_time_fixed(time);
    }
    else
        start_engine(prog, &in, &out);

    /* reset passed flags */
    int done = 0;
    int passed = 0;
    for(int i=0;i<8000;i++)
        failed[i] = false;

    while(fgets(buf, 1024, f))
    {
        char brd[81];
        char color[2],castle[5],passing[3];
        char best[256];
        char avoid[256];
        char name[64];// = "unknown";
        char *tmp;
        Move best_mv[10];
        Move avoid_mv[10];
        int num_best = 0;
        int num_avoid = 0;
        Move m;


        /* read the board */
        sscanf(buf,"%s%s%s%s",
               brd,color,castle,passing);
        read_board(brd,color,castle,passing,0,0);
        if(!quiet)
        {
            output("\n\n");
            board.print_board();
        }


        done++;
        //printf("%s\n", buf);

        /* save the best moves in the best_mv array */
        tmp = strstr(buf, "bm");
        if(tmp)
        {
            int tok = 0;
            sscanf(tmp, "bm %63[^;];", best);
            tok += strspn(best + tok, " \t\r\n");

            for(num_best=0;num_best<10;num_best++)
            {
                if(!best[tok])
                    break;

                int start = tok;
                int firstspace = (tok += strcspn(best + tok, " \t\r\n"));
                tok += strspn(best + tok, " \t\r\n");
                best[firstspace] = 0;

                best_mv[num_best] = board.move_from_string(best+start);

                if(!best_mv[num_best].valid())
                {
                    output("Error, could not parse move %s in pos %d\n", best+start, done);
                    return 0;
                }
            }
        }

        /* save the avoid moves in the best_mv array */
        tmp = strstr(buf, "am");
        if(tmp)
        {
            int tok = 0;
            sscanf(tmp, "am %63[^;];", avoid);
            tok += strspn(avoid + tok, " \t\r\n");

            for(num_avoid=0;num_avoid<10;num_avoid++)
            {
                if(!avoid[tok])
                    break;

                int start = tok;
                int firstspace = (tok += strcspn(avoid + tok, " \t\r\n"));
                tok += strspn(avoid + tok, " \t\r\n");
                avoid[firstspace] = 0;

                avoid_mv[num_avoid] = board.move_from_string(avoid+start);

                if(!avoid_mv[num_avoid].valid())
                {
                    output("Error, could not parse move %s in pos %d\n", best+start, done);
                    return 0;
                }
            }
        }

        /* get the id */
        tmp = strstr(buf, "id");
        if(tmp)
            sscanf ( tmp, "id %63[^;];", name);
        else
            strcpy( name, "<unknown>" );

        if(!quiet)
        {
            output(" %d - Thinking", done);
            if(num_best)
            {
                output(", the best move%s\e[32;1m", num_best>1 ? "s are" : " is");
                for(int i=0;i<num_best;i++)
                    output(" %s", board.move_to_alg(MoveStr().data(), best_mv[i]) );
                output("\e[0m");
            }
            if(num_avoid)
            {
                output(", the move%s to avoid\e[31;1m", num_avoid>1 ? "s are" : " is");
                for(int i=0;i<num_avoid;i++)
                    output(" %s", board.move_to_alg(MoveStr().data(), avoid_mv[i]) );
                output("\e[0m");
            }
            output("\n");
        }

        if(!strcmp(prog, "self"))
        {
            st_computer_color = board.color_to_move;
            m = find_best_move();
        }
        else
        {
            fprintf(out, "force\n");
            fprintf(out, "setboard %s\n", board.to_fen(BigStr().data()) );
            fprintf(out, "st %d\n", time);
            //fprintf(out, "level 1 %d 0\n", time);
            //fprintf(out, "time %d\n", time*100);
            //fprintf(out, "otim %d\n", time*100);
            fprintf(out, "post\n");
            fprintf(out, "go\n");

            //sleep(1);
            //fprintf(out, "?\n");

            while(fgets(buf, 1024, in))
            {
                char mv[32];
                if(!strncmp(buf, "move ", 5))
                {
                    sscanf ( buf, "move %s", mv);
                    m = board.move_from_string(mv);
                    if(!m.valid())
                    {
                        output("Got illegal move %s\n", mv);
                        return 0;
                    }
                    break;
                }
                if(!quiet)
                  output(buf);
            }
        }

        bool fail = false;
        if(!quiet)
            output("Result: \e[36;1m%s\e[0m\n", board.move_to_alg(MoveStr().data(), m));
        for(int i=0;i<num_avoid;i++)
        if(m == avoid_mv[i])
        {
            fail = true;
            if(!quiet)
                output("Test %d \e[31;1mFAILURE\e[0m, %s did not avoid the "
                        "wrong move \e[33;1m%s\e[0m (che pollo!)\n",done, prog,
                                   board.move_to_alg(MoveStr().data(), avoid_mv[i]));
        }
        if(num_avoid && !fail && !quiet)
            output("Test %d \e[32;1mSUCCESS\e[0m, %s avoided the "
                    "wrong move%s \e[33;1m%s\e[0m (teto figo!)\n",
                    done, prog, num_avoid>1?"s":"", avoid);
        if(!fail && num_best)
        {
            fail = true;
            for(int i=0;i<num_best;i++)
            if(m == best_mv[i])
            {
                fail = false;
                if(!quiet)
                    output("Test %d \e[32;1mSUCCESS\e[0m, %s found the "
                            "best move \e[33;1m%s\e[0m (o se figo!)\n",
                            done, prog,  board.move_to_alg(MoveStr().data(), best_mv[i]));
                break;
            }
            if(fail && !quiet)
                output("Test %d \e[31;1mFAILURE\e[0m, %s missed the best "
                        "move%s (\e[33;1m%s\e[0m) (che scarso!)\n",  done, prog,
                                num_best>1?"s":"", best);
        }
        if(!fail)
            passed++;
        else
            failed[done] = true;

        if(!quiet)
            output("Test %d %s %s!  (+%d, -%d, ~%d%%)\n", done, name, fail ?
                        "\e[31;1mFAILED\e[0m" : "\e[32;1mPASSED\e[0m",
                            passed, done-passed, passed*100/done);
        else {
            output("%s", fail ? "-" : "+");
            char str[80];
            snprintf(str, 40, "(%d/%d)", passed, done);
            int l = strlen(str);
            for(int i=0;i<l;i++)
                str[l+i] = '\b';
            fwrite(str, 2*l, 1, stdout);
            fflush(stdout);
        }
    }
    if(quiet)
        output("\n");
    output("Passed %d tests out of %d (%d%%)\n", passed, done, passed*100/done);
    if(!quiet)
    {
        output("Failed are:\n");
        int numsh = 0;
        for(int i=1;i<done+1;i++)
            if(failed[i])
            {
                output(" %d", i);
                if(((++numsh) % 10) == 0)
                    output("\n");
            }
        output("\n");
    }

    if(strcmp(prog, "self"))
    {
        fprintf(out, "quit\n");
        fclose(out);
        fclose(in);
    }
    fclose(f);

    return passed;
}

void Engine::save_pgn(FILE *f, const char *result1, const char *result)
{
    for(int i=mv_done_num-1;i>=0;i--)
        board.undo_move(mv_done[i], save_buf[--save_buf_num]);

    fprintf(f, "[White \"%s\"]\n", st_computer_color == WHITE ?
                                "RattateChess" : opponent );
    fprintf(f, "[Black \"%s\"]\n", st_computer_color == BLACK ?
                                "RattateChess" : opponent );
    fprintf(f, "[WhiteELO \"%d\"]\n", w_rating);
    fprintf(f, "[BlackELO \"%d\"]\n", b_rating);

    time_t t = time(NULL);
    char tbuf[256];
    strftime(tbuf, 256, "%Y.%m.%d %H:%M", localtime(&t));
    fprintf(f, "[Date \"%s\"]\n", tbuf);
    fprintf(f, "[Result \"%s\"]\n", result1);

    if(0)
        fprintf(f, "[FEN \"%s\"]\n", board.to_fen(BigStr().data()) );

    if(board.color_to_move==BLACK)
        fprintf(f, "%d. ... ",board.num_moves+1);
    for(int i=0;i<mv_done_num;i++)
    {
        if(board.color_to_move==WHITE)
            fprintf(f, "%d. ",board.num_moves+1);
        fprintf(f, "%s ", board.move_to_alg(MoveStr().data(), mv_done[i]) );
        if((i+1)%14 == 0 && i!=mv_done_num-1)
            fprintf(f, "\n");
        board.do_move(mv_done[i], save_buf[save_buf_num++]);
    }
    fprintf(f, "\n%s\n\n", result);
}

void Engine::challenge(const char* prog, int time, const char* file)
{
    char buf[1024];
    FILE *in = NULL;
    FILE *out = NULL;
    FILE *f;
    FILE *log;
    int won=0, draw=0, lost=0;

    /* open the file with the tests */
    f = fopen(file, "r");
    log = fopen("challenge.pgn", "w");

    if(!f)
    {
        output("Error, could not open %s for reading!\n", file);
        return;
    }

    start_engine(prog, &in, &out);

    while(fgets(buf, 1024, f))
    {
        uint8_t cols[2] = {WHITE, BLACK};

        for(int i=0;i<2;i++)
        {
            bool go_done = false;
            read_board(buf);
            //set_max_nodes(time);
            set_time_fixed(time);
            //set_time_control(40, time, 0);
            ponder = false;

            fprintf(out, "new\n");
            fprintf(out, "st %d\n", time);
            //fprintf(out, "level 40 %d:%d 0\n", time/60, time%60);
            fprintf(out, "force\n");
            fprintf(out, "setboard %s\n", board.to_fen(BigStr().data()));


            status = PLAYING;
            st_computer_color = cols[i];

            while(1)
            {
                char buf[1024];

                output("\n\n");
                board.print_board();

                /* print the pgn to the log file */
                if(status != PLAYING)
                {
                    const char *result = status==_01?"0-1":(status==_10?"1-0":"1/2-1/2");
                    const char *white = st_computer_color == WHITE ? "Rattatechess" : prog;
                    const char *black = st_computer_color == BLACK ? "Rattatechess" : prog;

                    if(log)
                    {
                        for(int i=mv_done_num-1;i>=0;i--)
                            board.undo_move(mv_done[i], save_buf[--save_buf_num]);

                        fprintf(log, "[White \"%s\"]\n", white);
                        fprintf(log, "[Black \"%s\"]\n", black);
                        fprintf(log, "[Result \"%s\"]\n", result);
                        fprintf(log, "[FEN \"%s\"]\n", board.to_fen(BigStr().data()) );

                        if(board.color_to_move==BLACK)
                            fprintf(log, "%d. ... ",board.num_moves+1);
                        for(int i=0;i<mv_done_num;i++)
                        {
                            if(board.color_to_move==WHITE)
                                fprintf(log, "%d. ",board.num_moves+1);
                            fprintf(log, "%s ", board.move_to_alg(MoveStr().data(), mv_done[i]) );
                            if((i+1)%14 == 0)
                                fprintf(log, "\n");
                            board.do_move(mv_done[i], save_buf[save_buf_num++]);
                        }
                        fprintf(log, "%s\n\n", result);
                        fflush(log);
                    }

                    if((status==_01 && st_computer_color==BLACK) ||
                            (status==_10 && st_computer_color==WHITE))
                    {
                        output("RATTA!!!\n");
                        won++;
                    }
                    else if(status==_12)
                    {
                        output("DRAW!\n");
                        draw++;
                    }
                    else
                    {
                        output("ARGH!!!\n");
                        lost++;
                    }
                    break;
                }

                /* think or wait for a new move */
                if(st_computer_color == board.color_to_move)
                {
                    /* think and move */
                    Move mv = find_best_move();
                    output("%d. %s %s\n",board.num_moves+1,
                            board.color_to_move==BLACK ? "..." : "",
                            board.move_to_alg(MoveStr().data(), mv) );
                    fprintf( out, "%s\n", Board::move_to_coord(MoveStr().data(), mv));
                    move(mv);
                }
                else
                {
                    if(!go_done)
                    {
                        fprintf( out, "go\n");
                        go_done = true;
                    }

                    while(fgets(buf, 1024, in))
                    {
                        /* get a move from the other engine */
                        if(!strncmp(buf, "move ", 5))
                        {
                            Move mv;
                            char buf2[32];
                            sscanf( buf, "move %s", buf2);
                            mv = board.move_from_string(buf2);
                            if(!mv.valid())
                            {
                                output("Got illegal move %s\n", buf2);
                                return;
                            }
                            output("%d. %s %s\n",board.num_moves+1,
                                    board.color_to_move==BLACK ? "..." : "",
                                    board.move_to_alg(MoveStr().data(), mv) );
                            move(mv);
                            break;
                        }
                        else if(!strcmp(buf, "resign"))
                        {
                            status = board.color_to_move==WHITE?_01:_10;
                            break;
                        }
                        //else
                          //  output(buf);
                    }
                }
            }
        }
    }

    fprintf(out, "quit\n");
    fclose(out);
    fclose(in);
    if(log) fclose(log);
    fclose(f);

    output("\n\n%d games played (+%d =%d -%d)\n", won+draw+lost, won, draw, lost);
}
