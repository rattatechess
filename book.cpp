/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Mer Oct 19 2005
    copyright            : (C) 2005 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "engine.h"
#include "utils.h"

/* each move in the book will be stored in 24 bits */
struct PACKED ComprMove
{
    unsigned int move : 14;
    unsigned int stat : 10;
};

/* MoveMark and MoveStat are for the book creation */
struct PACKED MoveMark
{
    uint16_t move;
    uint32_t stat;
};

class MoveStat
{
private:
    MoveMark mv_first;
    uint16_t oth_size;
    MoveMark *mv_oth;

public:
    int num;
    unsigned int tot;

    MoveMark& operator[](int i) {
        if(i==0)
            return mv_first;
        if(i > oth_size)
        {
            oth_size = i;
            mv_oth = (MoveMark*)realloc(mv_oth, oth_size*sizeof(MoveMark));
        }
        return mv_oth[i-1];
    }

    MoveStat()
        : oth_size(0)
        , mv_oth(NULL)
        , num(0)
        , tot(0)
    {
    }

    ~MoveStat()
    {
        if(mv_oth)
            free(mv_oth);
    }

    void insert(uint32_t m, unsigned int w )
    {
        for(int i=0;i<num; i++)
        if((*this)[i].move == m)
        {
            (*this)[i].stat += w;
            tot += w;
            return;
        }

        (*this)[num].move = m;
        (*this)[num].stat = w;
        tot += w;
        num++;
    }

    static int sort_mv_marks(const void *a, const void *b)
    {
        return ((MoveMark*)b)->stat - ((MoveMark*)a)->stat;
    }

    void sort_moves()
    {
        for(int i=0;i<num-1;i++)
        if(mv_oth[i].stat > mv_first.stat)
            SWITCH(mv_oth[i], mv_first);
        qsort(mv_oth, num-1, sizeof(MoveMark), sort_mv_marks);
    }
};

typedef std::map< HashKey, MoveStat> BkEntryMap;


/* an entry in the book */
struct PACKED BookEntry
{
    HashKey key;
    /* the offset you have to add to find the right branch of the binary tree */
    uint32_t right_off;
    /* the number of moves */
    uint8_t num;
    ComprMove mv[1];
};

/* special flags for marking the leaf nodes or those with only 1 child */
#define NO_RIGHT ((uint32_t)-1)
#define LEAF ((uint32_t)-2)

/* the size of the entry in a book with x alternatives */
#define ENTRY_SIZE(x) (sizeof(BookEntry) + ((x)-1)*sizeof(ComprMove))

static uint32_t calc_book_range_size(BkEntryMap::iterator begin, BkEntryMap::iterator end)
{
    uint32_t sz = 0;
    for(;begin!=end;++begin)
        sz += ENTRY_SIZE(begin->second.num);
    return sz;
}

static int write_book_range(FILE* f, BkEntryMap::iterator begin, BkEntryMap::iterator end)
{
    if(begin == end)
        return 0;

    int num = 0;
    BkEntryMap::iterator mid = begin;
    while(mid != end)
    {
        ++num;
        ++mid;
    }

    mid = begin;
    for(int i=0;i<num/2;i++)
        mid++;

    const HashKey& hk = mid->first;
    MoveStat& m = mid->second;
    uint32_t besz = ENTRY_SIZE(m.num);
    uint32_t lsize = calc_book_range_size(begin, mid);
    BkEntryMap::iterator tmpit = mid;
    BookEntry b;
    b.key = hk;
    b.num = m.num;
    if(++tmpit == end)
        b.right_off = lsize ? NO_RIGHT : LEAF;
    else
        b.right_off = lsize;
    b.mv[0].move = m[0].move;
    b.mv[0].stat = m[0].stat;
    fwrite(&b, 1, sizeof(BookEntry), f);

    for(int i=1;i<m.num;i++)
    {
        ComprMove c = { m[i].move, m[i].stat };
        fwrite(&c, 1, sizeof(ComprMove), f);
    }


    uint32_t truels = write_book_range(f, begin, mid);
    if(truels != lsize)
        fprintf(stderr, "Error! %d %d\n", truels, lsize);
    ++mid;

    return besz + lsize + write_book_range(f, mid, end);
}

void Engine::create_book(int argc, char *argv[])
{
    int n=0;
    BkEntryMap b;
    FILE* f = fopen(argv[0],"r");
    if(!f)
        output("Error, could not open \"%s\" for reading.\n", argv[0]);

    while(load_pgn(f))
    {
        if(++n % 1000 == 0)
            output("(%d games processed)\n", n);

        for(int i=mv_done_num-1; i>=0; i--)
        {
            board.undo_move(mv_done[i], save_buf[--save_buf_num]);

            if(i>v_book_creation_max_depth)
                continue;

            MoveStat& ms = b[board.hash];

            unsigned int weight = v_book_creation_weigh_draw;
            if(status == _01)
                weight = board.color_to_move == BLACK ? v_book_creation_weigh_win : v_book_creation_weigh_lose;
            else if(status == _10)
                weight = board.color_to_move == WHITE ? v_book_creation_weigh_win : v_book_creation_weigh_lose;

            ms.insert( board.compress_move(mv_done[i]), weight );
        }
    }
    fclose(f);


    /* adjust the entries */
    output("(preprocessing the tree)\n");

    for(BkEntryMap::iterator it = b.begin(); it != b.end(); )
    {
        MoveStat& ms = it->second;

        /* discard moves played too few times in games */
        if(ms.tot < (unsigned int)v_book_creation_min_games)
        {
            b.erase(it++);
            continue;
        }
        ++it;

        /* remove very rare moves) */
        for(int i=0;i<ms.num;i++)
        {
            if(ms[i].stat*1000 < ms.tot*v_book_creation_min_probability)
            {
                ms.tot -= ms[i].stat;
                ms[i] = ms[ms.num-1];
                i--;
                ms.num--;
            }
        }

        /* renormalize the alternatives up to 1024 */
        int prev_tot = 0;
        for(int i=0;i<ms.num;i++)
        {
            int st = ms[i].stat;
            ms[i].stat = ((prev_tot+st)*1024)/ms.tot
                        - (prev_tot*1024)/ms.tot;
            prev_tot += st;
        }

        /* sort them so the program will give a nicer output :) */
        ms.sort_moves();
    }

    FILE *bf = fopen("ratta.book","w");
    output("(writing the book)\n");
    uint32_t sz = write_book_range(bf, b.begin(), b.end());
    printf("Book size is %u\n",sz);
    fclose(bf);

    st_computer_color = 0;
}

bool Engine::open_book(const char *f)
{
    close_book();

#if 1
    book_mmap = map_file(f, &book_size);
    if(!book_mmap)
    {
        output("Error, could not open and mmap the book file \"%s\".\n", f);
        return false;
    }
    else
    {
        output("Book file \"%s\" opened and mapped, good!\n", f);
        return true;
    }
#else
    FILE *file = fopen(f, "r");
    if(!file)
    {
        output("Error, could not open and mmap the book file \"%s\" (%s).\n", f, strerror(errno));
        return false;
    }
    else
    {
        fseek(file, 0, SEEK_END);
        book_size = ftell(file);
        fseek(file, 0, SEEK_SET);
        book_mmap = malloc(book_size);
        fread(book_mmap, book_size, 1, file);
        fclose(file);

        output("Book file \"%s\" opened and mapped, good!\n", f);
        return true;
    }
#endif
}

void Engine::close_book()
{
#if 1
    if(book_mmap)
        unmap_file(book_mmap, book_size);
#else
    if(book_mmap)
        free(book_mmap);
#endif
    book_mmap = NULL;
}

Move Engine::probe_book(Board *brd)
{
    uint32_t delta = 0;

    if(!book_mmap || mv_done_num>50)
        return Move::None();

    /* walk the in-memory mapped binary search tree */
    while(1)
    {
        BookEntry *b = (BookEntry*)((long)book_mmap+delta);

        /* book entry found */
        if(b->key == brd->hash)
        {
            /* print the (e4 64%, d4 36%)-like thinking line */
            if(post)
            {
                if(OpeningInfo* i = probe_openings(brd))
                    output("\t0\t0\t0\t0\t%s: %s (", i->eco, i->name);
                else
                    output("\t0\t0\t0\t0\t(");

                for(unsigned int i=0;i<b->num;i++)
                {
                    Move m = brd->uncompress_move(b->mv[i].move);
                    output("%s%s %.01f%%",
                            i!=0 ? "," : "",
                            brd->move_to_alg(MoveStr().data(), m),
                            b->num == 1 ? 100 : b->mv[i].stat*100.0/1024);
                }
                output(")\n");
            }

            if(b->num == 1)
                return brd->uncompress_move(b->mv[0].move);

            int r = rand()%1024;
            for(unsigned int i=0;i<b->num;i++)
            {
                r -= b->mv[i].stat;
                if(r<=0)
                    return brd->uncompress_move(b->mv[i].move);
            }

            /* this should never happen */
            return Move::None();
        }

        if(b->right_off == LEAF)
            return Move::None();

        if(brd->hash < b->key)
        {
            /* no left entry */
            if(b->right_off == 0)
                return Move::None();

            /* go on */
            delta += ENTRY_SIZE(b->num);
        }
        else
        {
            /* no right entry */
            if(b->right_off == NO_RIGHT)
                return Move::None();

            /* go on */
            delta += ENTRY_SIZE(b->num) + b->right_off;
        }

        if(delta < 0 || delta >= book_size-16 )
        {
            output("Error! Damaged binary book!\n");
            return Move::None();
        }
    }
}

bool Engine::open_lines(const char *linef)
{
    close_lines();

    FILE* f = fopen(linef,"r");
    if(!f)
    {
        output("Error, could not open \"%s\" for reading.\n", linef);
        return false;
    }

    while(load_pgn(f))
    {
        for(int i=mv_done_num-1; i>=0; i--)
        {
            board.undo_move(mv_done[i], save_buf[--save_buf_num]);

            if(mv_done[i].val > 0)
            {
                std::map<Move, int>& ml = lines[board.hash];
                ml[mv_done[i]] += mv_done[i].val;
            }
        }
    }
    fclose(f);
    output("Lines loaded from file \"%s\", good!\n", linef);
    return true;
}

void Engine::close_lines()
{
    lines.clear();
}

Move Engine::probe_lines(Board *brd)
{
    Lines::iterator it = lines.find( brd->hash );
    if( it == lines.end() )
        return Move::None();

    int sum = 0;
    for(std::map<Move, int>::iterator mit = it->second.begin(); mit != it->second.end(); ++mit)
        sum += mit->second;

    if(!sum)
        return Move::None();

    int r = rand() % sum;
    for(std::map<Move, int>::iterator mit = it->second.begin(); mit != it->second.end(); ++mit)
    if( (r -= mit->second) < 0)
        return mit->first;

    return Move::None();
}

bool Engine::open_openings(const char *ecos)
{
    close_openings();

    FILE* f = fopen(ecos,"r");
    if(!f)
    {
        output("Error, could not open \"%s\" for reading.\n", ecos);
        return false;
    }
    int argc;
    char **argv;

    while( (argv = fget_tokenized(f, ". \t\v\n\r", &argc)) )
    {
        if(!argc)
        {
            free(argv);
            continue;
        }

        board.set_as_default();
        int i = 1;
        while(i<argc)
        {
            /* is this a move number? */
            char *e;
            strtol(argv[i], &e, 10);
            if(*e == '\0')
            {
                i++;
                continue;
            }

            Move mv = board.move_from_string(argv[i]);
            if(mv.valid())
            {
                SaveBuf tmp;
                board.do_move(mv, tmp);
                i++;
            }
            else
            {
                if(mv == Move::None() && i==argc-1)
                    openings[board.hash].set(argv[0], argv[i]);
                else
                    printf("Error parsing opening! (%s, %s)\n", argv[0], argv[i]);
                break;
            }
        }

        free(argv);
    }

    fclose(f);
    output("ECO codes loaded from file \"%s\", good!\n", ecos);
    return true;
}

void Engine::close_openings()
{
}

Engine::OpeningInfo* Engine::probe_openings(Board *b)
{
    std::map<HashKey, OpeningInfo>::iterator it = openings.find(b->hash);
    if(it == openings.end())
        return NULL;
    else
        return &it->second;
}

