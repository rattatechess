/***************************************************************************
                 search_gui.cpp  -  Gui to view the search tree
                             -------------------
    begin                : Sat Oct 06 2007
    copyright            : (C) 2007 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "search_gui.h"
#include <stdarg.h>
#include <QWidget>
#include <QApplication>
#include <QTreeWidget>
#include <QHeaderView>
#include <QSocketNotifier>
#include <QTreeWidgetItem>

QString qPrintf(const char* fmt, ...) {
  va_list ap;

  va_start(ap, fmt);
  int l = vsnprintf(NULL, 0, fmt, ap)+1;
  va_end(ap);

  char *str = (char*)alloca(l);
  //char *str = (char*)malloc(l);
  va_start(ap, fmt);
  vsnprintf(str, l, fmt, ap);
  va_end(ap);

  QString retv(str);

  //free(str);
  return retv;
}

SearchGui::SearchGui(int& argc, char** argv)
{
    printf("Hello!\n");
    app = new QApplication(argc, argv);
    tree_widget = new QTreeWidget();
    tree_widget->header()->hide();
    tree_widget->resize(640, 480);
    tree_widget->show();

    max_ply = 4;

    process_events();
    printf("Gui up!\n");

    new QSocketNotifier( fileno(stdin), QSocketNotifier::Read, this);
    new QSocketNotifier( fileno(stdin), QSocketNotifier::Exception, this);
}

SearchGui::~SearchGui()
{
    delete tree_widget;
    delete app;
}

void SearchGui::wait_input()
{
    while(1)
    {
        if(input_available())
            return;
        app->processEvents(QEventLoop::WaitForMoreEvents);
    }
}

void SearchGui::process_events()
{
    while(app->hasPendingEvents() && !input_available())
        app->processEvents();
}

void SearchGui::apply_flags(QTreeWidgetItem* w, int flags)
{
    QFont font = w->font(0);
    if(flags & Italic)
        font.setItalic(true);
    if(flags & Bold)
        font.setBold(true);
    if(flags & NoItalic)
        font.setItalic(false);
    if(flags & NoBold)
        font.setBold(false);
    w->setFont(0, font);
    if(flags & Red)
        w->setForeground(0, Qt::red);
    if(flags & Gray)
        w->setForeground(0, Qt::gray);
    if(flags & Green)
        w->setForeground(0, Qt::green);
    if(flags & Blue)
        w->setForeground(0, Qt::blue);
    if(flags & Magenta)
        w->setForeground(0, Qt::magenta);
}

void SearchGui::init_root()
{
    tree_widget->clear();
}

void SearchGui::new_root_level(int depth)
{
    QTreeWidgetItem *item = new QTreeWidgetItem(QStringList()<<qPrintf("ROOT(%d)", depth));
    tree_widget->addTopLevelItem(item);
    item->setExpanded(true);
}

void SearchGui::notify_value(int ply, int value, int nodecount, int newflags)
{
    if(ply>max_ply)
        return;

    QTreeWidgetItem *item = tree_widget->topLevelItem(tree_widget->topLevelItemCount()-1);
    for(int i=0;i<=ply;i++)
        item = (item && item->childCount()) ? item->child(item->childCount()-1) : NULL;
    if(!item)
        return;
    item->setText(0, qPrintf( qPrintable(item->text(0)), value, nodecount ) );
    apply_flags(item, newflags);
}

void SearchGui::notify_msg(int ply, const char* str, int flags)
{
    if(ply>max_ply)
        return;

    QTreeWidgetItem *parent = tree_widget->topLevelItem(tree_widget->topLevelItemCount()-1);
    for(int i=0;i<ply;i++)
        parent = (parent && parent->childCount()) ? parent->child(parent->childCount()-1) : NULL;
    if(!parent)
        return;
    QTreeWidgetItem *item = new QTreeWidgetItem(parent, QStringList()<< QString(str) );
    if(parent->isExpanded())
        tree_widget->scrollToItem(item);
    apply_flags(item, flags);
}

void SearchGui::notify(const Board *board, Move m, int ply, int depth, int heuval, int alpha, int beta, int flags)
{
    if(ply>max_ply)
        return;

    char str[64];
    if(m == Move::None())
        strcpy(str, "[NULL]");
    else
    {
        SaveBuf tmp;
        Board b = *board;
        b.undo_move(m, tmp);
        b.move_to_alg(str, m);
        b.do_move(m, tmp);
        if(heuval != -INF)
            sprintf(str+strlen(str), "(%d)", heuval);
    }
    QTreeWidgetItem *parent = tree_widget->topLevelItem(tree_widget->topLevelItemCount()-1);
    for(int i=0;i<ply;i++)
        parent = (parent && parent->childCount()) ? parent->child(parent->childCount()-1) : NULL;
    if(!parent)
        return;
    QTreeWidgetItem *item = new QTreeWidgetItem(parent, QStringList()<<
        qPrintf("%s  -> %%d @%d  [%d .. %d]  nc=%%d", str, depth, alpha, beta) );
    if(parent->isExpanded())
        tree_widget->scrollToItem(item);
    apply_flags(item, flags);
}

void SearchGui::notify_eval(int ply, int value, int alpha, int beta, int flags)
{
    if(ply>max_ply)
        return;

    QTreeWidgetItem *parent = tree_widget->topLevelItem(tree_widget->topLevelItemCount()-1);
    for(int i=0;i<ply;i++)
        parent = (parent && parent->childCount()) ? parent->child(parent->childCount()-1) : NULL;
    if(!parent)
        return;
    QTreeWidgetItem *item = new QTreeWidgetItem(parent, QStringList()<<
        qPrintf("[EVAL] -> %d [%d .. %d]", value, alpha, beta) );
    if(parent->isExpanded())
        tree_widget->scrollToItem(item);
    apply_flags(item, flags);
}

void SearchGui::notify_hash(int ply, int lower, int upper, int depth, int alpha, int beta, Move best, bool write, int flags)
{
    if(ply>max_ply)
        return;

    char buf[32];
    if(best == Move::None())
        strcpy(buf, "none");
    else
        Board::move_to_coord(buf, best);
    QTreeWidgetItem *parent = tree_widget->topLevelItem(tree_widget->topLevelItemCount()-1);
    for(int i=0;i<ply;i++)
        parent = (parent && parent->childCount()) ? parent->child(parent->childCount()-1) : NULL;
    if(!parent)
        return;
    QTreeWidgetItem *item = new QTreeWidgetItem(parent, QStringList()<<
        qPrintf("[%s] -> (%d .. %d) <%s> [%d .. %d]", write?"HTWR":"HTLK", lower, upper, buf, alpha, beta) );
    if(parent->isExpanded())
        tree_widget->scrollToItem(item);
    apply_flags(item, flags);
}

#include "search_gui.moc"
