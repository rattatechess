/***************************************************************************
              array.h  -  0x88 and bounds-checked static array
                             -------------------
    begin                : Sun Sep 23 2007
    copyright            : (C) 2007 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __ARRAY_H__
#define __ARRAY_H__

#include "compiler.h"

template<typename T, int S>
class Array {
public:
#ifdef DEBUG
    class PACKED Type {
    public:
        T data[S];
        T& operator[](unsigned int index) const {
            ASSERT(index < S);
            return ((T*)data)[index];
        }
    };
#else //DEBUG
    typedef T Type[S];
#endif //DEBUG
};

#ifdef DEBUG

class PACKED A88Data {
public:
    uint8_t data[128];
    uint8_t& operator[](unsigned int index) const {
        ASSERT(!(index & (uint8_t)0x08));
        ASSERT(index < 0x80);
        return ((uint8_t*)data)[index];
    }
};

class PACKED A88 {
public:
    uint8_t* ptr;
    A88(uint8_t *p) : ptr(p) {}
    A88(const A88Data& d) : ptr((uint8_t*)d.data) {}
    uint8_t& operator[](unsigned int index) const {
        ASSERT(!(index & (uint8_t)0x08));
        ASSERT(index < 0x80);
        return ((uint8_t*)ptr)[index];
    }
};
#define DEF2_A88(a,b) A88Data a; A88Data b

#else //DEBUG

typedef uint8_t* A88;
#define DEF2_A88(a,b) union { uint8_t a[128]; struct { uint8_t _pad_for_##b[8]; uint8_t b[120]; }; }

#endif //DEBUG

inline void zero_a88(A88 d)
{
    register uint64_t *z = (uint64_t*)&d[0];
    for(int i=0;i<8;i++)
    {
        *z++ = 0;
        z++;
    }
}

#endif //__ARRAY_H__
