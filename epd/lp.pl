# scoring script for LAPUCE2 test
if (@ARGV) {
  open(DATA,@ARGV[0]) || die "Can't open file " . $_;
  $move = "";
  $time = 0;
  $total_score = 0;
  $num_correct = 0;
  while (<DATA>)
  {
     if (/^ply/)
     {
        split;
        $newmove = @_[2];
        $newtime = @_[3];
        if ($newmove ne $move)
        {
          $move = $newmove;
          $time = $newtime;
        }
     }
     elsif (/^search:/)
     {
        if (/correct/)
        {
           $num_correct++;
#           print "correct - time = " . $time . "\n";
           $score = 0;
           if ($time <= 9)
           {
             $score = 30;
           }
           elsif ($time <= 29)
           {
             $score = 25;
           }
           elsif ($time <= 89)
           {
             $score = 20;
           }
           elsif ($time <= 179)
           {
             $score = 15;
           }
           elsif ($time <= 389)
           {
             $score = 10;
           }
           else
           {
             $score = 5;
           }
           $total_score += $score;
           
           $time = 0;
           $move = "";
        }
     }
  }
  print $num_correct . " correct.\n";
#  print "time = " . $total_time . "\n";
  print "rating = " . (1900 + $total_score) . "\n";
}