1. wac.epd contains the 300 problems from Reinfeld's "Win At Chess"
book. These are mostly easy tactical problems (a few are hard). This
test suite is widely used by computer chess programmers and I
have applied all known corrections to it.

2. wcsac.epd contains problems from Reinfeld's 1001 Winning Chess
Sacrifices and Combinations. These are tactical problems similar in
difficulty to the "Win At Chess" series.

3. ecmgcp.epd contains a subset of test positions from the Encyclopedia
of Chess Middlegames, selected and corrected by Gian-Carlo Pascutto.
They are difficult middlegame problems.

4. bt2630.epd is a set of 30 rather difficult problems. This is
used to determine an approximate rating for the program. Standard
procedure is to allow 15 minutes for each problem. Add the time
needed to find the correct answer (900 for problems that are not
solved), divide by 30, and subtract from 2630. There is
a Perl script, bt.pl, in the test directory, that can process a
log file from "testsrc" and score this test.

5. lapuce2.epd is a set of 35 tests from the French chess magazine
La Puce Echiquenne. This is another test that purports to estimate
a program's rating. Standard procedure is to allow 10 minutes for
each problem. See lapuce2.doc for the scoring procedure. There is
a Perl script, lp.pl, in the test directory, that can process a
log file from "testsrc" and score this test.

6. arasan5.epd is a set of test positions from Arasan games. This is a
latest version of the test suite. Some positions in this file are
"avoid move" positions where there is a bad but superficially tempting
move.

7. iq4.epd is a set of positions from the book "Test Your Chess IQ".
Jim Monaghan selected and corrected some tests from this book.
I have made some further modifications to the test and this version,
which I use, is in the file "iq4.epd". These tests are run at 10
seconds per position.

8. pet.epd is a set of endgame tests from Peter MacKenzie, the author
of the freeware chess program "Lampchop". I have applied a few
corrections to Peter's original test suite.

The results file in the tests subdirectory summarizes Arasan's
performance on these test suites. Note: these results were obtained
from the testsrc program.

