if (@ARGV) {
  open(DATA,@ARGV[0]) || die "Can't open file " . $_;
  $move = "";
  $time = 0;
  $total_time = 0;
  $num_correct = 0;
  while (<DATA>)
  {
     if (/^ply/)
     {
        split;
        $newmove = @_[2];
        $newtime = @_[3];
        if ($newmove ne $move)
        {
          $move = $newmove;
          $time = $newtime;
        }
     }
     elsif (/^search:/)
     {
        if (/correct/)
        {
           $num_correct++;
  #         print "correct - time = " . $time . "\n";
           $total_time += $time;
           $time = 0;
           $move = "";
        }
     }
  }
  print $num_correct . " correct.\n";
  #print "time = " . $total_time . "\n";
  print "rating = " . (2630 - ((900*(30-$num_correct)) + $total_time)/30) . "\n";
}
