/***************************************************************************
                          commands.h  -  description
                             -------------------
    begin                : mer ott 23 2002
    copyright            : (C) 2007 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __COMMANDS_H__
#define __COMMANDS_H__

#include "engine.h"

/* class to store information about a command */
class Engine::Cmd
{
    private:
        typedef void (Engine::*CmdFunc)(int argc, char *argv[]);

        CmdFunc m_func;
        int m_min;
        int m_max;
        const char *m_desc;

    public:
        Cmd()
        { }

        Cmd(CmdFunc f, const char *d = NULL)
            : m_func(f)
            , m_min(0)
            , m_max(0)
            , m_desc(d)
        { }

        Cmd(CmdFunc f, int n, const char *d = NULL)
            : m_func(f)
            , m_min(n)
            , m_max(n)
            , m_desc(d)
        { }

        Cmd(CmdFunc f, int n, int x, const char *d = NULL)
            : m_func(f)
            , m_min(n)
            , m_max(x)
            , m_desc(d)
        { }

        int min() const { return m_min; }
        int max() const { return m_max; }
        const char* desc() const { return m_desc; }

        void run(int argc, char *argv[]);
};

class Engine::Variable
{
public:
    typedef void (Engine::*VarSetFunc)(void* d, const char* value);
    typedef char* (Engine::*VarGetFunc)(void* d, char* value);

    enum Type
    {
        Integer,
        String,
        Custom
    };

    int m_type;
    const char* m_desc;

    union
    {
        struct
        {
            int (Engine::*m_int_var);
            int m_range_min;
            int m_range_max;
        };
        char* (Engine::*m_string_var);
        struct
        {
            void *m_custom_data;
            VarSetFunc m_custom_set;
            VarGetFunc m_custom_get;
        };
    };

    Variable()
    { }

    Variable(int (Engine::*ivar), int def_val, const char *d = NULL);
    Variable(int (Engine::*ivar), int def_val, int min, int max, const char *d = NULL);
    Variable(char* (Engine::*svar), const char* def_val, const char *d = NULL);
    Variable(VarSetFunc set, VarGetFunc get, const char *d = NULL);
    Variable(VarSetFunc set, VarGetFunc get, void* data, const char *d = NULL);

    void from_string(const char *s);
    const char* to_string(char* buf64);
    const char* desc() { return m_desc; }
};

#endif //__COMMANDS_H__
