/***************************************************************************
                          check.cpp  -  description
                             -------------------
    begin                : Wed Mar 13 2002
    copyright            : (C) 2002-2005 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "board.h"

/*******************************************************************************
    Find the pins for color to move and set a flag to mark whick is the
    pinning direction. The under_check var is also set to a value that is
    0/1/2 if the color to move is under no check, check or double check.
*******************************************************************************/
void
Board::find_check_and_pins()
{
    ASSERT(data[king_pos[1]] == WK);
    ASSERT(data[king_pos[0]] == BK);
    ASSERT(DISTANCE(king_pos[0], king_pos[1]) >= 2);

    A88 d = data;
    A88 cg = pins;
    uint8_t inc;
    uint8_t check;
    uint8_t maybe_ink;

    uint8_t pos      = king_pos[IS_WHITE(color_to_move)];
    uint8_t attacker = other_color;
    uint8_t defender = color_to_move;
    uint8_t killer = BISHOP|attacker;

    //clear pinning mask
    zero_a88(pins);

    under_check = 0;

    /* check bishop (or queen) attacks */
    for(int i=3;i>=0;i--)
    {
        inc = bishmoves[i];
        check = pos;
        maybe_ink = 0xff;
        do
        {
            diag_cont_lab:;
            check += inc;

            if(OUT_OF_BOARD(check))
                break;

            if(COLOR_OF(d[check]) == defender)
            {
                if(maybe_ink==0xff)            //first piece on the line?
                {
                    maybe_ink = check;
                    goto diag_cont_lab;
                }
                else
                    break;                     //two non-offending pieces in the line
            }

            if( (d[check] & ((uint8_t)~ROOK)) ==  killer)
            {
                if(maybe_ink==0xff)
                {                              //under a true check, sign all
                    register uint8_t p = pos;
                    under_check++;
                    do
                    {
                        p+=inc;
                        cg[p] = NFREE;
                    }
                    while(p!=check);
                }
                else
                {
                    cg[maybe_ink] = NFREE|i|DIAG;
                }
                break;
            }
        }
        while(!d[check]);
    }

    /* check rook (or queen) attacks */
    killer = ROOK|attacker;

    for(int i=3;i>=0;i--)
    {
        inc = rookmoves[i];
        check = pos;
        maybe_ink = 0xff;
        do
        {
            col_cont_lab:;
            check += inc;

            if(OUT_OF_BOARD(check))
                break;

            if(COLOR_OF(d[check]) == defender)
            {
                if(maybe_ink==0xff)            //first piece on the line?
                {
                    maybe_ink = check;
                    goto col_cont_lab;
                }
                else
                    break;                     //two non-offending pieces in the line
            }

            if( (d[check] & ((uint8_t)~BISHOP)) ==  killer)
            {
                if(maybe_ink==0xff)
                {                              //under a true check, sign all
                    register uint8_t p = pos;
                    under_check++;
                    do
                    {
                        p+=inc;
                        cg[p] = NFREE;
                    }
                    while(p!=check);
                }
                else
                {
                    cg[maybe_ink] = NFREE|i|COLM;
                }
                break;
            }
        }
        while(!d[check]);
    }

    /* check knight attacks */
    killer = KNIGHT|attacker;
    {
        KnightMove* hm= &knightmoves[pos];
        for(int i=hm->numm;i>=0;i--)
        {
            check = hm->jump[i];
            if(d[check] == killer)
            {
                under_check++;
                cg[check] = NFREE;
            }
        }
    }

    /* check pawn attacks */
    {
        killer = PAWN|attacker;
        check = pos - up_dir[IS_WHITE(attacker)] + RIGHT;
        if(!OUT_OF_BOARD(check) && (d[check] == killer))
        {
            under_check++;
            cg[check] = NFREE;
        }

        check += 2*LEFT;
        if(!OUT_OF_BOARD(check) && (d[check] == killer))
        {
            under_check++;
            cg[check] = NFREE;
        }
    }
}


/*******************************************************************************
    Find the pins for the other color.
*******************************************************************************/
void
Board::find_other_pins()
{
    ASSERT(data[king_pos[1]] == WK);
    ASSERT(data[king_pos[0]] == BK);
    ASSERT(DISTANCE(king_pos[0], king_pos[1]) >= 2);

    A88 d = data;
    A88 cg = oth_pins;
    uint8_t inc;
    uint8_t check;
    uint8_t maybe_ink;

    uint8_t pos      = king_pos[IS_WHITE(other_color)];
    uint8_t attacker = color_to_move;
    uint8_t killer   = BISHOP|attacker;

    //clear pinning mask
    zero_a88(oth_pins);

    /* check bishop (or queen) attacks */
    for(int i=3;i>=0;i--)
    {
        inc = bishmoves[i];
        check = pos;
        maybe_ink = 0xff;

        while(1)
        {
            check += inc;

            if(OUT_OF_BOARD(check))
                break;

            if( (d[check] & ((uint8_t)~ROOK)) ==  killer)
            {
                ASSERT(maybe_ink!=0xff); //else, under a true check!
                cg[maybe_ink] = NFREE|i|DIAG;
                break;
            }
            else if(d[check])
            {
                if(maybe_ink!=0xff)
                    break;
                maybe_ink = check;
            }
        }
    }

    /* check rook (or queen) attacks */
    killer = ROOK|attacker;

    for(int i=3;i>=0;i--)
    {
        inc = rookmoves[i];
        check = pos;
        maybe_ink = 0xff;

        while(1)
        {
            check += inc;

            if(OUT_OF_BOARD(check))
                break;

            if( (d[check] & ((uint8_t)~BISHOP)) ==  killer)
            {
                ASSERT(maybe_ink!=0xff); //else, under a true check!
                cg[maybe_ink] = NFREE|i|COLM;

                break;
            }
            else if(d[check])
            {
                if(maybe_ink!=0xff)
                    break;
                maybe_ink = check;
            }
        }
    }
}

/*******************************************************************************
    Almost exact, does not handle en-passant and castle
*******************************************************************************/
bool
Board::move_is_check(const Move& m)
{
    /* discover check? */
    /* if a pawn is marked on an attack line and moves, it is almost
        always a check (unless very rare en-passant on the attack line) */
    if(oth_pins[m.from])
        return true;

    uint8_t enking = king_pos[IS_WHITE(other_color)];
    uint8_t delta = enking - m.to;
    uint8_t piece = PIECE_OF(data[m.from]);

    /* not 100% exact, promotion to queen/rook may give check backward
       and it would not be detected because of the pawn itself "interposed" */
    if(m.flags >= PROMOTE_FIRST)
        piece = m.flags-PROMOTE0;

    switch(piece)
    {
        case PAWN:
        {
            if(delta == RIGHT_OF(up_dir[IS_WHITE(color_to_move)]) ||
               delta == LEFT_OF(up_dir[IS_WHITE(color_to_move)]))
                return true;
            /* TODO:handle promote */
            break;
        }

        case KNIGHT:
        {
            int deltax = X(enking) - X(m.to);
            int deltay = Y(enking) - Y(m.to);

            /* to see if it is reachable test if the distance is sqrt(5) */
            if(deltax*deltax + deltay*deltay == 5)
                return true;
            break;
        }

        case QUEEN:
        case BISHOP:
        {
            int deltax = X(enking) - X(m.to);
            int deltay = Y(enking) - Y(m.to);
            if(ABS(deltax) == ABS(deltay))
            {
                uint8_t inc = (deltax>0?RIGHT:LEFT) + (deltay>0?UP:DOWN);
                for(uint8_t currpos = m.to+inc; currpos != enking; currpos+=inc)
                if(data[currpos])
                    return false;
                return true;
            }
            if(piece != QUEEN) /* for the queen fall through to the rook */
                break;
        }

        case ROOK:
        {
            int deltax = X(enking) - X(m.to);
            int deltay = Y(enking) - Y(m.to);
            if(ABS(deltax)==0 || ABS(deltay)==0)
            {
                uint8_t inc = deltax>0 ? RIGHT : (deltax<0 ? LEFT : (deltay>0?UP:DOWN));
                for(uint8_t currpos = m.to+inc; currpos != enking; currpos+=inc)
                    if(data[currpos])
                        return false;
                return true;
            }
            break;
        }
    }

    return false;
}
