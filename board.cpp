/***************************************************************************
                          chess.cpp  -  description
                             -------------------
    begin                : Fri Jan 11 2002
    copyright            : (C) 2002-2005 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "engine.h"
#include <stdlib.h>
#include <string.h>

Board::Board()
{
    init_knight_moves();
    set_as_default();
}

//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

void Board::set_as_default()
{
    memcpy(&data[0],b_start,128);
    memcpy(&castle_adj[0],b_castle_adj,128);

    num_moves=0;

    castle_passing_mask = 0xfc;
    fifty         = 0;
    color_to_move = WHITE;
    other_color = BLACK;
    under_check = 0xff;
    king_pos[1] = E1;
    king_pos[0] = E8;
    first_rank[1] = 0x00;
    first_rank[0] = 0x70;
    second_rank[1] = 0x10;
    second_rank[0] = 0x60;
    passant_rank[1] = 0x40;
    passant_rank[0] = 0x30;
    seventh_rank[1] = 0x60;
    seventh_rank[0] = 0x10;
    //cicci = 0;

    recalc_line_pawns();
    recalc_mat_tracking();
    recalc_hash();
#if TRACK_ATTACKS
    recalc_attacks();
#endif
}

#if 0
void Board::set_as_default()
{
    static uint8_t b_start55[] =
    {
        SENDL,SENDL,
        SENDL,SENDL,
        SENDL,SENDL,
        WR,WN,WB,WQ,WK,STONE,STONE,STONE,SENDL,
        WP,WP,WP,WP,WP,STONE,STONE,STONE,SENDL,
        __,__,__,__,__,STONE,STONE,STONE,SENDL,
        BP,BP,BP,BP,BP,STONE,STONE,STONE,SENDL,
        BR,BN,BB,BQ,BK,STONE,STONE,STONE,SENDL
    };
    static uint8_t b_castle_adj55[] =
    {
        0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,ENDL,
        0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,ENDL,
        0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,ENDL,
        0xd0,0xf0,0xf0,0xf0,0xc0,0xf0,0xf0,0xf0,ENDL,
        0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,ENDL,
        0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,ENDL,
        0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,ENDL,
        0x70,0xf0,0xf0,0xf0,0x30,0xf0,0xf0,0xf0,ENDL
    };

    memcpy(data,b_start55,128);
    memset(data+128, 0xff, 128);
    memcpy(castle_adj,b_castle_adj55,128);

    num_moves=0;

    flags_stack_ptr = 0;
    num_old_hashes = 0;

    castle_passing_mask = 0xfc;
    fifty         = 0;
    color_to_move = WHITE;
    other_color = BLACK;
    under_check = 0xff;
    king_pos[1] = E3;
    king_pos[0] = E8;
    first_rank[1] = 0x30;
    first_rank[0] = 0x70;
    second_rank[1] = 0x40;
    second_rank[0] = 0x60;
    passant_rank[1] = 0x40;
    passant_rank[0] = 0x60;
    seventh_rank[1] = 0x60;
    seventh_rank[0] = 0x40;

    recalc_line_pawns();
    recalc_mat_tracking();
    recalc_hash();
    recalc_attacks();
}

void Board::set_as_default()
{
    static uint8_t b_start68[] =
    {
        SENDL,SENDL,
        WR,WN,WB,WQ,WK,WB,WN,WR,SENDL,
        WP,WP,WP,WP,WP,WP,WP,WP,SENDL,
        __,__,__,__,__,__,__,__,SENDL,
        __,__,__,__,__,__,__,__,SENDL,
        BP,BP,BP,BP,BP,BP,BP,BP,SENDL,
        BR,BN,BB,BQ,BK,BB,BN,BR,SENDL,
        SENDL,SENDL
    };
    static uint8_t b_castle_adj68[] =
    {
        0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,ENDL,
        0xd0,0xf0,0xf0,0xf0,0xc0,0xf0,0xf0,0xe0,ENDL,
        0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,ENDL,
        0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,ENDL,
        0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,ENDL,
        0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,ENDL,
        0x70,0xf0,0xf0,0xf0,0x30,0xf0,0xf0,0xb0,ENDL,
        0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,ENDL
    };

    memcpy(data,b_start68,128);
    memset(data+128, 0xff, 128);
    memcpy(castle_adj,b_castle_adj68,128);

    num_moves=0;

    flags_stack_ptr = 0;
    num_old_hashes = 0;

    castle_passing_mask = 0xfc;
    fifty         = 0;
    color_to_move = WHITE;
    other_color = BLACK;
    under_check = 0xff;
    king_pos[1] = E2;
    king_pos[0] = E7;
    first_rank[1] = 0x10;
    first_rank[0] = 0x60;
    second_rank[1] = 0x20;
    second_rank[0] = 0x50;
    passant_rank[1] = 0x30;
    passant_rank[0] = 0x40;
    seventh_rank[1] = 0x50;
    seventh_rank[0] = 0x20;

    recalc_line_pawns();
    recalc_mat_tracking();
    recalc_hash();
    recalc_attacks();
}
#endif



/*******************************************************************************
    initialize a table with all possible horse moves
*******************************************************************************/

void Board::init_knight_moves()
{
    char poss_hmoves[8][2] = {{1,2},{-1,2},{1,-2},{-1,-2},
                              {2,1},{-2,1},{2,-1},{-2,-1}};
    if(knightmoves)
        return;

    knightmoves = new KnightMove[128];

    // For every square just check all moves;
    for(int x=0;x<8;x++)
    for(int y=0;y<8;y++)
    {
        KnightMove* current = &knightmoves [ POS_XY(x,y) ];
        unsigned char c = 0;

        for(int i=0;i<8;i++)
        {
            int a = x + poss_hmoves[i][0];
            int b = y + poss_hmoves[i][1];

            if( (a >= 0) && (a < 8) && (b >= 0) && (b < 8) )
                current->jump[c++] = POS_XY(a,b);
        }
        for(int i=c;i<8;i++)
            current->jump[i] = 0xff;
        current->numm = c-1;
    }
}

//-------------------------------------------------------------------------------------


/* create the hashkeys.cpp file from a raw file with random data (ie /dev/urandom) */
void Board::init_hash_keys(const char* file)
{
    HashKey *hash_keys = new HashKey[14*128];
    FILE *f = fopen(file, "r");
    if(!f)
    {
        printf("Error, could not read hash keys file!\n");
        return;
    }

    //printf("loaded \"hashkeys.dat\"\n");
    if(fread(hash_keys, sizeof(HashKey), 14*128, f) != 14*128)
        printf("Error, could not read hash keys file!\n");
    fclose(f);

    FILE *s = fopen("hashkeys.cpp", "w");

    fprintf(s, "/* Automatically generated, do not edit */\n");
    fprintf(s, "#include \"board.h\"\n");
    fprintf(s, "HashKey Board::hash_keys[14*128] = {\n");

    for(int i=0;i<14*128;i++)
    {
        fprintf(s, " HashKey( 0x%016llxULL, 0x%08x )",
            (unsigned long long)hash_keys[i].check, hash_keys[i].index);
        if(i!=14*128-1)
            fprintf(s, ",");
        if((i+1)%2==0)
            fprintf(s, "\n");
    }
    fprintf(s, "};\n");
    fclose(s);

    printf("hashkeys.cpp generated!\n");
}
