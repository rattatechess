/***************************************************************************
                     utils.cpp  -  sytem dependent utilities
                             -------------------
    begin                : lun ott 24 2005
    copyright            : (C) 2005-2007 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "utils.h"
#include <stdlib.h>
#include <string.h>

char **get_tokenized(int* numtokens, bool accept_quotes)
{
    return fget_tokenized(stdin, numtokens, accept_quotes);
}

char **get_tokenized(const char *whitespaces, int* numtokens, bool accept_quotes)
{
    return fget_tokenized(stdin, whitespaces, numtokens, accept_quotes);
}

char **fget_tokenized(FILE* f, int* numtokens, bool accept_quotes)
{
    return fget_tokenized(f, " \v\t\r\n", numtokens, accept_quotes);
}

char **fget_tokenized(FILE* f, const char *whitespaces, int* numtokens, bool accept_quotes)
{
    char str[4096];
    if(!fgets(str, 4096, f))
        return NULL;
    return tokenize(str, whitespaces, numtokens, accept_quotes);
}

char **tokenize(const char *str, int* numtokens, bool accept_quotes)
{
    return tokenize(str, " \v\t\r\n", numtokens, accept_quotes);
}

char **tokenize(const char *str, const char *whitespaces, int* numtokens, bool accept_quotes)
{
    int len = strlen(str);
    const char *ptr = str;
    int ntoks = 0;

    while(1)
    {
        ptr += strspn(ptr, whitespaces);
        int s = strcspn(ptr, whitespaces);

        if(!s)
            break;
        ptr += s;
        ntoks++;
    }

    //printf("ntoks = %d\n", ntoks);

    void* block = malloc((ntoks+1)*sizeof(char*) + len+1);
    char** retv = (char**)block;
    char* buf = (char*)block + (ntoks+1)*sizeof(char*);

    ptr = str;
    ntoks = 0;

    while(1)
    {
        ptr += strspn(ptr, whitespaces);
        //printf("-> \"%s\"\n", ptr);
        if(!*ptr)
            break;
        retv[ntoks++] = buf;

        bool quoted = false;
        while( *ptr && (quoted || (strchr(whitespaces, *ptr) == NULL) ) )
        {
            if(accept_quotes && *ptr == '"')
            {
                quoted = !quoted;
                ptr++;
                continue;
            }

            if(quoted && *ptr == '\\')
            {
                ptr++;
                switch(*ptr)
                {
                    case '"':
                        *buf++ = '"';
                        ptr++;
                        break;
                    case '\\':
                        *buf++ = '\\';
                        ptr++;
                        break;
                    case '\0':
                        *buf++ = '\\';
                        break;
                    default:
                        *buf++ = *ptr++;
                        break;
                }
                continue;
            }

            *buf++ = *ptr++;
        }
        *buf++ = '\0';
        if(quoted)
        {
            printf("Error! Missing terminating '\"' while parsing!\n");
            puts(str);
        }
        if(!*ptr)
            break;
    }
    if(numtokens)
        *numtokens = ntoks;
    retv[ntoks++] = NULL;
    return retv;
}
