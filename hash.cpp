/***************************************************************************
                   hash.cpp  -  Hash related implementations
                             -------------------
    begin                : Mon Sep 19 2005
    copyright            : (C) 2005 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "engine.h"
#include <string.h>

#define HSIZE (1<<22)
#define HMASK (HSIZE-1)
#define REHASH 3

void
HashKey::print() const
{
    printf("HashKey( 0x%llxLL, 0x%x )\n", (long long)check, index);
}

char* HashKey::to_string(char* buf) const
{
    snprintf(buf, 64, "%08x%08x%08x",
                      check_hi, check_lo, index);
    return buf;
}

void
Engine::create_hash()
{
    hash_table = new HashEntry[HSIZE];
}

void
Engine::reset_hash()
{
    memset(hash_table, 0, sizeof(HashEntry)*HSIZE);
}

void
Engine::make_old_hash()
{
    for(int i=0;i<HSIZE;i++)
    {
        __builtin_prefetch( &hash_table[ (i + 30) & HMASK] );
        hash_table[i].is_old = 1;
    }
}

/* is this useful? */
void
Engine::prefetch_hash(const HashKey& hk)
{
    __builtin_prefetch( &hash_table[ (hk.index) & HMASK] );
    __builtin_prefetch( &hash_table[ (hk.index+REHASH-1) & HMASK] );
}

HashEntry*
Engine::probe_hash(const HashKey& hk)
{
    for(int i=0;i<REHASH;i++)
    {
        HashEntry* h = &hash_table[(hk.index + i) & HMASK];
        if(!h->check)
            return NULL;
        if(h->check == hk.check)
        {
            h->is_old = 0;
            return h;
        }
    }
    return NULL;
}

void
Engine::write_hash(const HashKey& hk, int16_t lo, int16_t up, int depth, int best_cont, bool no_good_moves)
{
    HashEntry* h = 0;

    /* look for an empty entry or the same entry to update */
    for(int i=0;i<REHASH;i++)
    {
        HashEntry* tmp = &hash_table[(hk.index + i) & HMASK];
        if(!tmp->check || tmp->check == hk.check)
        {
            h = tmp;
            break;
        }
    }

    if(!h) /* look for an entry searched at lower depth */
    {
        int d = INF;
        int lowest = -1;
        for(int i=0;i<REHASH;i++)
        {
            HashEntry* tmp = &hash_table[(hk.index + i) & HMASK];
            if(tmp->depth < d)
            {
                lowest = i;
                d = tmp->depth;
            }
        }

        if(d < depth)
            h = &hash_table[(hk.index + lowest) & HMASK];
    }

    if(!h) /* look for an old entry and take the one searched at lowest depth */
    {
        int d = INF;
        int lowest = -1;
        for(int i=0;i<REHASH;i++)
        {
            HashEntry* tmp = &hash_table[(hk.index + i) & HMASK];
            if(tmp->depth < d && tmp->is_old == 1)
            {
                lowest = i;
                d = tmp->depth;
            }
        }

        if(d != INF)
            h = &hash_table[(hk.index + lowest) & HMASK];
    }

    if(!h)
        return;
#if 0
    if(h->check == hk.check && h->depth>depth)
        return;

    if(h->check == hk.check && h->depth==depth)
    {
        /* same entry, improve bounds */
        h->up = MIN(h->up, up);
        h->lo = MAX(h->lo, lo);
    }
    else
    {
        /* replace bounds */
        h->up = up;
        h->lo = lo;
    }
#endif

    h->up = up;
    h->lo = lo;

    h->check = hk.check;
    h->depth = depth;
    h->best_mv = best_cont;
    h->no_good_moves = no_good_moves;
    h->is_old = 0;
}
