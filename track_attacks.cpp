/***************************************************************************
                          evaluate.cpp  -  description
                             -------------------
    begin                : Tue Nov 01 2005
    copyright            : (C) 2005 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "board.h"
#include <string.h>

#if TRACK_ATTACKS

//                               1    2     4      8             16                 32         64        128
uint8_t Board::attack_dirs[] = { UP, DOWN, LEFT, RIGHT, (uint8_t)(UP+LEFT), (uint8_t)(DOWN+LEFT),
                                                            (uint8_t)(DOWN+RIGHT), (uint8_t)(UP+RIGHT) };

void
Board::print_attacks()
{
    for(int y=7;y>=0;y--)
    for(int x=0;x<16;x++)
    {
        A88 attacks = x >= 8 ? w_attacks : b_attacks;
        uint8_t p = POS_XY(x%8,y);
        if(attacks[p] == 0)
            printf(".");
        else if(attacks[p] == 8)
            printf(">");
        else if(attacks[p] == 4)
            printf("<");
        else if(attacks[p] == 1)
            printf("^");
        else if(attacks[p] == 2)
            printf("v");
        else if(attacks[p] == 3)
            printf("|");
        else if(attacks[p] == 12)
            printf("-");
        else if(attacks[p] == 16 || attacks[p] == 64)
            printf("\\");
        else if(attacks[p] == 32 || attacks[p] == 128)
            printf("/");
        else if(!(attacks[p] & 0x0f))
            printf("x");
        else if(!(attacks[p] & 0xf0))
            printf("+");
        else
            printf("*");

        if(x==7)
            printf(" ");
        if(x==15)
            printf("\n");
    }
}

void
Board::check_attacks()
{
    struct { DEF2_A88(old_b, old_w); } a;

    for(int i=0;i<8;i++)
    for(int j=0;j<8;j++)
    {
        uint8_t p = POS_XY(i,j);
        a.old_b[p] = b_attacks[p];
        a.old_w[p] = w_attacks[p];
    }

    recalc_attacks();

    for(int i=0;i<8;i++)
    for(int j=0;j<8;j++)
    {
        uint8_t p = POS_XY(i,j);
        ASSERT(a.old_b[p] == b_attacks[p]);
        ASSERT(a.old_w[p] == w_attacks[p]);
        (void)(p);
    }
}

void
Board::recalc_attacks()
{
    zero_a88(b_attacks);
    zero_a88(w_attacks);

    for(int i=0;i<mat_tracking[BB_12].count;i++)
        add_attacks(BB, mat_tracking[BB_12].pos[i]);
    for(int i=0;i<mat_tracking[BR_12].count;i++)
        add_attacks(BR, mat_tracking[BR_12].pos[i]);
    for(int i=0;i<mat_tracking[BQ_12].count;i++)
        add_attacks(BQ, mat_tracking[BQ_12].pos[i]);
    for(int i=0;i<mat_tracking[WB_12].count;i++)
        add_attacks(WB, mat_tracking[WB_12].pos[i]);
    for(int i=0;i<mat_tracking[WR_12].count;i++)
        add_attacks(WR, mat_tracking[WR_12].pos[i]);
    for(int i=0;i<mat_tracking[WQ_12].count;i++)
        add_attacks(WQ, mat_tracking[WQ_12].pos[i]);

    //print_attacks();
}

void
Board::add_attacks(uint8_t piece, uint8_t where)
{
    A88 data = this->data;

    /* update the attacks passing through this place */
    for(int is_white = 0; is_white<2; is_white++)
    {
        A88 attacks = is_white ? w_attacks : b_attacks;

        if(!attacks[where])
            continue;

        for(int d=0;d<8;d++)
        {
            uint8_t m = 1<<d;

            if(!(attacks[where] & m))
                continue;

            uint8_t inc = attack_dirs[d];
            uint8_t currpos = where+inc;
            m = ~m;

            while(!OUT_OF_BOARD(currpos))
            {
                /* clear the "attack from this direction" flag */
                attacks[currpos] &= m;

                if(data[currpos])
                    break;

                currpos += inc;
            }
        }
    }

    A88 attacks = IS_WHITE(piece) ? w_attacks : b_attacks;

    /* if ROOK-like movements... */
    if( (PIECE_OF(piece) & ~BISHOP) == ROOK)
    {
        for(int d=0;d<4;d++)
        {
            uint8_t m = 1<<d;
            uint8_t inc = attack_dirs[d];
            uint8_t currpos = where+inc;

            while(!OUT_OF_BOARD(currpos))
            {
                /* set the "attack from this direction" flag */
                attacks[currpos] |= m;

                if(data[currpos])
                    break;

                currpos += inc;
            }
        }
    }

    /* if BISHOP-like movements... */
    if( (PIECE_OF(piece) & ~ROOK) == BISHOP)
    {
        for(int d=4;d<8;d++)
        {
            uint8_t m = 1<<d;
            uint8_t inc = attack_dirs[d];
            uint8_t currpos = where+inc;

            while(!OUT_OF_BOARD(currpos))
            {
                /* set the "attack from this direction" flag */
                attacks[currpos] |= m;

                if(data[currpos])
                    break;

                currpos += inc;
            }
        }
    }
}

void
Board::del_attacks(uint8_t piece, uint8_t where)
{
    A88 data = this->data;
    A88 attacks = IS_WHITE(piece) ? w_attacks : b_attacks;

    /* if ROOK-like movements... */
    if( (PIECE_OF(piece) & ~BISHOP) == ROOK)
    {
        for(int d=0;d<4;d++)
        {
            uint8_t m = ~(1<<d);
            uint8_t inc = attack_dirs[d];
            uint8_t currpos = where+inc;

            while(!OUT_OF_BOARD(currpos))
            {
                /* clear the "attack from this direction" flag */
                attacks[currpos] &= m;

                if(data[currpos])
                    break;

                currpos += inc;
            }
        }
    }

    /* if BISHOP-like movements... */
    if( (PIECE_OF(piece) & ~ROOK) == BISHOP)
    {
        for(int d=4;d<8;d++)
        {
            uint8_t m = ~(1<<d);
            uint8_t inc = attack_dirs[d];
            uint8_t currpos = where+inc;

            while(!OUT_OF_BOARD(currpos))
            {
                /* clear the "attack from this direction" flag */
                attacks[currpos] &= m;

                if(data[currpos])
                    break;

                currpos += inc;
            }
        }
    }

    /* update the attacks passing through this place */
    for(int is_white = 0; is_white<2; is_white++)
    {
        A88 attacks = is_white ? w_attacks : b_attacks;

        if(!attacks[where])
            continue;

        for(int d=0;d<8;d++)
        {
            uint8_t m = 1<<d;

            if(!(attacks[where] & m))
                continue;

            uint8_t inc = attack_dirs[d];
            uint8_t currpos = where+inc;

            while(!OUT_OF_BOARD(currpos))
            {
                /* clear the "attack from this direction" flag */
                attacks[currpos] |= m;

                if(data[currpos])
                    break;

                currpos += inc;
            }
        }
    }
}

void
Board::replace_attacks(uint8_t piece1, uint8_t piece2, uint8_t where)
{
    A88 attacks = IS_WHITE(piece1) ? w_attacks : b_attacks;

    /* if ROOK-like movements... */
    if( (PIECE_OF(piece1) & ~BISHOP) == ROOK)
    {
        for(int d=0;d<4;d++)
        {
            uint8_t m = ~(1<<d);
            uint8_t inc = attack_dirs[d];
            uint8_t currpos = where+inc;

            while(!OUT_OF_BOARD(currpos))
            {
                /* clear the "attack from this direction" flag */
                attacks[currpos] &= m;

                if(data[currpos])
                    break;

                currpos += inc;
            }
        }
    }

    /* if BISHOP-like movements... */
    if( (PIECE_OF(piece1) & ~ROOK) == BISHOP)
    {
        for(int d=4;d<8;d++)
        {
            uint8_t m = ~(1<<d);
            uint8_t inc = attack_dirs[d];
            uint8_t currpos = where+inc;

            while(!OUT_OF_BOARD(currpos))
            {
                /* clear the "attack from this direction" flag */
                attacks[currpos] &= m;

                if(data[currpos])
                    break;

                currpos += inc;
            }
        }
    }


    attacks = IS_WHITE(piece2) ? w_attacks : b_attacks;

    /* if ROOK-like movements... */
    if( (PIECE_OF(piece2) & ~BISHOP) == ROOK)
    {
        for(int d=0;d<4;d++)
        {
            uint8_t m = 1<<d;
            uint8_t inc = attack_dirs[d];
            uint8_t currpos = where+inc;

            while(!OUT_OF_BOARD(currpos))
            {
                /* set the "attack from this direction" flag */
                attacks[currpos] |= m;

                if(data[currpos])
                    break;

                currpos += inc;
            }
        }
    }

    /* if BISHOP-like movements... */
    if( (PIECE_OF(piece2) & ~ROOK) == BISHOP)
    {
        for(int d=4;d<8;d++)
        {
            uint8_t m = 1<<d;
            uint8_t inc = attack_dirs[d];
            uint8_t currpos = where+inc;

            while(!OUT_OF_BOARD(currpos))
            {
                /* set the "attack from this direction" flag */
                attacks[currpos] |= m;

                if(data[currpos])
                    break;

                currpos += inc;
            }
        }
    }
}

#endif //TRACK_ATTACKS
