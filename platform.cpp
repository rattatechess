/***************************************************************************
       platform.cpp  -  Platoform dependent utilities, implementation
                             -------------------
    begin                : Sun Sep 28 2007
    copyright            : (C) 2007 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#if defined(_WIN32) || defined(_WIN64)

#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include "platform.h"

static bool pipe;
static HANDLE handle;

void init_stdin()
{
    DWORD val;

    handle = GetStdHandle(STD_INPUT_HANDLE);
    pipe = !GetConsoleMode(handle,&val);

    if (!pipe)
    {
        SetConsoleMode(handle, val & ~(ENABLE_MOUSE_INPUT|ENABLE_WINDOW_INPUT));
        FlushConsoleInputBuffer(handle);
    }
}

bool input_available() {
    DWORD val;

    if (stdin->_cnt > 0)
        return true;

    if(pipe)
    {
        if(!PeekNamedPipe(handle, NULL, 0, NULL, &val, NULL) )
            return true;
        return val > 0;
    }
    else
    {
        GetNumberOfConsoleInputEvents(handle, &val);
        return val > 1;
    }

    return false;
}

int current_time()
{
    return GetTickCount() / 10;
}

bool start_engine(const char *prog, FILE **in, FILE **out)
{
    printf("Error, start_engine unimplemented on windows!\n");
    return false;
}

void *map_file(const char *file_name, uint32_t *size)
{
    HANDLE file, fmap;
    uint64_t offset = 0;
    DWORD hiword, loword;
    void *retv;

    file = CreateFile(file_name, GENERIC_READ, FILE_SHARE_READ|FILE_SHARE_WRITE,
                                    NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if(file==INVALID_HANDLE_VALUE)
        return NULL;

    loword = GetFileSize(file, &hiword);
    if(loword == INVALID_FILE_SIZE)
    {
        CloseHandle(file);
        return(NULL);
    }
    *size = loword | ((uint64_t)hiword<<32);

    fmap = CreateFileMapping(file, NULL, PAGE_READONLY, 0, 0, NULL);
    if(fmap == INVALID_HANDLE_VALUE)
    {
        CloseHandle(file);
        return NULL;
    }

    retv = MapViewOfFile(fmap, FILE_MAP_READ, (DWORD)(offset >> 32), (DWORD)(offset % 0xffffffff), 0);

    CloseHandle(fmap);
    CloseHandle(file);
    return retv;
}

void unmap_file(void *addr, uint32_t size)
{
    UnmapViewOfFile(addr);
}

#else //defined(_WIN32) || defined(_WIN64)

#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include "platform.h"

void init_stdin()
{
}

bool input_available()
{
    int n = fileno(stdin);
    fd_set ifds;
    fd_set efds;
    struct timeval tv = { 0, 0 };

    FD_ZERO(&ifds);
    FD_SET(n, &ifds);
    FD_ZERO(&efds);
    FD_SET(n, &efds);

    while(select(n+1, &ifds, NULL, &efds, &tv)==-1)
    if(errno != EINTR)
        printf("select: %s\n", strerror(errno));

    return FD_ISSET(0, &ifds) || FD_ISSET(0, &efds);
}

int current_time()
{
    timeval tv;
    gettimeofday(&tv,NULL);
    return tv.tv_sec*100 + tv.tv_usec/10000;
}

void *map_file(const char *file_name, uint32_t *size)
{
    struct stat st;
    void *addr;
    int fd;

    fd = open( file_name, O_RDONLY);
    if(fd == -1)
        return NULL;

    fstat(fd, &st);
    if(size)
     *size = st.st_size;

    addr = mmap( NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if(addr == (void*)-1)
    {
        close(fd);
        return NULL;
    }

    close(fd);
    return addr;
}

void unmap_file(void*addr, uint32_t size)
{
    munmap(addr, size);
}

bool start_engine(const char *prog, FILE **in, FILE **out)
{
    int pin[2];
    int pout[2];
    pipe(pin);
    pipe(pout);
    if(!fork())
    {
        dup2(pout[0], 0);
        dup2(pin[1], 1);
        dup2(pin[1], 2);
        execlp(prog, prog, NULL);
        fprintf(stderr, "Error, could not run %s!\n", prog);
        exit(0);
    }
    *in = fdopen(pin[0], "r");
    *out = fdopen(pout[1], "w");
    setbuf(*in, NULL);
    setbuf(*out, NULL);
    fprintf(*out, "xboard\n");
    fprintf(*out, "protover 2\n");
    //fprintf(*out, "post\n");
    fprintf(*out, "easy\n");
    fprintf(*out, "new\n");
    fprintf(*out, "force\n");

    return true;
}
#endif //defined(_WIN32) || defined(_WIN64)
