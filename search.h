/***************************************************************************
                 platform.h  -  Platoform dependent utilities
                             -------------------
    begin                : Sun Oct 07 2007
    copyright            : (C) 2007 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __SEARCH_H__
#define __SEARCH_H__

#include "engine.h"

#define MAX_PV 80
#define PLY    100

/* a prime number. Probably no good reason, i just like primes */
// #define PLUTO_SIZE ((1<<16)+1)
// #define MP(_x) ({int x = _x; x = (x - (x>>16))&0xffff; x<0?x+PLUTO_SIZE:x;})
//#define PLUTO_SIZE 577177
#define PLUTO_SIZE 277177
#define MP(x) ((int)(((unsigned int)(x))%PLUTO_SIZE))

#define CAPT_INDEX(m) ({Move _m=(m);_m.capture*128*128 + _m.from*128 + _m.to;})
#define PLUTO_INDEX(a,b) MP(CAPT_INDEX(a)*9829+CAPT_INDEX(b))
#define PLUTO2_INDEX(a,b,c) MP(CAPT_INDEX(a)*9829+CAPT_INDEX(b)*67+CAPT_INDEX(c))
//#define PLUTO2_INDEX(a,b,c) (CAPT_INDEX(a)*9829+CAPT_INDEX(b)*49+CAPT_INDEX(c))%PLUTO_SIZE

#define PLUTO1(a)    MP(CAPT_INDEX(a)*9829)
#define PLUTO2(a,b)  MP(CAPT_INDEX(a)*9829+CAPT_INDEX(b)*67)
#define FAST_PLUTO(p,m) MP(p+m)

#define HISTORY_SIZE (64*64)
#define HISTORY_INDEX(m) (SQUARE_TO_64(m.from)*64 + SQUARE_TO_64(m.to))

#define WORST_MATE (INF-5000)

class SearchRoot
{
public:
    class SearchStack
    {
    public:
        Move *moves;       /* generated moves */
        int   num_moves;   /* num of moves */
        int   curr_move;   /* the move being currently analyzed */
        bool     under_check;
        int      best_move;
        SaveBuf  save_buf;
        Move     null_threat;
        bool     null_threat_dangerous;
        uint8_t  escape_from[2];
    };

    Move mv_stack[200*MAX_PV];
    int mv_stack_top;
    SearchStack stack[MAX_PV];
    Move mv_pv[MAX_PV];
    int mv_pv_top;

    int base_depth;

    SearchGui *search_gui;
    Engine *engine;
    Board board;

    static uint16_t pluto_tot[PLUTO_SIZE];
    static uint16_t pluto_hit[PLUTO_SIZE];
    static uint16_t pluto2_tot[PLUTO_SIZE];
    static uint16_t pluto2_hit[PLUTO_SIZE];
    static uint16_t history_tot[HISTORY_SIZE];
    static uint16_t history_hit[HISTORY_SIZE];

    bool    check_draw(int curr_ply);
    bool    check_repetition(int curr_ply, int nfold);
    bool    null_move_ok();
    void    moves_heuristic(Move *mv, int num_mv, bool pv, int ply,
                        int orig_depth, Move best_mv_hash, bool quiesce,
                        Move* prev, int* overall_extensions,
                        int p1, int p2);
    void    moves_quiescence_heuristic(Move *mv, int num_mv, const Move& hash,
                        int static_eval, int alpha, int beta, int depth, Move* prev);
    int16_t search(int ply, int depth, bool pv,
                   int16_t alpha, int16_t beta, bool expect_allbad);
    SearchRoot(Engine* e, const Board& b);
    ~SearchRoot();
};

#endif //__SEARCH_H__
