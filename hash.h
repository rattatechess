/***************************************************************************
                          hash.h  -  Hash related definitions
                             -------------------
    begin                : Sun Sep 28 2007
    copyright            : (C) 2007 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __HASH_H__
#define __HASH_H__

#include "utils.h"

/* Yes, 96 bits hash keys.
   the 'index' field is used as index in the hashtable,
   the 'check' field to check the indexed position.
   Now, i don't want event to think about possible key collisions :) */
class PACKED HashKey
{
public:
    union
    {
        uint64_t check;
        struct {
            uint32_t check_lo;
            uint32_t check_hi;
        };
    };
    uint32_t index;

    HashKey(){}
    HashKey(uint64_t c, uint32_t i = 0) :
            check(c), index(i) {}
    HashKey(uint32_t ch, uint32_t cl, uint32_t i) :
            check_lo(cl), check_hi(ch), index(i) {}

    HashKey operator^(const HashKey& h)  const  {
        return HashKey(check ^ h.check, index ^ h.index);    }
    const HashKey& operator^=(const HashKey& h)    {
        check ^= h.check;
        index ^= h.index;
        return *this;
    }
    bool operator==(const HashKey& h) const  {
        return (check == h.check) && (index == h.index);    }
    bool operator!=(const HashKey& h) const   {
        return (check != h.check) || (index != h.index);    }
    bool operator<(const HashKey& h) const  {
        return check<h.check ? true :
                (check==h.check && index<h.index) ? true :
                false; }
    void print() const;
    char* to_string(char*) const;
};

/* An entry in the hashtable */
class PACKED HashEntry
{
public:
    /* field to verify the entry against a position */
    uint64_t check;
    int16_t depth;
    uint8_t is_old : 1;
    uint8_t no_good_moves : 1;
    uint16_t best_mv : 14;
    int16_t lo;
    int16_t up;

    int16_t lower(){ return lo; }
    int16_t upper(){ return up; }
};

#endif //__HASH_H__
