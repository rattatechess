/***************************************************************************
                board_defs.h  -  Board related definitions
                             -------------------
    begin                : Sun Sep 28 2007
    copyright            : (C) 2007 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __BOARD_DEFS_H__
#define __BOARD_DEFS_H__


#define INF 30000


/*******************************************************************************
    the chessboard is a 16x8 arrays, and is accessed with 1-byte indices
    whose binary form look like: 0yyy0xxx
    where xxx and yyy are 3bits (0 ... 7) that store the x and y coords.
    The 4th and 8th bits are 0 if the index is valid, so an invalid (out
    of board) index can be detected testing with 10001000 (0x88)

    Chess pieces. Keep these numbers because in a few place it is useful to be
    able to assume that QUEEN=ROOK|BISHOP, ie if you want to test if a piece
    moves diagonally you have just to test (piece&(~ROOK) == BISHOP)
*******************************************************************************/
#define ROOK   1
#define BISHOP 2
#define QUEEN  3
#define KNIGHT 4
#define PAWN   5
#define KING   6

#define STONE ((uint8_t)0xff)
#define ENDL 0,0,0,0,0,0,0,0
#define SENDL STONE,STONE,STONE,STONE,STONE,STONE,STONE,STONE

#define INVALID ((uint8_t)0xff)

// color bits
#define WHITE ((uint8_t)0x80)
#define BLACK ((uint8_t)0x40)

// flags to mark pinning information
#define NFREE  ((uint8_t)0x20)
#define CLFREE ((~NFREE)+(~(NFREE<<8))+(~(NFREE<<16))+(~(NFREE<<24)))
#define DIAG ((uint8_t)0x80)
#define COLM ((uint8_t)0x40)

#define I(a) ((uint8_t)(a))

// define directions
#define UP    ((uint8_t)0x10)
#define DOWN  ((uint8_t)0xf0)
#define RIGHT ((uint8_t)0x01)
#define LEFT  ((uint8_t)0xff)

#define RIGHT_OF(a) ((uint8_t)(RIGHT+a))
#define LEFT_OF(a) ((uint8_t)(LEFT+a))
#define UP_OF(a) ((uint8_t)(UP+a))
#define DOWN_OF(a) ((uint8_t)(DOWN+a))
#define RIGHTUP_OF(a) ((uint8_t)(RIGHT+UP+a))
#define LEFTUP_OF(a) ((uint8_t)(LEFT+UP+a))
#define RIGHTDOWN_OF(a) ((uint8_t)(RIGHT+DOWN+a))
#define LEFTDOWN_OF(a) ((uint8_t)(LEFT+DOWN+a))

// macros to work with 1-byte coordinates
#define POS_XY(x,y)       ((uint8_t)((y)<<4)+(x))
#define OTHER_COLOR(a)    (((uint8_t)0xc0) - (a))
#define IS_OF_COLOR(a,c)  ((a) & (c))
#define COLOR_OF(a)       (((uint8_t)0xc0) & (a))
#define PIECE_OF(a)       (((uint8_t)0x0f) & (a))
#define ROW_OF(a)         (a & ((uint8_t)0xf0))
#define COL_OF(a)         (a & ((uint8_t)0x0f))
#define Y(a)              ((uint8_t)(((a) & ((uint8_t)0xf0))>>4))
#define X(a)              ((a) & ((uint8_t)0x0f))
#define CAN_MOVE(a)       (! ((a) & NFREE))
#define IS_VOID(a)        (!(a))
#define IS_PIECE(a,b)     (((a) & ((uint8_t)0x0f)) ==(b))
#define OUT_OF_BOARD(a)   ((a) & ((uint8_t)0x88))
#define IS_WHITE(a)       ((uint8_t)((a)>>7))
#define PIECE_TO_12(p)    ({uint8_t _p = (p); uint8_t _r = PIECE_OF(_p)-1 + 6*IS_WHITE(_p);ASSERT(_r<12);_r;})
#define SQUARE_TO_64(s)   ({uint8_t _s = (s); ASSERT(!OUT_OF_BOARD(_s)); X(_s) + (Y(_s)<<3);})
#define SQUARE_FROM_64(s) ({uint8_t _s = (s); (0x07&_s) + ((0x38&_s)<<1);})
#define DISTANCE(a, b)    ({int _x = X(a)-X(b); int _y = Y(a)-Y(b); MAX(ABS(_x), ABS(_y));})
#define DISTANCE_2(a, b)  ({int _x = X(a)-X(b); int _y = Y(a)-Y(b); _x*_x + _y*_y;})

// flag of the move (flags) field to store
// if the move is en-passant, castle, a pawn 2-push or a promotion
#define ENPASSANT       1
#define CASTLEKINGSIDE  2
#define CASTLEQUEENSIDE 3
#define PAWNOF2         4
#define PROMOTE0        4
#define PROMOTE_FIRST   5
#define PROMOTEROOK     (ROOK+4)
#define PROMOTEBISHOP   (BISHOP+4)
#define PROMOTEQUEEN    (QUEEN+4)
#define PROMOTEKNIGHT   (KNIGHT+4)
#define PROMOTE_LAST    8

//flags of the castle-passing mask
#define NOT_PASSING     ((uint8_t)0x08)
#define WCANCASTLEKS    ((uint8_t)0x10)
#define WCANCASTLEQS    ((uint8_t)0x20)
#define BCANCASTLEKS    ((uint8_t)0x40)
#define BCANCASTLEQS    ((uint8_t)0x80)

/* 0 -> 11 pieces representation */
#define BR_12 0
#define BB_12 1
#define BQ_12 2
#define BN_12 3
#define BP_12 4
#define BK_12 5
#define WR_12 6
#define WB_12 7
#define WQ_12 8
#define WN_12 9
#define WP_12 10
#define WK_12 11

// pieces shorcuts
#define __ 0
#define WN WHITE|KNIGHT
#define WB WHITE|BISHOP
#define WR WHITE|ROOK
#define WP WHITE|PAWN
#define WQ WHITE|QUEEN
#define WK WHITE|KING
#define BN BLACK|KNIGHT
#define BB BLACK|BISHOP
#define BR BLACK|ROOK
#define BP BLACK|PAWN
#define BQ BLACK|QUEEN
#define BK BLACK|KING

/* define chess squares values */
#define A1 POS_XY(0,0)
#define A2 POS_XY(0,1)
#define A3 POS_XY(0,2)
#define A4 POS_XY(0,3)
#define A5 POS_XY(0,4)
#define A6 POS_XY(0,5)
#define A7 POS_XY(0,6)
#define A8 POS_XY(0,7)
#define B1 POS_XY(1,0)
#define B2 POS_XY(1,1)
#define B3 POS_XY(1,2)
#define B4 POS_XY(1,3)
#define B5 POS_XY(1,4)
#define B6 POS_XY(1,5)
#define B7 POS_XY(1,6)
#define B8 POS_XY(1,7)
#define C1 POS_XY(2,0)
#define C2 POS_XY(2,1)
#define C3 POS_XY(2,2)
#define C4 POS_XY(2,3)
#define C5 POS_XY(2,4)
#define C6 POS_XY(2,5)
#define C7 POS_XY(2,6)
#define C8 POS_XY(2,7)
#define D1 POS_XY(3,0)
#define D2 POS_XY(3,1)
#define D3 POS_XY(3,2)
#define D4 POS_XY(3,3)
#define D5 POS_XY(3,4)
#define D6 POS_XY(3,5)
#define D7 POS_XY(3,6)
#define D8 POS_XY(3,7)
#define E1 POS_XY(4,0)
#define E2 POS_XY(4,1)
#define E3 POS_XY(4,2)
#define E4 POS_XY(4,3)
#define E5 POS_XY(4,4)
#define E6 POS_XY(4,5)
#define E7 POS_XY(4,6)
#define E8 POS_XY(4,7)
#define F1 POS_XY(5,0)
#define F2 POS_XY(5,1)
#define F3 POS_XY(5,2)
#define F4 POS_XY(5,3)
#define F5 POS_XY(5,4)
#define F6 POS_XY(5,5)
#define F7 POS_XY(5,6)
#define F8 POS_XY(5,7)
#define G1 POS_XY(6,0)
#define G2 POS_XY(6,1)
#define G3 POS_XY(6,2)
#define G4 POS_XY(6,3)
#define G5 POS_XY(6,4)
#define G6 POS_XY(6,5)
#define G7 POS_XY(6,6)
#define G8 POS_XY(6,7)
#define H1 POS_XY(7,0)
#define H2 POS_XY(7,1)
#define H3 POS_XY(7,2)
#define H4 POS_XY(7,3)
#define H5 POS_XY(7,4)
#define H6 POS_XY(7,5)
#define H7 POS_XY(7,6)
#define H8 POS_XY(7,7)

#endif //__BOARD_DEFS_H__
