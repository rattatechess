/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Fri Jan 11 2002
    copyright            : (C) 2002-2007 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "board.h"
#include "engine.h"

//--------------------------------------------------------------------------------------------


int main(int argc, char *argv[])
{
#if defined(_WIN32) || defined(_WIN64)
    putenv("HOME=C:");
#endif

    init_stdin();
    //setvbuf(stdin, NULL, _IOLBF, 0);
    setbuf(stdin, NULL);
    setvbuf(stdout, NULL, _IOLBF, 0);

    bool message = false;
    Engine *eng = new Engine();

    /*#define PRINT_SIZE(c) printf("sizeof(" #c ") = %ld\n", sizeof(c))
    PRINT_SIZE(Move);
    PRINT_SIZE(HashKey);
    PRINT_SIZE(HashEntry);
    PRINT_SIZE(LinePawns);
    PRINT_SIZE(MatTrack);*/

    if(argc >=2 && !strcmp(argv[1], "-a"))
        eng->autotune();

    if(argc >=4 && !strcmp(argv[1], "-b"))
    {
        int tot_nodes = 0;
        eng->set_depth(atoi(argv[2]));
        for(int i=3;i<argc;i++)
        {
            FILE* f = fopen(argv[i],"r");
            if(f)
            {
                eng->read_board(f);
                fclose(f);

                eng->start();
                printf("\nanalyzing %s\n", argv[i]);
                eng->find_best_move();
                tot_nodes += eng->processed_nodes;
            }
        }
        printf("\ntot processed nodes is %d\n", tot_nodes);
        return 0;
    }

    if(argc >=3 && !strcmp(argv[1], "-e"))
    {
        for(int i=2;i<argc;i++)
        {
            FILE* f = fopen(argv[i],"r");
            if(f)
            {
                eng->read_board(f);
                fclose(f);
                eng->print_board();
            }
        }
        return 0;
    }

#if 0
    eng->print_board();
    eng->set_depth(9);
    eng->read_board("r2rb1k1/pp1q1p1p/2n1p1p1/2bp4/5P2/PP1BPR1Q/1BPN2PP/R5K1 w - - 0 1");
    eng->start();
    eng->find_best_move();
#endif

    if(!eng->io_xboard)
        eng->print_board();

    int prev_move_index = 999;
    int prev_move_val = 0;

    while(1)
    {
        if(eng->eng_status==ANALYZING)
        {
            eng->analyze();
        }
        else if(eng->computer_color() != eng->color_to_move()
            || eng->get_status() != PLAYING)  //wait for command or move...
        {
            if(eng->ponder && eng->eng_status==PLAYING &&
                    eng->status == PLAYING &&
                    eng->st_computer_color == eng->board.other_color)
                eng->do_ponder();
            else
                eng->process_input();
        }
        else
        {
            if(prev_move_index > eng->mv_done_num)
                prev_move_val = +INF;
            prev_move_index = eng->mv_done_num;

            Move mv = eng->find_best_move();
            int t = current_time();         ////
            if(t < eng->max_think_time-5)        ////
            {
                //printf("# will simulate a human think %d centiseconds\n", 50*(eng->max_think_time-t));
                //usleep(5000*(eng->max_think_time-t));      ////
            }

////            if(mv.val > prev_move_val+200 && mv.capture && PIECE_OF(mv.capture) != PAWN)
////                eng->output("tellicsnoalias say Chomp!\n");

            prev_move_val = mv.val;

            if(!eng->io_xboard)
                eng->print_moves(&mv,1);

            if(eng->io_xboard && !eng->io_san)
                eng->output("move %s\n",eng->board.move_to_coord(MoveStr().data(), mv));
            else
                eng->output("move %s\n",eng->board.move_to_alg(MoveStr().data(), mv));
            eng->move(mv);

            if(!eng->io_xboard)
                eng->print_board();
            message=true;
        }

        if(eng->get_status()!=PLAYING && message)
        {
            message=false;
            eng->output("%s {%s}\n", eng->status==_01?"0-1":
                                  eng->status==_10?"1-0":"1/2-1/2",
                                  eng->status_string );
        }
    }
}
