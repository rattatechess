/***************************************************************************
                          move.cpp  -  description
                             -------------------
    begin                : lun ott 21 2002
    copyright            : (C) 2002-2005 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include "engine.h"
#include "board.h"
#include <stdio.h>

#define HASH_KEY(piece, square) hash_keys[ PIECE_TO_12(piece)*64 + SQUARE_TO_64(square) ]

//-------------------------------------------------------------------------------------

/* return a 14 bits integer with the move */
uint16_t Board::compress_move(const Move& m) const
{
    uint16_t retv = SQUARE_TO_64(m.from) | (SQUARE_TO_64(m.to)<<6);

    if(m.flags >= PROMOTE_FIRST && m.flags <= PROMOTE_LAST)
    {
        retv |= (m.flags-PROMOTE0)<<12;
    }
    return retv;
}

Move Board::uncompress_move(uint16_t m) const
{
    Move retv;
    retv.from = SQUARE_FROM_64(m);
    retv.to = SQUARE_FROM_64(m>>6);
    retv.capture = data[retv.to];
    retv.flags = 0;

    if(PIECE_OF(data[retv.from]) == PAWN)
    {
        if(ABS(Y(retv.from)-Y(retv.to))==2)
            retv.flags = PAWNOF2;
        else if(X(retv.from) != X(retv.to) && !retv.capture)
            retv.flags = ENPASSANT;
        else if(Y(retv.to) == (color_to_move==WHITE?7:0))
            retv.flags = (m>>12) + PROMOTE0;
    }
    else if(PIECE_OF(data[retv.from]) == KING)
    {
        if(X(retv.from)==4 && X(retv.to)==6)
            retv.flags = CASTLEKINGSIDE;
        else if(X(retv.from)==4 && X(retv.to)==2)
            retv.flags = CASTLEQUEENSIDE;
    }
    return retv;
}

void Board::recalc_hash()
{
    hash = HashKey(0,0);

    int piece_pos = 128;
    for(int i=7;i>=0;i--)
    {
        piece_pos -= 8;
        for(int j=7;j>=0;j--)
        {
            piece_pos--;

            if(data[piece_pos])
                hash ^= HASH_KEY( data[piece_pos],  piece_pos );
        }
    }

    /* adjust castle and en-passant */
    hash ^= hash_keys[ 12*64 + castle_passing_mask ];

    /* adjust player to move */
    if(color_to_move == BLACK)
        hash ^= hash_key_toggle;
}

/* return the hash key that the board would have after playing the move */
HashKey Board::move_hash(const Move& m) const
{
    uint8_t newcpm;

    /* adjust hash key */
    HashKey h =  hash ^ hash_key_toggle
            ^ HASH_KEY( data[m.from],  m.to )
            ^ HASH_KEY( data[m.from],  m.from );
    if(m.capture)
            h ^= HASH_KEY( m.capture,  m.to );


    /* reset castle/en passant flags*/
    newcpm = 12 | (castle_passing_mask & castle_adj[m.from] & castle_adj[m.to]);

    /* handle special moves */
    switch(m.flags)
    {
        case PAWNOF2:
            newcpm ^= 12^X(m.to);
            break;

        case ENPASSANT:
        {
            uint8_t place = ROW_OF(m.from)+COL_OF(m.to);
            h ^= HASH_KEY( PAWN|other_color,  place );
            break;
        }

        case CASTLEKINGSIDE:
        {
            uint8_t p1 = ROW_OF(m.from)+5;
            uint8_t p2 = ROW_OF(m.from)+7;
            h ^= HASH_KEY( color_to_move|ROOK,  p1 )
                    ^ HASH_KEY( color_to_move|ROOK,  p2 );
            break;
        }

        case CASTLEQUEENSIDE:
        {
            uint8_t p1 = ROW_OF(m.from)+3;
            uint8_t p2 = ROW_OF(m.from);
            h ^= HASH_KEY( color_to_move|ROOK,  p1 )
                    ^ HASH_KEY( color_to_move|ROOK,  p2 );
            break;
        }

        case PROMOTEROOK ... PROMOTEKNIGHT:
        {
            uint8_t p = (m.flags-4)|color_to_move;
            h ^= HASH_KEY( color_to_move|PAWN,  m.to )
                    ^ HASH_KEY( p,  m.to );
            break;
        }
    }

    if(newcpm != castle_passing_mask)
        h ^= hash_keys[ 12*64 + newcpm ]
            ^ hash_keys[ 12*64 + castle_passing_mask ];

    return h;
}

void
Board::do_null_move(SaveBuf& s)
{
    s.hash_key = hash;

    hash ^= hash_key_toggle;

    SWITCH(color_to_move, other_color);
    num_moves += IS_WHITE(color_to_move);

    s.castle_passing_mask = castle_passing_mask;
    s.fifty = fifty;

    castle_passing_mask = 12 | (castle_passing_mask & 0xf0);

    /* adjust the hash key for the new castle/en passant flags*/
    if(s.castle_passing_mask != castle_passing_mask)
    {
        hash ^= hash_keys[ 12*64 + s.castle_passing_mask ]
                ^ hash_keys[ 12*64 + castle_passing_mask ];
    }

    /* mark that now we do not know anything about pinnings */
    under_check = 0xff;
}

void
Board::undo_null_move(const SaveBuf& s)
{
    hash = s.hash_key;

    num_moves -= IS_WHITE(color_to_move);
    SWITCH(color_to_move, other_color);

    fifty = s.fifty;
    castle_passing_mask = s.castle_passing_mask;

    /* mark that now we do not know anything about pinnings */
    under_check = 0xff;
}


#ifdef DEBUG
    #define HEAVY_DEBUG
#endif

//do move m and adjust hashkey
void
Board::do_move(const Move& m, SaveBuf& s)
{
    A88 data = this->data;

#ifdef HEAVY_DEBUG
    check_line_pawns();
    check_mat_tracking();
#if TRACK_ATTACKS
    check_attacks();
#endif //TRACK_ATTACKS
#endif //HEAVY_DEBUG
    ASSERT(COLOR_OF(data[m.from]) == color_to_move);
    ASSERT(data[king_pos[1]] == WK);
    ASSERT(data[king_pos[0]] == BK);
    ASSERT(DISTANCE(king_pos[0], king_pos[1]) >= 2);

    /* save the hash key */
    s.hash_key = hash;

    /* save all flags about the position */
    s.castle_passing_mask = castle_passing_mask;
    s.fifty = fifty;

#if TRACK_ATTACKS
    /* attacking info, 1/2 */
    del_attacks(data[m.from], m.from);
#endif //TRACK_ATTACKS

    data[m.to] = data[m.from];
    data[m.from] = 0;

#if TRACK_ATTACKS
    /* attacking info, 2/2 */
    if(m.capture)
        replace_attacks(m.capture,data[m.to], m.to);
    else
        add_attacks(data[m.to], m.to);
#endif //TRACK_ATTACKS;

    /* update material tracking */
    if(m.capture)
        mat_remove(m.capture, m.to);
    mat_move( data[m.to], m.from, m.to);

    /* update pawn tracking */
    if(PIECE_OF(data[m.to])==PAWN)
    {
        del_pawn(color_to_move, m.from);
        if(m.flags <= 4) //readd pawn to tracking structure, unless promoting
            add_pawn(color_to_move, m.to);
    }
    if(PIECE_OF(m.capture)==PAWN)
        del_pawn(other_color, m.to);

    /* adjust hash key */
    hash ^= HASH_KEY( data[m.to],  m.to )
            ^ HASH_KEY( data[m.to],  m.from )
            ^ hash_key_toggle;
    if(m.capture)
        hash ^= HASH_KEY( m.capture,  m.to );

    /* reset fifty moves count */
    if(m.capture || (PIECE_OF(data[m.to])==PAWN) )
        fifty = 0;
    else
        fifty++;

    /* reset castle/en passant flags*/
    castle_passing_mask = 12 | (castle_passing_mask & castle_adj[m.from] & castle_adj[m.to]);

    /* update king position */
    if(PIECE_OF(data[m.to])==KING)
        king_pos[IS_WHITE(data[m.to])] = m.to;

    /* handle special moves */
    switch(m.flags)
    {
        case PAWNOF2:
            castle_passing_mask ^= 12^X(m.to);
            break;

        case ENPASSANT:
        {
            uint8_t place = ROW_OF(m.from)+COL_OF(m.to);
            data[place] = 0;
            mat_remove( PAWN|other_color, place);
            del_pawn(other_color, place);
#if TRACK_ATTACKS
            del_attacks(PAWN|other_color, place);
#endif //TRACK_ATTACKS
            hash ^= HASH_KEY( PAWN|other_color,  place );
            break;
        }

        case CASTLEKINGSIDE:
        {
            uint8_t p1 = ROW_OF(m.from)+5;
            uint8_t p2 = ROW_OF(m.from)+7;
            data[p1] = data[p2];
            data[p2] = 0;
            mat_move( data[p1], p2, p1 );
#if TRACK_ATTACKS
            del_attacks( data[p1], p2);
            add_attacks( data[p1], p1);
#endif //TRACK_ATTACKS
            hash ^= HASH_KEY( data[p1],  p1 )
                    ^ HASH_KEY( data[p1],  p2 );
            break;
        }

        case CASTLEQUEENSIDE:
        {
            uint8_t p1 = ROW_OF(m.from)+3;
            uint8_t p2 = ROW_OF(m.from);
            data[p1] = data[p2];
            data[p2] = 0;
            mat_move( data[p1], p2, p1 );
#if TRACK_ATTACKS
            del_attacks( data[p1], p2);
            add_attacks( data[p1], p1);
#endif //TRACK_ATTACKS
            hash ^= HASH_KEY( data[p1],  p1 )
                    ^ HASH_KEY( data[p1],  p2 );
            break;
        }

        case PROMOTE_FIRST ... PROMOTE_LAST:
        {
            uint8_t p = (m.flags-PROMOTE0)|color_to_move;
            hash ^= HASH_KEY( data[m.to],  m.to )
                    ^ HASH_KEY( p,  m.to );
            mat_remove( data[m.to], m.to );
            data[m.to] = p;
            mat_add( data[m.to], m.to );
#if TRACK_ATTACKS
            replace_attacks( 0, p, m.to );
#endif //TRACK_ATTACKS
        }
    }

    /* adjust the hash key for the new castle/en passant flags*/
    if(s.castle_passing_mask != castle_passing_mask)
    {
        hash ^= hash_keys[ 12*64 + s.castle_passing_mask ]
                ^ hash_keys[ 12*64 + castle_passing_mask ];
    }

    /* switch side and increase move count */
    SWITCH(color_to_move, other_color);
    num_moves += IS_WHITE(color_to_move);

    /* mark that now we do not know anything about pinnings */
    under_check = 0xff;

    ASSERT(data[king_pos[1]] == WK);
    ASSERT(data[king_pos[0]] == BK);
    ASSERT(DISTANCE(king_pos[0], king_pos[1]) >= 2);
#ifdef HEAVY_DEBUG
    check_line_pawns();
    check_mat_tracking();
#if TRACK_ATTACKS
    check_attacks();
#endif //TRACK_ATTACKS
#endif //HEAVY_DEBUG
}

//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

void
Board::undo_move(const Move& m, const SaveBuf& s)
{
    A88 data = this->data;

#ifdef HEAVY_DEBUG
    check_line_pawns();
    check_mat_tracking();
#if TRACK_ATTACKS
    check_attacks();
#endif //TRACK_ATTACKS
#endif //HEAVY_DEBUG
    ASSERT(COLOR_OF(data[m.to]) == other_color);
    ASSERT(data[king_pos[1]] == WK);
    ASSERT(data[king_pos[0]] == BK);
    ASSERT(DISTANCE(king_pos[0], king_pos[1]) >= 2);

#if TRACK_ATTACKS
    /* attacking info, 1/2 */
    if(m.capture)
        replace_attacks(data[m.to], m.capture, m.to);
    else
        del_attacks(data[m.to], m.to);
#endif //TRACK_ATTACKS

    data[m.from] = data[m.to];
    if(m.capture)
        data[m.to] = m.capture;
    else
        data[m.to] = 0;

#if TRACK_ATTACKS
    /* attacking info, 2/2 */
    add_attacks(data[m.from], m.from);
#endif //TRACK_ATTACKS

    mat_move( data[m.from], m.to, m.from);
    if(m.capture)
        mat_add(m.capture, m.to);

    /* update king position */
    if(PIECE_OF(data[m.from])==KING)
        king_pos[IS_WHITE(data[m.from])] = m.from;

    /* handle special moves */
    switch(m.flags)
    {
        case ENPASSANT:
        {
            uint8_t place = ROW_OF(m.from)+COL_OF(m.to);
            data[place] = PAWN|color_to_move;
            mat_add( PAWN|color_to_move, place);
            add_pawn(color_to_move, place);
#if TRACK_ATTACKS
            add_attacks( PAWN|color_to_move, place);
#endif //TRACK_ATTACKS
            break;
        }

        case CASTLEQUEENSIDE:
        {
            uint8_t p1 = ROW_OF(m.from)+3;
            uint8_t p2 = ROW_OF(m.from);
            data[p2] = data[p1];
            data[p1] = 0;
            mat_move( data[p2], p1, p2 );
#if TRACK_ATTACKS
            del_attacks( data[p2], p1);
            add_attacks( data[p2], p2);
#endif //TRACK_ATTACKS
            break;
        }

        case CASTLEKINGSIDE:
        {
            uint8_t p1 = ROW_OF(m.from)+5;
            uint8_t p2 = ROW_OF(m.from)+7;
            data[p2] = data[p1];
            data[p1] = 0;
            mat_move( data[p2], p1, p2 );
#if TRACK_ATTACKS
            del_attacks( data[p2], p1);
            add_attacks( data[p2], p2);
#endif //TRACK_ATTACKS
            break;
        }

        case PROMOTE_FIRST ... PROMOTE_LAST:
        {
            uint8_t p = PAWN|other_color;
#if TRACK_ATTACKS
            replace_attacks( data[m.from], 0, m.from );
#endif //TRACK_ATTACKS
            mat_remove( data[m.from], m.from );
            data[m.from] = p;
            mat_add( data[m.from], m.from );
        }
    }

    if(PIECE_OF(data[m.from])==PAWN)
    {
        if(m.flags <= 4)
            del_pawn(other_color, m.to);
        add_pawn(other_color, m.from);
    }
    if(PIECE_OF(m.capture)==PAWN)
        add_pawn(color_to_move, m.to);

    /* load saved flags */
    fifty = s.fifty;
    castle_passing_mask = s.castle_passing_mask;
    hash = s.hash_key;

    /* switch side and increase move count */
    num_moves -= IS_WHITE(color_to_move);
    SWITCH(color_to_move, other_color);

    /* mark that now we do not know anything about pinnings */
    under_check = 0xff;

    ASSERT(data[king_pos[1]] == WK);
    ASSERT(data[king_pos[0]] == BK);
    ASSERT(DISTANCE(king_pos[0], king_pos[1]) >= 2);
#ifdef HEAVY_DEBUG
    check_line_pawns();
    check_mat_tracking();
#if TRACK_ATTACKS
    check_attacks();
#endif //TRACK_ATTACKS
#endif //HEAVY_DEBUG
}
