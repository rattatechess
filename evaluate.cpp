/***************************************************************************
                          evaluate.cpp  -  description
                             -------------------
    begin                : Wed Mar 13 2002
    copyright            : (C) 2002-2005 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <string.h>
#include "board.h"


#define PASSED_PAWN        30
#define BLOCKED_PASSER     12
#define PASSED_PIECE       11
#define ATTACKING_PIECE     7
#define ATTACKING_PAWN      5
#define DEFEND_PASSED_PAWN 13
#define CONNECTED_ROOKS    21
#define UNDEVELOPED        10
#define TWO_BISHOPS        45

#define VERY_UNSAFE_KING_LATERAL -16
#define UNSAFE_KING_LATERAL      -10
#define VERY_UNSAFE_KING         -23
#define UNSAFE_KING              -14

//------------------------------------------------------

const int king_still_to_castle    = -120;

const int pawn_chain              = 5;
const int pawn_insula             = -18;
const int pawn_isulated           = -10;
const int pawn_isulated_blocked   = -17;
const int pawn_backward_open_file = -6;
const int pawn_backward_blocked   = -9;
const int pawn_backward[8]        = { -2, -3, -5, -7, -7, -5, -3, -2 };
const int pawn_doubled[8]         = { -7, -5, -3, -2, -2, -3, -5, -7 }; //counted twice
const int pawn_duo[7]             = { 2, 3, 4, 5, 4, 3, 2 };            //counted x the row, x2, x3, etc

const int queen_value = 975;
const int queen_seventh = 9;
const int queen_open_file = 7;
const int queen_semiopen_file = 5;

const int rook_value = 500;
const int rook_seventh = 19;
const int rook_open_file = 14;
const int rook_semiopen_file = 10;



const int pawn_value[128] = {
    0,0,0,0,0,0,0,0,ENDL,
    -3,0,1,-15,-15,1,0,-3,ENDL,
    -2,1,3, 7, 7,3,1,-2,ENDL,
    -1,2,9,16,16,9,2,-1,ENDL,
    0,4,11,22,22,11,4,0,ENDL,
    1,6,13,24,24,13,6,1,ENDL,
    2,8,15,26,26,15,8,2,ENDL,
    0,0,0,0,0,0,0,0,ENDL
};

const int endgame_king_val[128] = {
       0,   2,   7,   7,   7,   7,   2,   0,   ENDL,
       2,  11,  19,  19,  19,  19,  11,   2,   ENDL,
       7,  19,  23,  25,  25,  23,  19,   7,   ENDL,
       7,  19,  25,  27,  27,  25,  19,   7,   ENDL,
       7,  19,  25,  27,  27,  25,  19,   7,   ENDL,
       7,  19,  23,  25,  25,  23,  19,   7,   ENDL,
       2,  11,  19,  19,  19,  19,  11,   2,   ENDL,
       0,   2,   7,   7,   7,   7,   2,   0,   ENDL
};


const int queen_tropism[8] = { 0, 51, 70, 40, 10, 0, 0, 0};
const int rook_tropism[8] = { 0, 3, 2, 1, 0, 0, 0, 0};
const int knight_tropism[8] = { 0, 10, 7, 5, 2, 0, 0, 0};
const int bishop_tropism[8] = { 0, 3, 3, 2, 1, 0, 0, 0};
const int rook_attack_tropism[8] = { 0, 5, 3, 0, 0, 0, 0, 0};
const int knight_attack_tropism[8] = { 0, 5, 3, 0, 0, 0, 0, 0};
const int bishop_attack_tropism[8] = { 0, 5, 3, 0, 0, 0, 0, 0};

const int piece_val[] = { 500, 325, 975, 325, 100, 0 };

inline int distance(uint8_t a, uint8_t b)
{
    return MAX(ABS(X(a)-X(b)), ABS(Y(a)-Y(b)));
}

int16_t Board::dummy_evaluate()
{
    int material[2] = { 0, 0 };

    int p=0;
    for(int col=0;col<2;col++)
        for(int i=0;i<6;i++)
            material[col] += mat_tracking[p++].count * piece_val[i];

    return material[IS_WHITE(color_to_move)] -
            material[IS_WHITE(other_color)];
}

//--------------------------------------------------------------------------

const int piece_value[] = { 0, 500, 325, 975, 325, 100, 0 };

const int controlled_bonus[128] =
{
      3,   3,   3,   3,   3,   3,   3,   3,   ENDL,
      3,   4,   4,   4,   4,   4,   4,   3,   ENDL,
      3,   4,   5,   5,   5,   5,   4,   3,   ENDL,
      3,   4,   5,   5,   5,   5,   4,   3,   ENDL,
      3,   4,   5,   5,   5,   5,   4,   3,   ENDL,
      3,   4,   5,   5,   5,   5,   4,   3,   ENDL,
      3,   4,   4,   4,   4,   4,   4,   3,   ENDL,
      3,   3,   3,   3,   3,   3,   3,   3,   ENDL
};

const int bishop_pair = 50;

const int bishop_base = 24;
const int bishop_middle = 20;
const int bishop_end    = 20;
const uint16_t bishop_attack = (1<<10)+1;

const int knight_base = 16;
const int knight_middle = 16;
const int knight_end    = 16;
const uint16_t knight_attack = (1<<10)+1;

const int rook_base  = 28;
const int rook_middle = 8;
const int rook_end    = 16;
const uint16_t rook_attack = (1<<10)+1;

const int queen_base = 52;
const int queen_middle = 4;
const int queen_end    = 8;
const uint16_t queen_attack = (1<<7)+1;

const uint16_t pawn_attack = (1<<14)+1;

const uint16_t king_attack = (1<<6)+1;

int16_t Board::evaluate(uint8_t eng_color, int16_t alpha, int16_t beta)
{
    int evaluation[2] = { 0, 0 };
    int king_danger[2] = { 0, 0 };
    int tropism[2] = { 0, 0 };

    int material[2] = {
        mat_tracking[PAWN-1].count * piece_value[PAWN] +
        mat_tracking[QUEEN-1].count * piece_value[QUEEN] +
        mat_tracking[BISHOP-1].count * piece_value[BISHOP] + ((mat_tracking[BISHOP-1].count >= 2) ? bishop_pair : 0) +
        mat_tracking[ROOK-1].count * (piece_value[ROOK] + (10 - mat_tracking[PAWN-1].count - mat_tracking[PAWN+5].count)*100/16) +
        mat_tracking[KNIGHT-1].count * (piece_value[KNIGHT] - (10 - mat_tracking[PAWN-1].count - mat_tracking[PAWN+5].count)*100/32),
        mat_tracking[PAWN+5].count * piece_value[PAWN] +
        mat_tracking[QUEEN+5].count * piece_value[QUEEN] +
        mat_tracking[BISHOP+5].count * piece_value[BISHOP] + ((mat_tracking[BISHOP+5].count >= 2) ? bishop_pair : 0) +
        mat_tracking[ROOK+5].count * (piece_value[ROOK] + (10 - mat_tracking[PAWN-1].count - mat_tracking[PAWN+5].count)*100/16) +
        mat_tracking[KNIGHT+5].count * (piece_value[KNIGHT] - (10 - mat_tracking[PAWN-1].count - mat_tracking[PAWN+5].count)*100/32)
    };
    int activity_middle[2] = { 0, 0 };
    int activity_end[2] = { 0, 0 };

    union { uint16_t white[128]; struct { uint16_t pad[8]; uint16_t black[120]; }; } attacks_data;
    uint16_t* attacks[2] = { attacks_data.white, attacks_data.black };
    memset(attacks_data.white, 0, 128*sizeof(uint16_t));

    for(int is_white=0;is_white<2;is_white++)
    {
        int     is_black = !is_white;
        uint8_t color = is_white ? WHITE : BLACK;
        uint8_t othcol = OTHER_COLOR(color);
        uint8_t up      = up_dir[is_white];
        uint8_t up_left = up+LEFT;
        uint8_t up_right = up+RIGHT;
        uint8_t othpawn = PAWN|othcol;
        uint8_t mypawn = PAWN|color;
        int mt = (is_white ? +5 : -1);


        /* Evaluate bishops */
        for(int i=mat_tracking[BISHOP+mt].count-1;i>=0;i--)
        {
            int16_t v = -bishop_base;
            uint8_t pos = mat_tracking[BISHOP+mt].pos[i];

            for(int i=3;i>=0;i--)
            {
                register uint8_t currpos = pos;
                register uint8_t currinc = bishmoves[i];

                while(1)
                {
                    currpos += currinc;

                    if(!OUT_OF_BOARD(currpos))
                        attacks[is_white][currpos] += bishop_attack;

                    if(OUT_OF_BOARD(currpos) || COLOR_OF(data[currpos]) == color)
                        break;

                    v += controlled_bonus[currpos];

                    if(data[currpos])
                        break;
                }

                if(OUT_OF_BOARD(currpos))
                    continue;
            }

            activity_middle[is_white] += v * bishop_middle;
            activity_end[is_white]    += v * bishop_end;
        }


        /* Evaluate rooks */
        for(int i=mat_tracking[ROOK+mt].count-1;i>=0;i--)
        {
            int16_t v = -rook_base;
            uint8_t pos = mat_tracking[ROOK+mt].pos[i];

            for(int i=3;i>=0;i--)
            {
                register uint8_t currpos = pos;
                register uint8_t currinc = rookmoves[i];

                while(1)
                {
                    currpos += currinc;

                    if(!OUT_OF_BOARD(currpos))
                        attacks[is_white][currpos] += rook_attack;

                    if(OUT_OF_BOARD(currpos) || COLOR_OF(data[currpos]) == color)
                        break;

                    v += controlled_bonus[currpos];

                    if(data[currpos])
                        break;
                }

                if(OUT_OF_BOARD(currpos))
                    continue;
            }

            activity_middle[is_white] += v * rook_middle;
            activity_end[is_white]    += v * rook_end;
        }


        /* Evaluate queens */
        for(int i=mat_tracking[QUEEN+mt].count-1;i>=0;i--)
        {
            int16_t v = -queen_base;
            uint8_t pos = mat_tracking[QUEEN+mt].pos[i];

            for(int i=7;i>=0;i--)
            {
                register uint8_t currpos = pos;
                register uint8_t currinc = kingmoves[i];

                while(1)
                {
                    currpos += currinc;

                    if(!OUT_OF_BOARD(currpos))
                        attacks[is_white][currpos] += queen_attack;

                    if(OUT_OF_BOARD(currpos) || COLOR_OF(data[currpos]) == color)
                        break;

                    v += controlled_bonus[currpos];

                    if(data[currpos])
                        break;
                }

                if(OUT_OF_BOARD(currpos))
                    continue;
            }

            activity_middle[is_white] += v * queen_middle;
            activity_end[is_white]    += v * queen_end;
        }


        /* Evaluate knights */
        for(int i=mat_tracking[KNIGHT+mt].count-1;i>=0;i--)
        {
            int16_t v = -knight_base;
            uint8_t pos = mat_tracking[KNIGHT+mt].pos[i];
            KnightMove* hm = &knightmoves[pos];

            for(int i=hm->numm;i>=0;i--)
            {
                register uint8_t currpos = hm->jump[i];

                attacks[is_white][currpos] += knight_attack;

                if(IS_OF_COLOR(data[currpos], color))
                    continue;

                v += controlled_bonus[currpos];
            }

            activity_middle[is_white] += v * knight_middle;
            activity_end[is_white]    += v * knight_end;
        }

        for(int i=mat_tracking[PAWN+mt].count-1;i>=0;i--)
        {
            uint8_t pos = mat_tracking[PAWN+mt].pos[i];
            int x = X(pos);

            if(x<7)
                attacks[is_white][I(pos+up_right)] += pawn_attack;
            if(x>0)
                attacks[is_white][I(pos+up_left)] += pawn_attack;
        }

        for(int i=mat_tracking[KING+mt].count-1;i>=0;i--)
        {
            uint8_t pos = mat_tracking[KING+mt].pos[i];

            for(int i=7;i>=0;i--)
            {
                register uint8_t currpos = pos + kingmoves[i];
                if(!OUT_OF_BOARD(currpos))
                    attacks[is_white][currpos] += king_attack;
            }
        }
    }

#if 0
    for(int y=7;y>=0;y--)
    {
        for(int is_white = 0;is_white<2;is_white++)
        {
            for(int x=0;x<8;x++)
                printf(" % 4x", attacks[is_white][POS_XY(x,y)]);
            printf("%s", is_white ? "\n" : "      ");
        }
    }
#endif

     int loss[2][2] = { { 0, 0 }, { 0, 0 } };
     int loss_soft[2][2] = { { 0, 0 }, { 0, 0 } };
     int attackings[2] = { 0, 0 };
#if 1

     #define UPDATE_LOSS(_x) ({int x = (_x); \
                        if(x >= loss[is_white][0]) { loss[is_white][1] = loss[is_white][0]; loss[is_white][0] = x; }  \
                     else if(x > loss[is_white][1]) { loss[is_white][1] = x; } })
     #define UPDATE_LOSS_SOFT(_x) ({int x = (_x); \
                        if(x >= loss_soft[is_white][0]) { loss_soft[is_white][1] = loss_soft[is_white][0]; loss_soft[is_white][0] = x; }  \
                     else if(x > loss_soft[is_white][1]) { loss_soft[is_white][1] = x; } })
     #define BC(_x) ({int x = _x, c; for(c=0; x; c++, x &= x-1); c; })
     #define N(x) ((x) & 0x1f)

     for(int is_white=0; is_white<2; is_white++)
     {
         int     on_move  = IS_WHITE(color_to_move) == is_white;
         int     is_black = !is_white;
         int     mt = (is_white ? +5 : -1);

         int malus1 = on_move ? -32 : -39;
         int malus2 = on_move ? -10 : -13;
         int bonus1 = +5;

         int malus = on_move ? -20 : -35;

         /* Evaluate knights */
         for(int i=mat_tracking[KNIGHT+mt].count-1;i>=0;i--)
         {
             uint8_t pos = mat_tracking[KNIGHT+mt].pos[i];
             if(attacks[is_black][pos] >= pawn_attack)
                 UPDATE_LOSS(attacks[is_white][pos]>=attacks[is_black][pos] ? 270 : 325);
             else if(!attacks[is_white][pos] && attacks[is_black][pos])
                 UPDATE_LOSS_SOFT(325);
             else if(attacks[is_black][pos] > attacks[is_white][pos] + bishop_attack)
                 UPDATE_LOSS_SOFT(325);
             else if(attacks[is_black][pos] && attacks[is_black][pos] >= attacks[is_white][pos])
                 attackings[is_white] += malus;
         }

         /* Evaluate bishops */
         for(int i=mat_tracking[BISHOP+mt].count-1;i>=0;i--)
         {
             uint8_t pos = mat_tracking[BISHOP+mt].pos[i];
             if(attacks[is_black][pos] >= pawn_attack)
                 UPDATE_LOSS(attacks[is_white][pos]>=attacks[is_black][pos] ? 270 : 325);
             else if(!attacks[is_white][pos] && attacks[is_black][pos])
                 UPDATE_LOSS_SOFT(325);
             else if(attacks[is_black][pos] > attacks[is_white][pos] + bishop_attack)
                 UPDATE_LOSS_SOFT(325);
             else if(attacks[is_black][pos] && attacks[is_black][pos] >= attacks[is_white][pos])
                 attackings[is_white] += malus;
         }

         /* Evaluate rooks */
         for(int i=mat_tracking[ROOK+mt].count-1;i>=0;i--)
         {
             uint8_t pos = mat_tracking[ROOK+mt].pos[i];
             if(attacks[is_black][pos] >= pawn_attack)
                 UPDATE_LOSS(attacks[is_white][pos]>=attacks[is_black][pos] ? 430 : 500);
             else if(!attacks[is_white][pos] && attacks[is_black][pos])
                 UPDATE_LOSS_SOFT(500);
             else if(attacks[is_black][pos] > attacks[is_white][pos] + bishop_attack)
                 UPDATE_LOSS_SOFT(400);
             else if(attacks[is_black][pos] && attacks[is_black][pos] >= attacks[is_white][pos])
                 attackings[is_white] += malus;
         }

         /* Evaluate QUEEN */
         for(int i=mat_tracking[QUEEN+mt].count-1;i>=0;i--)
         {
             uint8_t pos = mat_tracking[QUEEN+mt].pos[i];
             if(attacks[is_black][pos] >= pawn_attack)
                 UPDATE_LOSS(attacks[is_white][pos]>=attacks[is_black][pos] ? 830 : 900);
             else if(!attacks[is_white][pos] && attacks[is_black][pos])
                 UPDATE_LOSS_SOFT(1000);
             else if(attacks[is_black][pos] > attacks[is_white][pos] + bishop_attack)
                 UPDATE_LOSS_SOFT(600);
             else if(attacks[is_black][pos] && attacks[is_black][pos] >= attacks[is_white][pos])
                 attackings[is_white] += malus;
         }

         for(int i=mat_tracking[PAWN+mt].count-1;i>=0;i--)
         {
             uint8_t pos = mat_tracking[PAWN+mt].pos[i];
             if(N(attacks[is_black][pos]) > N(attacks[is_white][pos])
                    && attacks[is_black][pos] > attacks[is_white][pos]*3/4)
                 UPDATE_LOSS_SOFT(100);
             else if(attacks[is_black][pos] && attacks[is_black][pos] >= attacks[is_white][pos])
                 attackings[is_white] += malus;
         }
    }
#endif

#if 0
    #define N(x) ((x) & 0x1f)
    int attackings[2] = { 0, 0 };

    for(int is_white=0; is_white<2; is_white++)
    {
        int     on_move  = IS_WHITE(color_to_move) == is_white;
        int     is_black = !is_white;
        int     mt = (is_white ? +5 : -1);

        /* Evaluate bishops */
        for(int i=mat_tracking[BISHOP+mt].count-1;i>=0;i--)
        {
            uint8_t pos = mat_tracking[BISHOP+mt].pos[i];
            if(attacks[is_black][pos] && !attacks[is_white][pos])
                attackings[is_white] += on_move ? -20 : -200;
            else if(attacks[is_black][pos] >= pawn_attack || attacks[is_black][pos] > attacks[is_white][pos] + bishop_attack)
                attackings[is_white] += on_move ? -20 : -200;
            else if(N(attacks[is_black][pos]) >= N(attacks[is_white][pos]) && attacks[is_black][pos] >= attacks[is_white][pos])
            //else if(attacks[is_black][pos] >= attacks[is_white][pos])
                attackings[is_white] += -10;
        }

        /* Evaluate rooks */
        for(int i=mat_tracking[ROOK+mt].count-1;i>=0;i--)
        {
            uint8_t pos = mat_tracking[ROOK+mt].pos[i];
            if(attacks[is_black][pos] && !attacks[is_white][pos])
                attackings[is_white] += on_move ? -25 : -300;
            else if(attacks[is_black][pos] >= pawn_attack || attacks[is_black][pos] > attacks[is_white][pos] + bishop_attack)
                attackings[is_white] += on_move ? -25 : -300;
            else if(N(attacks[is_black][pos]) >= N(attacks[is_white][pos]) && attacks[is_black][pos] >= attacks[is_white][pos])
            //else if(attacks[is_black][pos] >= attacks[is_white][pos])
                attackings[is_white] += -10;
        }

        /* Evaluate queens */
        for(int i=mat_tracking[QUEEN+mt].count-1;i>=0;i--)
        {
            uint8_t pos = mat_tracking[QUEEN+mt].pos[i];
            if(attacks[is_black][pos] && !attacks[is_white][pos])
                attackings[is_white] += on_move ? -40 : -600;
            else if(attacks[is_black][pos] >= bishop_attack)
                attackings[is_white] += on_move ? -40 : (attacks[is_black][pos] >= pawn_attack ? -600 : -400);
        }

        /* Evaluate knights */
        for(int i=mat_tracking[KNIGHT+mt].count-1;i>=0;i--)
        {
            uint8_t pos = mat_tracking[KNIGHT+mt].pos[i];
            if(attacks[is_black][pos] && !attacks[is_white][pos])
                attackings[is_white] += on_move ? -20 : -200;
            else if(attacks[is_black][pos] >= pawn_attack || attacks[is_black][pos] > attacks[is_white][pos] + bishop_attack)
                attackings[is_white] += on_move ? -20 : -200;
            else if(N(attacks[is_black][pos]) >= N(attacks[is_white][pos]) && attacks[is_black][pos] >= attacks[is_white][pos])
            //else if(attacks[is_black][pos] >= attacks[is_white][pos])
                attackings[is_white] += -10;
        }

        for(int i=mat_tracking[PAWN+mt].count-1;i>=0;i--)
        {
            uint8_t pos = mat_tracking[PAWN+mt].pos[i];
            if(attacks[is_black][pos] >= attacks[is_white][pos])
            {
                if(N(attacks[is_black][pos]) > N(attacks[is_white][pos]))
                    attackings[is_white] += on_move ? -25 : -60;
                else if(N(attacks[is_black][pos]) == N(attacks[is_white][pos]))
                    attackings[is_white] += -5;
            }
        }
    }
#endif

    for(int c = 0;c<2;c++)
    {
        int on_move = c == IS_WHITE(color_to_move);
        attackings[c] -= !on_move ? MAX(loss[c][0]*4/8, loss_soft[c][0]*4/8) :
            MAX(loss[c][1]*3/8, MAX(loss_soft[c][1]*2/8, MIN(loss[c][0]*3/8, loss_soft[c][0]*2/8) ) );
    }

    int tot_mat = material[0] + material[1];
    int max_mat = 100*16 + 325*8 + 500*4 + 975*2;
    int eval[2] = {
        material[0] + (activity_middle[0]*tot_mat + activity_end[0]*(max_mat-tot_mat))/(16 * max_mat),
        material[1] + (activity_middle[1]*tot_mat + activity_end[1]*(max_mat-tot_mat))/(16 * max_mat)
    };

    return 7 + eval[IS_WHITE(color_to_move)] - eval[IS_WHITE(other_color)]
             + attackings[IS_WHITE(color_to_move)] - attackings[IS_WHITE(other_color)];
}

#if 0
        /* Evaluate pawns */
        bool p = false;
        for(int x=0;x<8;x++)
        {
            bool nextp = !!line_pawns[is_white][x].count;
            if(nextp && !p)
                evaluation[is_white] += pawn_insula;
            p = nextp;
        }

        for(int i=mat_tracking[PAWN+mt].count-1;i>=0;i--)
        {
            int16_t v = 0;
            uint8_t pos = mat_tracking[PAWN+mt].pos[i];
            uint8_t x = X(pos);
            uint8_t y = is_white ? Y(pos) : 7-Y(pos);

            v += (100 + pawn_value[POS_XY(x,y)]);

            /* isulated pawn malus */
            #if 0
            if( (x == 0 || line_pawns[is_white][x-1].count == 0) &&
                (x == 7 || line_pawns[is_white][x+1].count == 0) )
            {
                v += pawn_isulated;
                if(COLOR_OF(data[uint8_t(up+pos)]) == othcol)
                    v += pawn_isulated_blocked;
            }
            #endif

            /* backward pawn malus */
            if( (x == 0 || line_pawns[is_white][x-1].count == 0
                    || line_pawns[is_white][x-1].pos[0] > y) &&
                    (x == 7 || line_pawns[is_white][x+1].count == 0
                    || line_pawns[is_white][x+1].pos[0] > y) )
            {
                v += pawn_backward[x];
                if(!line_pawns[!is_white][x].count)
                    v += pawn_backward_open_file;
                if(data[I(up+pos)])
                    v += pawn_backward_blocked;
            }

            if(x!=0 && data[LEFT_OF(pos)]==data[pos])
                v += pawn_duo[x]*(y+2)/2;
            if(x!=0 && (data[LEFTUP_OF(pos)]==data[pos] ||
                    data[LEFTDOWN_OF(pos)]==data[pos]))
                v += pawn_chain;

            /* passed pawn */
            if( (x==0||!line_pawns[1-is_white][x-1].count ||
                 line_pawns[1-is_white][x-1].pos[0]>=7-y) &&
                 (x==7||!line_pawns[1-is_white][x+1].count ||
                 line_pawns[1-is_white][x+1].pos[0]>=7-y) &&
                 (!line_pawns[1-is_white][x].count||line_pawns[1-is_white][x].pos[0]>7-y) )
            {
                if(COLOR_OF(up_dir[is_white]+pos) == othcol)
                    v += MAX(y-2,1)*BLOCKED_PASSER;
                else
                    v += MAX(y-2,1)*PASSED_PAWN;
            }

            /* doubled pawns */
            if(line_pawns[is_white][x].count > 1)
                v += pawn_doubled[x];

            evaluation[is_white] += v;
        }

        /* Evaluate king */
        for(int i=mat_tracking[KING+mt].count-1;i>=0;i--)
        {
            int16_t v = 0;
            uint8_t pos = mat_tracking[KING+mt].pos[i];
            uint8_t x = X(pos);
            uint8_t y = is_white ? Y(pos) : 7-Y(pos);

            if( is_white ? !(castle_passing_mask & 0x30) :
                    !(castle_passing_mask & 0xc0) )
            {
                if(material[!is_white]>=1500)
                {
                    v += king_safety[pos];
                    if((x>=5 || x<=2) && y<=1)
                    {
                        uint8_t f = x<=2 ? 2 : 5;
                        uint8_t g = x<=2 ? 1 : 6;
                        uint8_t h = x<=2 ? 0 : 7;
                        uint8_t fpawn = line_pawns[is_white][f].count ? line_pawns[is_white][f].pos[0] : 255;
                        uint8_t gpawn = line_pawns[is_white][g].count ? line_pawns[is_white][g].pos[0] : 255;
                        uint8_t hpawn = line_pawns[is_white][h].count ? line_pawns[is_white][h].pos[0] : 255;

                        if(fpawn >= 3)
                            v += -12;
                        else
                            v += -4;
                        if(gpawn >= 3)
                            v += -15;
                        else
                            v += -8;
                        if(hpawn >= 3)
                            v += -13;
                        else
                            v += -6;
                        if(fpawn==1 && gpawn>=2 && hpawn==1)
                        {
                            uint8_t start = POS_XY(x<=2 ? 1 : 6, is_white?1:6);
                            uint8_t defbish = color | BISHOP;
                            if(data[start] == defbish || data[RIGHTUP_OF(start)] == defbish ||
                               data[LEFTUP_OF(start)] == defbish || data[RIGHTDOWN_OF(start)] == defbish ||
                               data[LEFTDOWN_OF(start)] == defbish)
                              v += 5;
                            else
                              v += -13;
                        }
                        if(!line_pawns[is_white][f].count)
                            v += -10;
                        if(!line_pawns[is_white][g].count)
                            v += -16;
                        if(!line_pawns[is_white][h].count)
                            v += -16;
                        if(data[POS_XY(h, is_white?0:7)] == (color | ROOK) ||
                                data[POS_XY(h, is_white?1:6)] == (color | ROOK) )
                            v += -80;
                    }
                    else if((x<5 && x>2) && y<=1)
                    {
                        if(!line_pawns[is_white][x].count)
                            v += -26;
                        if(!line_pawns[is_white][7-x].count)
                            v += -16;
                    }

                    v = v * (MIN(material[!is_white], 2400)-1500)/900;
                    king_danger[is_white] = v;
                }
                if(material[!is_white]<2400)
                    v += endgame_king_val[pos] * (2400 - MAX(material[!is_white], 1500) )/900;
            }
            else
                v += king_still_to_castle;

            evaluation[is_white] += v;
        }
    }


    evaluation[0] += (1500*(evaluation[0]-evaluation[1]))/
                       (2000+material[0]+material[1]);
    evaluation[0] += tropism[0] * 200 / MAX(200 + king_danger[1], 50);
    evaluation[1] += tropism[1] * 200 / MAX(200 + king_danger[0], 50);

    int16_t retv = 7 + evaluation[IS_WHITE(color_to_move)] -
                        evaluation[IS_WHITE(other_color)];

//     if(mat_tracking[PAWN-1].count + mat_tracking[PAWN+5].count >= 15)
//         retv += color_to_move == eng_color ? -15 : 15;
//     else if(mat_tracking[PAWN-1].count + mat_tracking[PAWN+5].count >= 15)
//         retv += color_to_move == eng_color ? -8 : 8;

    return retv;
}
#endif
