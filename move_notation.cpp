/***************************************************************************
            move_notation.h  -  Convert move from and to string
                             -------------------
    begin                : Mon Oct 08 2007
    copyright            : (C) 2007 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <string.h>
#include "board.h"


static int char_to_piece(char c)
{
    switch(c)
    {
        case 'K': case 'k': return KING;
        case 'Q': case 'q': return QUEEN;
        case 'R': case 'r': return ROOK;
        case 'B': case 'b': return BISHOP;
        case 'N': case 'n': return KNIGHT;
        case 'P': case 'p': return PAWN;
    }

    return -1;
}


/* reads a string and if possible understands a move */
Move Board::move_from_string(const char* s)
{
    int piece = -1;
    int from_x = -1;
    int from_y = -1;
    int to_x = -1;
    int to_y = -1;
    int promote = -1;
    int comment_1 = 0;
    int comment_2 = strlen(s);
    int pos = 0;
    Move retv = Move::None();

    /* parse O-O-O */
    if(!strncmp(s, "0-0-0", 5) || !strncmp(s, "O-O-O", 5) || !strncmp(s, "o-o-o", 5) )
    {
        comment_1 = 5;

        Move moves[250];
        int num_moves = find_moves(moves);

        for(int i=0;i<num_moves;i++)
        if(moves[i].flags == CASTLEQUEENSIDE)
        {
            retv = moves[i];
            goto do_return;
        }

        return Move::Illegal();
    }

    /* parse O-O. Note that O-O-O matches the expr for O-O, so it must be tested after */
    if(!strncmp(s, "0-0", 3) || !strncmp(s, "O-O", 3) || !strncmp(s, "o-o", 3) )
    {
        comment_1 = 3;

        Move moves[250];
        int num_moves = find_moves(moves);

        for(int i=0;i<num_moves;i++)
        if(moves[i].flags == CASTLEKINGSIDE)
        {
            retv = moves[i];
            goto do_return;
        }

        return Move::Illegal();
    }

    /* parse a SAN */
    if(s[pos]>='A' && s[pos]<='Z' && (piece=char_to_piece(s[pos])) != -1)
        pos++;
    if(s[pos]>='a' && s[pos]<='h')
        to_x = s[pos++] - 'a';
    if(s[pos]>='1' && s[pos]<='8')
        to_y = s[pos++] - '1';
    if(s[pos]=='-' || s[pos]=='x')
        pos++;
    if(s[pos]>='A' && s[pos]<='Z' && char_to_piece(s[pos]) != -1)
        pos++;
    if(s[pos]>='a' && s[pos]<='h')
    {
        from_x = to_x;
        to_x = s[pos++] - 'a';
    }
    if(s[pos]>='1' && s[pos]<='8')
    {
        from_y = to_y;
        to_y = s[pos++] - '1';
    }
    if(s[pos]=='=')
        pos++;
    if((promote=char_to_piece(s[pos])) != -1)
        pos++;

    if(to_x != -1 && to_y != -1)
    {
        if(piece==-1 && (from_x==-1 || from_y==-1))
            piece = PAWN;

        comment_1 = pos;

        /* find a unique matching move */
        Move moves[250];
        int num_moves = find_moves(moves);

//         print_board();
//         printf("piece=%d from_x=%d from_y=%d to_x=%d to_y=%d promote=%d\n",
//             piece, from_x, from_y, to_x, to_y, promote);

        for(int i=0;i<num_moves;i++)
        {
//             char buf[64];
//             move_to_alg(buf, &moves[i]);
//             printf("%s piece=%d from_x=%d from_y=%d to_x=%d to_y=%d promote=%d\n", buf,
//                 PIECE_OF(data[moves[i].from]), X(moves[i].from), Y(moves[i].from),
//                 X(moves[i].to), Y(moves[i].to), moves[i].flags);
            if( (piece  == -1 || piece == PIECE_OF(data[moves[i].from])) &&
                (from_x == -1 || from_x == X(moves[i].from)) &&
                (from_y == -1 || from_y == Y(moves[i].from)) &&
                (to_x == X(moves[i].to)) &&
                (to_y == Y(moves[i].to)) &&
                (moves[i].flags<PROMOTE_FIRST || moves[i].flags>PROMOTE_LAST || moves[i].flags-PROMOTE0 == promote) )
            {
                if(retv != Move::None())
                    return Move::Ambiguous();
                else
                    retv = moves[i];
            }
        }

        if(retv == Move::None())
            return Move::Illegal();

        goto do_return;
    }

    return Move::None();

do_return:
    retv.val = 0;
    for(int i=comment_1; i<comment_2; i++)
    if(s[i] == '!')
        retv.val++;
    else if(s[i] == '?')
        retv.val--;

    return retv;
}

//converts a move to the dummy notation (e2e4,g7h8q,...)
char* Board::move_to_coord(char* string, const Move& mv)
{
    char* str = string;
    *(str++) = 'a' + X(mv.from);
    *(str++) = '1' + Y(mv.from);
    *(str++) = 'a' + X(mv.to);
    *(str++) = '1' + Y(mv.to);

    /* promotion */
    if(mv.flags > PROMOTE0)
        *(str++) = piecename[ mv.flags-PROMOTE0 ]^0x20;
    *(str++)='\0';
    return string;
}

/* converts a move into the standard algebraic notation
   the current board must be the board before the move is done.
   mv must be a valid move in the current position! */
char* Board::move_to_alg(char* string, const Move& mv)
{
    char* str=string;

    if(mv.flags==CASTLEKINGSIDE)
    {
        *(str++)='O';
        *(str++)='-';
        *(str++)='O';
    }
    else if(mv.flags==CASTLEQUEENSIDE)
    {
        *(str++)='O';
        *(str++)='-';
        *(str++)='O';
        *(str++)='-';
        *(str++)='O';
    }
    else
    {
        Move moves[250];
        int num_moves = find_moves(moves);
        unsigned char piece=data[mv.from];

        if(PIECE_OF(piece)!=PAWN)
        {
            /* mark here how the move could be ambiguous */
            bool ambig = false;
            bool samecol=false;
            bool samerow=false;

            *(str++)=Board::piecename[PIECE_OF(piece)];

            for(int i=0;i<num_moves;i++)
            {
                //check for ambiguity
                if(piece == data[moves[i].from]      //same piece?
                  && moves[i].to == mv.to                 //same target?
                  && moves[i].from != mv.from)            //but not same move?
                {
                    ambig = true;
                    if(X(moves[i].from)==X(mv.from))
                        samecol=true;
                    if(Y(moves[i].from)==Y(mv.from))
                        samerow=true;
                }
            }

            if(ambig)
            {
                if(!samecol || samerow)
                    *(str++)='a' + X(mv.from);
                if(samecol)
                    *(str++)='1' + Y(mv.from);
            }
        }
        if((mv.capture) || mv.flags==ENPASSANT)
        {
            if(PIECE_OF(piece)==PAWN)
                *(str++)='a' + X(mv.from);
            *(str++)='x';
        }

        *(str++)='a' + X(mv.to);
        *(str++)='1' + Y(mv.to);

        if(mv.flags > PROMOTE0)
        {
            *(str++) = '=';
            *(str++) = Board::piecename[ mv.flags-PROMOTE0 ];
        }
    }
    /* is this move a check(mate)? */
    SaveBuf tmp;
    do_move(mv, tmp);
    if(under_attack( king_pos[IS_WHITE(color_to_move)],
                                                        other_color) )
    {
        Move mv_tmp[250];
        int n = find_moves(mv_tmp);

        if(n==0)
            *(str++)='#';
        else
            *(str++)='+';
    }
    undo_move(mv, tmp);
    *(str++)='\0';
    return string;
}
