/***************************************************************************
                          utils.h  -  some utility funtions
                             -------------------
    begin                : lun ott 24 2005
    copyright            : (C) 2005-2007 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __UTILS_H__
#define __UTILS_H__

#include <stdio.h>
#include <stdarg.h>
#include <inttypes.h>
#include <errno.h>
#include "platform.h"
#include "compiler.h"
#include "array.h"

#define SWITCH(a, b) ({ typeof(a) tmp = b; b = a; a = tmp; })
#define MAX(a,b) ({ typeof(a) _a=(a); typeof(b) _b=(b); _a > _b ? _a : _b; })
#define MIN(a,b) ({ typeof(a) _a=(a); typeof(b) _b=(b); _a < _b ? _a : _b; })
#define ABS(a) ({ typeof(a) _a=(a); _a > 0 ? _a : -_a; })

#ifdef __x86_64__
#define U64F "%lu"
#else
#define U64F "%llu"
#endif

#define PROF(f,n) {uint64_t tmp = rdtsc(); f; prof_##n += rdtsc()-tmp; }

template<typename T, int S>
class Buf
{
private:
    T m_data[S];

public:
    T *data() { return m_data; }
    Buf() {}
    Buf(const char* fmt, ...)
    {
        va_list ap;
        va_start(ap, fmt);
        vsnprintf(m_data, S, fmt, ap);
        va_end(ap);
    }
};

typedef Buf<char, 64>   TmpStr;
typedef Buf<char, 1024> BigStr;

char **tokenize(const char *str, int* numtokens = NULL, bool accept_quotes = true);
char **tokenize(const char *str, const char *whitespaces, int* numtokens = NULL, bool accept_quotes = true);

char **get_tokenized(int* numtokens = NULL, bool accept_quotes = true);
char **get_tokenized(const char *whitespaces, int* numtokens = NULL, bool accept_quotes = true);
char **fget_tokenized(FILE* f, int* numtokens = NULL, bool accept_quotes = true);
char **fget_tokenized(FILE* f, const char *whitespaces, int* numtokens = NULL, bool accept_quotes = true);

#endif //__UTILS_H__
