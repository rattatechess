/***************************************************************************
                compiler.h  -  compiler-dependent definitions
                             -------------------
    begin                : mer ott 18 2005
    copyright            : (C) 2005-2007 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __COMPILER_H__
#define __COMPILER_H__

#include <stdlib.h>

#ifdef __GNUC__

#define PACKED __attribute__((packed))
#define ALIGN(x) __attribute__((aligned (x)))
#define PRINTF(x,y) __attribute__ ((__format__ (__printf__, x, y)))

#define TRAP asm("int $0x03\n\t")

#ifdef DEBUG

    #define ASSERT(x) ({if(!(x)){ printf("Assertion " #x " failed\n" \
                        "in %s, line %d of file %s\n", __PRETTY_FUNCTION__, \
                        __LINE__, __FILE__); system("kdialog --error 'Crash!'"); TRAP; }; 0;})
#else //DEBUG

    #define ASSERT(x) ({})
#endif //DEBUG

#else

#define PACKED
#define ALIGN(x)
#define PRINTF(x,y)
#define TRAP
#define ASSERT(x)

#endif //0

#endif //__COMPILER_H__
