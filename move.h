/***************************************************************************
                          move.h  -  Move related definitions
                             -------------------
    begin                : Sun Sep 28 2007
    copyright            : (C) 2007 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __MOVE_H__
#define __MOVE_H__

#include "utils.h"
#include "hash.h"

// store all the possible moves of a knight.
class PACKED KnightMove
{
public:
    uint8_t jump[8];
    uint8_t numm;
};


/* PACKED, use a small structure even if unaligned.
   if i can quote Alan Cox, "Cache is king" :) */
class PACKED Move
{
public:
    union
    {
        struct
        {
            uint8_t from;    /* square we are moving from */
            uint8_t to;      /* square we are moving to */
            uint8_t capture;
            uint8_t flags;
        };
        uint32_t as_int;
    };
    int16_t val;
    uint8_t cost;
    uint8_t extend;

    bool operator==(const Move& m) const {  return (as_int == m.as_int);  }
    bool operator!=(const Move& m) const {  return (as_int != m.as_int);  }
    bool operator<(const Move& m) const {  return (as_int < m.as_int);  }

    bool valid() const { return as_int<0xff000000; }

    static Move FromInt(int i)
    {
        Move m;
        m.as_int = i;
        return m;
    }
    static Move None() {  return FromInt(0xffffffff);  }
    static Move Illegal() {  return FromInt(0xfffffffe);  }
    static Move Ambiguous() {  return FromInt(0xfffffffd);  }
};


class PACKED SaveBuf
{
public:
    HashKey hash_key;
    uint8_t castle_passing_mask;
    uint8_t fifty;
};


typedef Buf<char, 64> MoveStr;

#endif //__MOVE_H__
