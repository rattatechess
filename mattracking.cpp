/***************************************************************************
                          check.cpp  -  description
                             -------------------
    begin                : Mon Sep 19 2005
    copyright            : (C) 2005 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "board.h"


void
Board::mat_add(uint8_t piece, uint8_t piece_pos)
{
    int p = PIECE_TO_12(piece);
    int c = mat_tracking[ p ].count;

    ASSERT(c>=0 && c<=11 && p<12 && p>=0);

    mat_tracking[ p ].pos[ mat_tracking[ p ].count++ ] = piece_pos;
    mat_idx[ piece_pos ] = c;
}

void
Board::mat_remove(uint8_t piece, uint8_t piece_pos)
{
    int p = PIECE_TO_12(piece);
    int c = mat_tracking[ p ].count;
    int idx = mat_idx[ piece_pos ];

    ASSERT(c>=0 && c<=11 && p<12 && p>=0);

    c--;
    if(idx<c)
    {
        mat_tracking[p].pos[idx] = mat_tracking[p].pos[c];
        mat_idx[ mat_tracking[p].pos[idx] ] = idx;
    }

    mat_tracking[p].count = c;
}

void
Board::mat_move(uint8_t piece, uint8_t from, uint8_t to)
{
    int p = PIECE_TO_12(piece);
    int idx = mat_idx[ from ];

    ASSERT(p<12 && p>=0);

    mat_tracking[p].pos[idx] = to;
    mat_idx[ to ] = idx;
}

void
Board::recalc_mat_tracking()
{
    for(int j=0;j<12;j++)
        mat_tracking[j].count = 0;

    int piece_pos = 128;
    for(int i=7;i>=0;i--)
    {
        piece_pos -= 8;
        for(int j=7;j>=0;j--)
        {
            piece_pos--;
            if(data[piece_pos] && data[piece_pos]!=STONE)
            {
                int p = PIECE_TO_12(data[piece_pos]);
                int c = mat_tracking[ p ].count;

                ASSERT(c>=0 && c<=11 && p<12 && p>=0);

                mat_tracking[ p ].pos[ mat_tracking[ p ].count++ ] = piece_pos;
                mat_idx[ piece_pos ] = c;
            }
        }
    }
}

void
Board::check_mat_tracking()
{
    int tot = 0;
    for(int j=0;j<12;j++)
    {
        uint8_t p = j%6 + 1 + (j/6 ? WHITE : BLACK);
        for(int n = 0;n< mat_tracking[ j ].count; n++)
        {
            ASSERT(!OUT_OF_BOARD(mat_tracking[ j ].pos[n]));
            ASSERT(data[mat_tracking[ j ].pos[n]] == p);
        }
        (void)(p);
        tot += mat_tracking[ j ].count;
    }

    int piece_pos = 128;
    for(int i=7;i>=0;i--)
    {
        piece_pos -= 8;
        for(int j=7;j>=0;j--)
        {
            piece_pos--;
            if(data[piece_pos])
                tot--;
        }
    }
    ASSERT(!tot);
}
