/***************************************************************************
                 search_gui.h  -  Gui to view the search tree
                             -------------------
    begin                : Sat Oct 06 2007
    copyright            : (C) 2007 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __SEARCH_GUI_H__
#define __SEARCH_GUI_H__

#ifndef NO_GUI

#include "engine.h"
#include <QObject>
class QTreeWidget;
class QTreeWidgetItem;
class QApplication;

class SearchGui : public QObject
{
Q_OBJECT
    QApplication *app;
    QTreeWidget  *tree_widget;

    int max_ply;

public:
    const static int Italic =   1<<0;
    const static int Bold =     1<<1;
    const static int NoItalic = 1<<2;
    const static int NoBold =   1<<3;
    const static int Gray =     1<<4;
    const static int Red =      1<<5;
    const static int Green =    1<<6;
    const static int Blue =     1<<7;
    const static int Magenta =  1<<8;
    SearchGui(int& argc, char** argv);
    ~SearchGui();

    static void apply_flags(QTreeWidgetItem* w, int flags);
    void init_root();
    void new_root_level(int depth);
    void notify_msg(int ply, const char*, int flags);
    void notify(const Board *board, Move m, int ply, int depth, int heuval, int alpha, int beta, int flags);
    void notify_value(int ply, int value, int nodecount, int newflags);
    void notify_eval(int ply, int value, int alpha, int beta, int flags);
    void notify_hash(int ply, int lower, int upper, int depth, int alpha, int beta, Move best, bool write, int flags);
    void wait_input();
    void process_events();
};

#define DIFF_NODES (engine->processed_nodes-__nc)
#define GUI1(a) {uint64_t __nc = engine->processed_nodes; if(search_gui) { search_gui->a; } else {}
#define GUI2(a) if(search_gui) { search_gui->a; } else {} }
#define GUI(a) if(search_gui) search_gui->a; else {}
#define IFGUI(a) a

// #define GUI1(a)
// #define GUI2(a)

#else

class SearchGui
{
public:
    const static int Italic =   1<<0;
    const static int Bold =     1<<1;
    const static int NoItalic = 1<<2;
    const static int NoBold =   1<<3;
    const static int Gray =     1<<4;
    const static int Red =      1<<5;
    const static int Green =    1<<6;
    const static int Blue =     1<<7;
    const static int Magenta =  1<<8;

    SearchGui(int& argc, char** argv){}
    ~SearchGui(){}

    void init_root(){}
    void new_root_level(int depth){}
    void notify(Board *board, Move m, int ply, int depth, int heuval, int alpha, int beta, int flags){}
    void notify_value(int ply, int value, int nodecount, int newflags){}
    void notify_eval(int ply, int value, int alpha, int beta, int flags){}
    void notify_hash(int ply, int lower, int upper, int depth, int alpha, int beta, Move best, bool write, int flags){}
    void wait_input(){}
    void process_events(){}
};

#define GUI1(a)
#define GUI2(a)
#define GUI(a)
#define IFGUI(a)

#endif

#endif //__SEARCH_GUI_H__
