/***************************************************************************
                          movegen.cpp  -  description
                             -------------------
    begin                : ven ott 25 2002
    copyright            : (C) 2002-2005 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "board.h"
#include "engine.h"

/* macro to add a move in the move list */
#define ADDSIMPLEMOVE(f,tj)                                            \
{                                                                      \
                register Move* mtmp=mvg_curr++;                     \
                mtmp->from = (f);                                      \
                mtmp->to = (tj);                                       \
                mtmp->capture = 0;                                     \
                mtmp->flags = 0;                                       \
                mtmp->val = 0;                                         \
}

#define ADDMOVE(f,tj,x)                                                \
{                                                                      \
                register Move* mtmp=mvg_curr++;                     \
                mtmp->from = (f);                                      \
                mtmp->to = (tj);                                       \
                mtmp->capture = data[((uint8_t)(tj))];              \
                mtmp->flags = (x);                                     \
                mtmp->val = 0;                                         \
}

#define ADDPROMOTE(f,tj)                                               \
{                                                                      \
                register char kpt = data[(uint8_t)(tj)];            \
                register Move* mtmp=mvg_curr;                       \
                mtmp->from = (f);                                      \
                mtmp->to = (tj);                                       \
                mtmp->capture = (kpt);                                 \
                mtmp->flags = PROMOTEQUEEN;                            \
                mtmp->val = 0;                                         \
                mtmp++;                                                \
                mtmp->from = (f);                                      \
                mtmp->to = (tj);                                       \
                mtmp->capture = (kpt);                                 \
                mtmp->flags = PROMOTEKNIGHT;                           \
                mtmp->val = 0;                                         \
                mtmp++;                                                \
                mtmp->from = (f);                                      \
                mtmp->to = (tj);                                       \
                mtmp->capture = (kpt);                                 \
                mtmp->flags = PROMOTEBISHOP;                           \
                mtmp->val = 0;                                         \
                mtmp++;                                                \
                mtmp->from = (f);                                      \
                mtmp->to = (tj);                                       \
                mtmp->capture = (kpt);                                 \
                mtmp->flags = PROMOTEROOK;                             \
                mtmp->val = 0;                                         \
                mvg_curr+=4;                                           \
}

void
Board::find_knight_moves( uint8_t where)
{
    if(pins[where] & NFREE)
           return;

    KnightMove* hm= &Board::knightmoves[where];
    for(int i=hm->numm;i>=0;i--)
    {
        register uint8_t currpos = hm->jump[i];
        if(data[currpos] & color_to_move)
            continue;

        ADDMOVE(where,currpos,0);
    }
}

//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

void
Board::find_queen_moves( uint8_t where)
{
    find_bishop_moves(where);
    find_rook_moves(where);
}

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------

void
Board::find_king_moves( uint8_t where)
{
    uint8_t oldd = data[where];
    bool freerg = false;
    bool freelf = false;

    /* clear the place where the king is, so that the
        king is not 'protecting' cases near him */
    data[where] = 0;
#if TRACK_ATTACKS
    del_attacks(0, where);
#endif //TRACK_ATTACKS

    for(int i=7;i>=0;i--)
    {
        uint8_t currpos = where + kingmoves[i];
        if( OUT_OF_BOARD(currpos)
                || (data[currpos] & color_to_move)
                || (pins[currpos]&NFREE && data[currpos]==0)
                || under_attack(currpos,other_color))
            continue;
        //minimize calls to the expensive "under_attack"
        if(kingmoves[i]==RIGHT)
                freerg = true;
        else if(kingmoves[i]==LEFT)
                freelf = true;
        ADDMOVE(where,currpos,0);
    }

    /* test if castling is possible */
    if(!under_check)
    {
        if(color_to_move==WHITE)
        {
            if((castle_passing_mask & WCANCASTLEKS)
                && freerg // ie: !under_attack(POS_XY(5,0),BLACK)
                && data[ first_rank[1]+5 ]==0
                && data[ first_rank[1]+6 ]==0
                && !under_attack( first_rank[1]+6 ,BLACK))
            {
                ADDMOVE(where, first_rank[1]+6 ,CASTLEKINGSIDE);
            }
            if((castle_passing_mask & WCANCASTLEQS)
                && freelf // ie: !under_attack(POS_XY(3,0),BLACK)
                && data[ first_rank[1]+3 ]==0
                && data[ first_rank[1]+2 ]==0
                && data[ first_rank[1]+1 ]==0
                && !under_attack( first_rank[1]+2 ,BLACK))
            {
                ADDMOVE(where, first_rank[1]+2 ,CASTLEQUEENSIDE);
            }
        }
        else
        {
            if((castle_passing_mask & BCANCASTLEKS)
                && freerg // ie: !under_attack(POS_XY(5,7),WHITE)
                && data[ first_rank[0]+5 ]==0
                && data[ first_rank[0]+6 ]==0
                && !under_attack( first_rank[0]+6, WHITE))
            {
                ADDMOVE(where, first_rank[0]+6 ,CASTLEKINGSIDE);
            }
            if((castle_passing_mask & BCANCASTLEQS)
                && freelf // ie: !under_attack(POS_XY(3,7),WHITE)
                && data[ first_rank[0]+3 ]==0
                && data[ first_rank[0]+2 ]==0
                && data[ first_rank[0]+1 ]==0
                && !under_attack( first_rank[0]+2, WHITE))
            {
                ADDMOVE(where, first_rank[0]+2 ,CASTLEQUEENSIDE);
            }
        }
    }
    data[where] = oldd;
#if TRACK_ATTACKS
    add_attacks(0, where);
#endif //TRACK_ATTACKS
}

//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

void
Board::find_bishop_moves( uint8_t where)
{
    /* fast path if the bishop is not pinned */
    if( !(pins[where] & NFREE) )
    {
        for(int i=3;i>=0;i--)
        {
            register uint8_t currpos = where;
            register uint8_t currinc = bishmoves[i];

            do
            {
                currpos += currinc;

                if(OUT_OF_BOARD(currpos))
                    break;

                /* break if out of board or we found a where of the same color (or a stone) */
                if(data[currpos] & color_to_move)
                    break;

                ADDMOVE(where,currpos,0);
            }
            /* exit loop if the square is not empty (don't 'jump') */
            while(!IS_OF_COLOR(data[currpos],other_color));
        }
    }
    /* if we are pinned but we can move along the line  */
    else if(pins[where] & DIAG)
    {
        register uint8_t currinc = bishmoves[pins[where]&0x0f];

        /* do for the 2 allowed directions (back and forward) */
        for(int i=2;i>0;i--)
        {
            register uint8_t currpos = where;

            do
            {
                currpos += currinc;
                if(OUT_OF_BOARD(currpos))
                    break;
                if(data[currpos] & color_to_move)
                    break;
                ADDMOVE(where,currpos,0);
            }
            while(!IS_OF_COLOR(data[currpos],other_color));

            currinc = -currinc;
        }
    }
}

//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

void
Board::find_rook_moves( uint8_t where)
{
    /* fast path if the rook is not pinned */
    if(!(pins[where] & NFREE))
    {
        for(int i=3;i>=0;i--)
        {
            register uint8_t currpos = where;
            register uint8_t currinc = rookmoves[i];

            do
            {
                currpos += currinc;

                if(OUT_OF_BOARD(currpos))
                    break;

                /* break if out of board or we found a where of the same color (or a stone) */
                if(data[currpos] & color_to_move)
                    break;

                ADDMOVE(where,currpos,0);
            }
            /* exit loop if the square is not empty (don't 'jump') */
            while(!IS_OF_COLOR(data[currpos],other_color));
        }
    }
    /* if we are pinned but we can move along the line  */
    else if(pins[where] & COLM)
    {
        register uint8_t currinc = rookmoves[pins[where]&0x0f];

        /* do for the 2 allowed directions (back and forward) */
        for(int i=2;i>0;i--)
        {
            register uint8_t currpos = where;

            do
            {
                currpos += currinc;
                if(OUT_OF_BOARD(currpos))
                    break;
                if(data[currpos] & color_to_move)
                    break;
                ADDMOVE(where,currpos,0);
            }
            while(!IS_OF_COLOR(data[currpos],other_color));

            currinc = -currinc;
        }
    }
}

//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

void
Board::find_pawn_moves(uint8_t where)
{
    /*static const uint8_t second_rank[] = { 0x60, 0x10 };
    static const uint8_t seventh_rank[] = { 0x10, 0x60 };
    static const uint8_t passant_rank[] = { 0x30, 0x40 };*/

    uint8_t is_white = IS_WHITE(color_to_move);
    uint8_t updir = up_dir[is_white];
    uint8_t row_p = ROW_OF(where);

    register uint8_t up = where + updir;

    uint8_t cginfo=pins[where];
    bool free = !(cginfo & NFREE);
    bool cangoforw = true;
    bool cantakerg = X(where) != 7;
    bool cantakelf = X(where) != 0;

    /* if the pawn is pinned set a few marks */
    if(!free)
    {
        /* nm is the direction of the pinning
            FIXME: theese checks depend on the layout of the bishop/rook moves array */
        uint8_t nm = cginfo & 0x0f;
        cangoforw = ((cginfo & COLM) && (nm>=2));
        cantakerg &= ((cginfo & DIAG)
                      && ((nm&1) == 1-is_white));
        cantakelf &= ((cginfo & DIAG)
                      && ((nm&1) == is_white));
    }

    /* if the pawn is not going to promote... */
    if(row_p != seventh_rank[is_white])
    {
        /* if can the pawn move forward... */
        if(IS_VOID(data[up]) && cangoforw)
        {
            uint8_t up2 = up + updir;
            ADDSIMPLEMOVE(where,up);

            /* if the pawn can move of two */
            if(row_p == second_rank[is_white] && IS_VOID(data[up2]))
                ADDMOVE(where,up2,PAWNOF2);
        }

        /* can the pawn capture on the right or take en passant? */
        if(cantakerg) //not in the last col
        {
            uint8_t upright = up + RIGHT;

            if(COLOR_OF(data[upright])==other_color)
            {
                ADDMOVE(where,upright,0);
            }
            else if( (row_p == passant_rank[is_white]) &&
                     ((castle_passing_mask&0x0f) == COL_OF(upright)))
            {
                /*  uff, looks like this odd check is really needed, as the en passant
                    cannot be done if there is the king on one side and an enemy
                    ROOK (or queen) on the other                                     */
                uint8_t right_piece = 0;
                uint8_t left_piece = 0;
                uint8_t tmp_pos;

                tmp_pos = where + LEFT;
                while(1)
                {
                    if(OUT_OF_BOARD(tmp_pos))
                        break;
                    if(data[tmp_pos])
                    {
                        left_piece = data[tmp_pos];
                        break;
                    }
                    tmp_pos += LEFT;
                }
                tmp_pos = where + 2*RIGHT;
                while(1)
                {
                    if(OUT_OF_BOARD(tmp_pos))
                        break;
                    if(data[tmp_pos])
                    {
                        right_piece = data[tmp_pos];
                        break;
                    }
                    tmp_pos += RIGHT;
                }
                if(!(left_piece == (KING|color_to_move)
                        && (right_piece&~BISHOP)==(ROOK|other_color))
                     && !(right_piece == (KING|color_to_move)
                        && (left_piece&~BISHOP)==(ROOK|other_color)))
                {
                    ADDMOVE(where,upright,ENPASSANT);
                }
            }
        }

        /* can the pawn capture on the left or take en passant? */
        if(cantakelf)   //not in the first col
        {
            uint8_t upleft  = up + LEFT;

            if(COLOR_OF(data[upleft])==other_color)
            {
                ADDMOVE(where,upleft,0);
            }
            else if( (row_p == passant_rank[is_white]) &&
                     ((castle_passing_mask & 0x0f) == COL_OF(upleft)))
            {
                /*  uff, looks like this odd check is really needed, as the en passant
                    cannot be done if there is the king on one side and an enemy
                    ROOK (or queen) on the other                                     */
                uint8_t right_piece = 0;
                uint8_t left_piece = 0;
                uint8_t tmp_pos;

                tmp_pos = where + 2*LEFT;
                while(1)
                {
                    if(OUT_OF_BOARD(tmp_pos))
                        break;
                    if(data[tmp_pos])
                    {
                        left_piece = data[tmp_pos];
                        break;
                    }
                    tmp_pos += LEFT;
                }
                tmp_pos = where + RIGHT;
                while(1)
                {
                    if(OUT_OF_BOARD(tmp_pos))
                        break;
                    if(data[tmp_pos])
                    {
                        right_piece = data[tmp_pos];
                        break;
                    }
                    tmp_pos += RIGHT;
                }
                if(!(left_piece == (KING|color_to_move)
                        && (right_piece&~BISHOP)==(ROOK|other_color))
                     && !(right_piece == (KING|color_to_move)
                        && (left_piece&~BISHOP)==(ROOK|other_color)))
                {
                    ADDMOVE(where,upleft,ENPASSANT);
                }
            }
        }
    }
    else
    {
        /* can we go forw? */
        if(IS_VOID(data[up]) && cangoforw)
        {
            ADDPROMOTE(where,up);
        }

        if(cantakerg) //not in the last col
        {
            uint8_t upright = up + RIGHT;

            if(COLOR_OF(data[upright])==other_color)
            {
                ADDPROMOTE(where,upright);
            }
        }

        if(cantakelf)   //not in the first col
        {
            uint8_t upleft  = up + LEFT;

            if(COLOR_OF(data[upleft])==other_color)
            {
                ADDPROMOTE(where,upleft);
            }
        }
    }
}

//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

/* fill the mvs array (that we suppose to be big enough, etc) with all
   the legal moves for the player on move */
int Board::find_moves(Move* mvs)
{
    if(under_check == 0xff)
        find_check_and_pins();
    find_other_pins();

    /* setup some useful data */
    mvg_curr = mvs;

    /* if not double check... */
    if(under_check<2)
    {
        int mt = (color_to_move == BLACK ? -1 : +5);
        for(int i=mat_tracking[ROOK+mt].count-1;i>=0;i--)
            find_rook_moves( mat_tracking[ROOK+mt].pos[i]);
        for(int i=mat_tracking[BISHOP+mt].count-1;i>=0;i--)
            find_bishop_moves( mat_tracking[BISHOP+mt].pos[i]);
        for(int i=mat_tracking[QUEEN+mt].count-1;i>=0;i--)
            find_queen_moves( mat_tracking[QUEEN+mt].pos[i]);
        for(int i=mat_tracking[KNIGHT+mt].count-1;i>=0;i--)
            find_knight_moves( mat_tracking[KNIGHT+mt].pos[i]);
        for(int i=mat_tracking[PAWN+mt].count-1;i>=0;i--)
            find_pawn_moves( mat_tracking[PAWN+mt].pos[i]);
        for(int i=mat_tracking[KING+mt].count-1;i>=0;i--)  /* ! */
            find_king_moves( mat_tracking[KING+mt].pos[i]);

        /* if under check select only the moves that protect the king */
        if(under_check==1)
        {
            int n=mvg_curr-mvs;
            mvg_curr=mvs;
            for(int i=0;i<n;i++)
                if(pins[mvs[i].to]&NFREE   //ok, interposition/capture
                   || ((mvs[i].flags == ENPASSANT) && //ok, taking a checking pawn en-passant
                        pins[ROW_OF(mvs[i].from) | COL_OF(mvs[i].to)]&NFREE)
                   || PIECE_OF(data[mvs[i].from])==KING) //ok, moving king
                          *mvg_curr++=mvs[i];
        }

        return mvg_curr-mvs;
    }

    /* under double check only generate king moves */
    find_king_moves( king_pos[IS_WHITE(color_to_move)]);
    return mvg_curr-mvs;
}
