/***************************************************************************
                    board.h  -  Board related definitions
                             -------------------
    begin                : Fri Jan 11 2002
    copyright            : (C) 2002-2007 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __BOARD_H__
#define __BOARD_H__

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include "utils.h"
#include "hash.h"
#include "move.h"
#include "board_defs.h"

/*******************************************************************************
    the chessboard is a 16x8 arrays, and is accessed with 1-byte indices
    whose binary form look like: 0yyy0xxx
    where xxx and yyy are 3bits (0 ... 7) that store the x and y coords.
    The 4th and 8th bits are 0 if the index is valid, so an invalid (out
    of board) index can be detected testing with 10001000 (0x88)

    Chess pieces. Keep these numbers because in a few place it is useful to be
    able to assume that QUEEN=ROOK|BISHOP, ie if you want to test if a piece
    moves diagonally you have just to test (piece&(~ROOK) == BISHOP)
*******************************************************************************/


#define TRACK_ATTACKS 0

class PACKED Board
{
public:/* a struct to keep the info of which pawns are in a row */
    class LinePawns
    {
    public:
        uint8_t count;
        Array<uint8_t, 7>::Type pos;//uint8_t pos[7];
    };

    /* structure to track material */
    class MatTrack
    {
    public:
        uint8_t count;
        Array<uint8_t, 11>::Type pos; //uint8_t pos[11];
    };

    /* structure to track attacks */
    class AttackList
    {
    public:
        uint8_t count;
        uint8_t curr;
        Array<uint8_t, 10>::Type piece;//uint8_t piece[10];
    };

    // utility data
    static uint8_t b_center[];
    static uint8_t b_start[];
    static uint8_t b_castle_adj[];
    static uint8_t up_dir[];
    static char piecename[];

    DEF2_A88(data, mat_idx);
    DEF2_A88(pins, oth_pins);
    DEF2_A88(castle_adj, boh_nothing);

#if TRACK_ATTACKS
    /* attack tracking relative data */
    DEF2_A88(b_attacks, w_attacks);
    static uint8_t attack_dirs[];
#endif //TRACK_ATTACKS

    /* flags of the current position */
    uint8_t castle_passing_mask;
    uint8_t fifty;

    /* color on move (and the other one) */
    uint8_t color_to_move;
    uint8_t other_color;

    /* mark black/white king position */
    Array<uint8_t, 2>::Type king_pos;//uint8_t king_pos[2];
    Array<uint8_t, 2>::Type first_rank;//uint8_t first_rank[2];
    Array<uint8_t, 2>::Type second_rank;//uint8_t second_rank[2];
    Array<uint8_t, 2>::Type passant_rank;//uint8_t passant_rank[2];
    Array<uint8_t, 2>::Type seventh_rank;//uint8_t seventh_rank[2];

    /* 0xff if not calculated yet, 0/1/2 if no check, check or double check */
    uint8_t under_check;

    /* pawn tracking relative data */
    Array<Array<LinePawns, 8>::Type, 2>::Type line_pawns;

    /* material tracking relative data */
    Array<MatTrack, 12>::Type mat_tracking;

    /* hash relative data and functions */
    HashKey hash;
    static HashKey hash_keys[14*128];
    static HashKey hash_key_toggle;

    /* data for move generation */
    Move*   mvg_curr;
    static KnightMove* knightmoves;
    static uint8_t     kingmoves[];
    static uint8_t     bishmoves[];
    static uint8_t     rookmoves[];

    /* num of moves from the start, for fen loading/saving */
    int     num_moves;

    /* simple values, PAWN=1, KNIGHT=3, etc */
    static int16_t simple_values[];



    /* move generators prototypes */
    void find_rook_moves(uint8_t where);
    void find_bishop_moves(uint8_t where);
    void find_queen_moves(uint8_t where);
    void find_knight_moves(uint8_t where);
    void find_pawn_moves(uint8_t where);
    void find_king_moves(uint8_t where);
    static void init_knight_moves();

    /* pawn tracking relative functions */
    void add_pawn(uint8_t col, uint8_t where);
    void del_pawn(uint8_t col, uint8_t where);
    void recalc_line_pawns();
    void check_line_pawns();

    /* material tracking relative functions */
    void mat_add(uint8_t piece, uint8_t pos);
    void mat_remove(uint8_t piece, uint8_t pos);
    void mat_move(uint8_t piece, uint8_t from, uint8_t to);
    void recalc_mat_tracking();
    void check_mat_tracking();

#if TRACK_ATTACKS
    /* attack tracking relative functions */
    void add_attacks(uint8_t piece, uint8_t where);
    void del_attacks(uint8_t piece, uint8_t where);
    void replace_attacks(uint8_t piece1, uint8_t piece2, uint8_t where);
    void recalc_attacks();
    void print_attacks();
    void check_attacks();
#endif //TRACK_ATTACKS

    /* hash relative functions */
    static void init_hash_keys(const char *file);

    Board();

    void init_globals();
    void set_as_default();
    bool under_attack(uint8_t pos, uint8_t attacker);
    void find_check_and_pins();
    void find_other_pins();
    bool move_is_check(const Move& m);
    int  list_attackers(uint8_t pos, uint8_t attacker_col,
                                            A88 pins, AttackList* a);
    int  propagate_see(uint8_t victim, int numa, AttackList* a,
                                            int numd, AttackList* d);
    int  move_see_val(const Move& m);
    int  find_moves(Move* m);
    int  find_captures(Move* m);

    int16_t evaluate(uint8_t eng_col, int16_t alpha = -INF, int16_t beta = INF);
    int16_t dummy_evaluate();
    bool insufficient_material();

    uint16_t compress_move(const Move& m) const;
    Move uncompress_move(uint16_t m) const;
    void do_move(const Move& m, SaveBuf& s);
    void undo_move(const Move& m, const SaveBuf& s);
    void do_null_move(SaveBuf& s);
    void undo_null_move(const SaveBuf& s);

    HashKey move_hash(const Move& m) const;
    void  recalc_hash();

    Move         move_from_string(const char* str);
    static char* move_to_coord(char* str, const Move& mv);
    char*        move_to_alg(char* str, const Move& mv);

    void print_board();        //prints colored board to stdout
    void read_board(char* brd, char* color, char* castle,
                    char* passing, int moves_to_draw, int num_moves);
    void read_board(char *str);  //reads from f a Forsythe-Edwards notation board
    char* to_fen(char *str); //writes to f a Forsythe-Edwards notation board
};

#endif //__BOARD_H__
