/***************************************************************************
        attack.cpp  -  Implmentation of attacking-related functions
                             -------------------
    begin                : Dom Oct 30 2005
    copyright            : (C) 2005 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "board.h"


bool
Board::under_attack(uint8_t pos,uint8_t attacker)
{
    A88 d = data;
    uint8_t check;
    uint8_t killer;

#if TRACK_ATTACKS
#ifdef DEBUG
    check_attacks();
#endif

    /* check bishop or rook attacks */
    A88 attacks = IS_WHITE(attacker) ? w_attacks : b_attacks;
    if(attacks[pos])
        return true;
#endif //TRACK_ATTACKS

    /* check if attacked by a knight */
    killer = KNIGHT|attacker;
    {
        KnightMove* hm= &knightmoves[pos];
        for(int i=hm->numm;i>=0;i--)
        {
            register uint8_t tmp = hm->jump[i];
            if(d[tmp] == killer)
                return true;
        }
    }

#if !TRACK_ATTACKS
    uint8_t inc;

    /* check bishop (or queen) attacks */
    killer = BISHOP|attacker;

    for(int i=3;i>=0;i--)
    {
        inc = bishmoves[i];
        check = pos;
        do
        {
            check += inc;
            if(OUT_OF_BOARD(check))
                break;
            if((d[check] & ((uint8_t)~ROOK)) ==  killer)
                return true;
        }
        while(IS_VOID(d[check]));
    }

    /* check rook (or queen) attacks */
    killer = ROOK|attacker;

    for(int i=3;i>=0;i--)
    {
        inc = rookmoves[i];
        check = pos;
        do
        {
            check += inc;
            if(OUT_OF_BOARD(check))
                break;
            if((d[check] & ((uint8_t)~BISHOP)) ==  killer)
                return true;
        }
        while(IS_VOID(d[check]));
    }
#endif // !TRACK_ATTACKS

    /* check if attacked by a pawn */
    {
        killer = PAWN|attacker;
        check = pos - up_dir[IS_WHITE(attacker)] + RIGHT;
        if(!OUT_OF_BOARD(check) && (d[check] == killer))
            return true;

        check += 2*LEFT;
        if(!OUT_OF_BOARD(check) && (d[check] == killer))
            return true;
    }

    /* check if attacked by the other king */
    {
        killer = KING|attacker;
        for(int i=7;i>=0;i--)
        {
            check = pos + kingmoves[i];
            if(OUT_OF_BOARD(check))
                continue;
            if(d[check] == killer)
                return true;
        }
    }
    return false;
}

/* count attackes (end possibly defenders) of a piece/square.
    an array is passed to give info about pinned attacker pieces */
int
Board::list_attackers(uint8_t pos, uint8_t attacker, A88 pins, AttackList* a)
{
    A88 d = data;
    uint8_t killer;
    int num_atck = 0;

    a[num_atck].curr = a[num_atck].count = 0;

    /* check if attacked by a knight */
    killer = KNIGHT|attacker;
    {
        KnightMove* hm= &knightmoves[pos];
        for(int i=hm->numm;i>=0;i--)
        {
            register uint8_t tmp = hm->jump[i];
            if(d[tmp] == killer && !pins[tmp])
                a[num_atck].piece[a[num_atck].count++] = KNIGHT;
        }
    }

    /* check if attacked by the king.
        the king cannot be pinned, at least :) */
    {
        int deltax = X(king_pos[IS_WHITE(attacker)])-X(pos);
        int deltay = Y(king_pos[IS_WHITE(attacker)])-Y(pos);
        if(ABS(deltax)<=1 && ABS(deltay)<=1)
            a[num_atck].piece[a[num_atck].count++] = KING;
    }

    if(a[num_atck].count)
        num_atck++;

#if TRACK_ATTACKS
    A88 attacks = IS_WHITE(attacker) ? w_attacks : b_attacks;
    uint8_t up1 = up_dir[1-IS_WHITE(attacker)] + LEFT;
    uint8_t up2 = up_dir[1-IS_WHITE(attacker)] + RIGHT;

    for(int r=0;r<8;r++)
    {
        uint8_t m = 1<<r;

        if(!(attacks[pos] & m))
            continue;

        uint8_t inc = -attack_dirs[r];
        uint8_t currpos = pos+inc;
        a[num_atck].curr = a[num_atck].count = 0;

        if( (inc == up1 || inc == up2)
             && !OUT_OF_BOARD(currpos)
             && d[currpos]==(PAWN|attacker) )
        {
            /* we found a pawn, enlist it and skip */
            a[num_atck].piece[a[num_atck].count++] = PAWN;
            currpos += inc;
        }

        while(!OUT_OF_BOARD(currpos))
        {
            if(d[currpos])
            {
                a[num_atck].piece[a[num_atck].count++] = PIECE_OF(d[currpos]);
                if(!(attacks[currpos] & m))
                    break;
            }

            currpos += inc;
        }

        if(a[num_atck].count)
            num_atck++;
    }
#else //TRACK_ATTACKS
    /* check bishop (or queen) attacks */
    killer = BISHOP|attacker;

    for(int i=3;i>=0;i--)
    {
        uint8_t inc = bishmoves[i];
        uint8_t currpos = pos+inc;
        a[num_atck].curr = a[num_atck].count = 0;

        if(OUT_OF_BOARD(currpos))
            continue;
        if( ( (0xf0&(inc+1)) == up_dir[1-IS_WHITE(attacker)])
             && d[currpos]==(PAWN|attacker)
             && (!pins[currpos] || ((pins[currpos]&DIAG) &&
           (bishmoves[pins[currpos]&0x0f]==inc ||
             bishmoves[pins[currpos]&0x0f]==(uint8_t)-inc))))
        {
            /* we found a pawn, enlist it and skip */
            a[num_atck].piece[a[num_atck].count++] = PAWN;
            currpos += inc;
        }

        while(1)
        {
            if(OUT_OF_BOARD(currpos))
                break;
            if( ((d[currpos] & ((uint8_t)~ROOK))==killer)
                && (!pins[currpos] || ((pins[currpos]&DIAG) &&
                  (bishmoves[pins[currpos]&0x0f]==inc ||
                    bishmoves[pins[currpos]&0x0f]==(uint8_t)-inc))))
                a[num_atck].piece[a[num_atck].count++] = PIECE_OF(d[currpos]);
            else if(d[currpos])
                break;
            currpos += inc;
        }
        if(a[num_atck].count)
            num_atck++;
    }

    /* check rook (or queen) attacks */
    killer = ROOK|attacker;

    for(int i=3;i>=0;i--)
    {
        uint8_t inc = rookmoves[i];
        uint8_t currpos = pos+inc;
        a[num_atck].curr = a[num_atck].count = 0;

        while(1)
        {
            if(OUT_OF_BOARD(currpos))
                break;
            if( ((d[currpos] & ((uint8_t)~BISHOP)) ==  killer)
                 && (!pins[currpos] || ((pins[currpos]&COLM) &&
                  (rookmoves[pins[currpos]&0x0f]==inc ||
                    rookmoves[pins[currpos]&0x0f]==(uint8_t)-inc))))
                a[num_atck].piece[a[num_atck].count++] = PIECE_OF(d[currpos]);
            else if(d[currpos])
                break;
            currpos += inc;
        }
        if(a[num_atck].count)
            num_atck++;
    }
#endif //TRACK_ATTACKS

    return num_atck;
}

int
Board::propagate_see(uint8_t victim, int numa, AttackList* a, int numd, AttackList* d)
{
    uint8_t newvict = KING;
    int idxnewvict = -1;

    for(int i=0;i<numa;i++)
    if(a[i].curr < a[i].count)
    {
        if(idxnewvict==-1 || simple_values[a[i].piece[a[i].curr]] < simple_values[newvict])
        {
            idxnewvict = i;
            newvict = a[i].piece[a[i].curr];
        }
    }

    if(idxnewvict == -1)
        return 0;

    a[idxnewvict].curr++;
    return MAX(0, simple_values[victim] - propagate_see(newvict, numd, d, numa, a) );
}

/* calculate how much is winning the exchange sequence stating with Move */
int
Board::move_see_val(const Move& m)
{
    AttackList atck[12];
    AttackList def[12];
    int atck_num;
    int def_num;

    uint8_t piece = data[m.from];
    int bof = simple_values[PIECE_OF(data[m.to])] - simple_values[PIECE_OF(piece)];
    if(bof>0)
        return bof;

    data[m.from] = 0;
#if TRACK_ATTACKS
    del_attacks(piece, m.from);
#endif //TRACK_ATTACKS
    atck_num = list_attackers(m.to, color_to_move, pins, atck);
    def_num = list_attackers(m.to, other_color, oth_pins, def);
    data[m.from] = piece;
#if TRACK_ATTACKS
    add_attacks(piece, m.from);
#endif //TRACK_ATTACKS

    return simple_values[PIECE_OF(m.capture)] -
        propagate_see( PIECE_OF(data[m.from]), def_num, def, atck_num, atck);
}

