/***************************************************************************
                          sort.cpp  -  description
                             -------------------
    begin                : mar ott 22 2002
    copyright            : (C) 2002-2005 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "engine.h"

void
Engine::incremental_sort_moves(Move* mv,int num_mv)
{
    int16_t bmvv = -INF;
    int bmvi = 0;
    for(int j=0;j<num_mv;j++)
    {
        if(mv[j].val > bmvv)
        {
            bmvv = mv[j].val;
            bmvi = j;
        }
    }
    SWITCH(mv[0], mv[bmvi]);
}

void
Engine::sort_moves(Move* m,int num)
{
    Move mv_tmp[200];
    Move *in = m;
    Move *out = mv_tmp;
    Move *crin = in;
    Move *crout = out;

    //sort 2-blocks
    for(int j=0;j<num;j+=2)
    {
        int n = MIN(2, (num-j));

        if(n==2)
        {
            if(crin[0].val < crin[1].val)
                SWITCH(crin[0],crin[1]);
        }
        crin+=2;
    }

    //now sort 8+ blocks
    for(int i=2; i<num; i<<=1)
    {
        int segs=i<<1;
        crin=in;
        crout=out;
        SWITCH(in,out);
        for(int j=0; j<num; j+=segs)
        {
            int n  = MIN(segs, (num-j));
            int n1 = i ;                //first half
            int n2 = n - n1;


            if(n2<=0) //only one half? if so copy
            {
                for(int i=0;i<n;i++)
                    crout[i] = crin[i];
                break;
            }


            int tmp1 = 0, tmp2 = n1;

            while(1)
            {
                if(crin[tmp1].val > crin[tmp2].val)
                {
                    *(crout++) = crin[tmp1++];
                    if(tmp1 == n1)
                    {
                        for(int i=tmp2; i<n;i++)
                            *(crout++) = crin[tmp2++];
                        break;
                    }
                }
                else
                {
                    *(crout++) = crin[tmp2++];
                    if(tmp2 == n)
                    {
                        for(int i=tmp1; i<n1;i++)
                            *(crout++) = crin[tmp1++];
                        break;
                    }
                }
            }

            crin+=segs;
        }
    }
    if(in!=m)
        memcpy(m,mv_tmp,num*sizeof(Move));
}
