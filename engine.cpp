/***************************************************************************
                          engine.cpp  -  description
                             -------------------
    begin                : mer ott 23 2002
    copyright            : (C) 2002-2005 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "engine.h"
#include "utils.h"
#include "commands.h"
#include "search_gui.h"
#include <string.h>

Engine* Engine::instance = NULL;

Engine* Engine::eng()
{
    return instance;
}

Engine::Engine()
{
    instance = this;

    mv_done             = new Move[4096];
    mv_done_num         = 0;

    save_buf            = new SaveBuf[4096];
    save_buf_num        = 0;

    create_hash();
    reset_hash();

    analysis_limit      = TIME_LIMIT;
    time_fixed          = 2;

    st_computer_color   = BLACK;
    status              = PLAYING;
    eng_status          = PLAYING;
    thinking            = false;
    status_string       = NULL;
    post                = true;
    ponder              = false;

    io_xboard           = false;
    io_san              = false;
    io_colors           = false;

    current_root        = NULL;

    /* register engine commands */
    register_commands();

    /* register engine variables */
    register_variables();

    /* open log file */
    log = fopen("rattatechess.log", "w");

    /* open the book */
    book_mmap = NULL;

    srand(time(NULL));
    board.set_as_default();

    load_ini();
}

unsigned char Engine::color_to_move()
{
    return board.color_to_move;
}

unsigned char Engine::computer_color()
{
    return st_computer_color;
}

int Engine::get_status()
{
    return status;
}

int Engine::move_number()
{
    return board.num_moves;
}

void Engine::start()
{
    st_computer_color = board.color_to_move;
    status = PLAYING;
    status_string = NULL;
    eng_status = PLAYING;
    //output("starting!\n");
    if(thinking)
        longjmp(back, 1);
}

void Engine::start(unsigned char color)
{
    st_computer_color = color;
    status = PLAYING;
    status_string = NULL;
    eng_status = PLAYING;
    if(thinking)
        longjmp(back, 1);
}

void Engine::force()
{
    st_computer_color = 0;
    eng_status = FORCING;
    if(thinking)
        longjmp(back, 1);
}

void Engine::analyze()
{
    st_computer_color = 0;
    HashKey h = board.hash;
    find_best_move();

    /* exited analyzing without any move being done?
        (because of a near mate or a book entry found) */
    while(eng_status==ANALYZING && h == board.hash)
        process_input();
}

void Engine::do_ponder()
{
    HashKey h = board.hash;
    find_best_move();

    /* exited pondering without any move being done?
        (because of a near mate or a book entry found) */
    while(ponder && eng_status==PLAYING &&
         st_computer_color == board.other_color && h == board.hash)
        process_input();
}

void Engine::move_now()
{
    if(!thinking)
    {
        output("You cannot use this command now!\n");
        return;
    }

    longjmp(back, 1);
}

/* do the move and update a couple of things */
void Engine::move(Move mv)
{
    /* save the move in the stack of played moves (from the starting position) */
    mv_done[mv_done_num++] = mv;

    board.do_move(mv, save_buf[save_buf_num++]);

    if(board.insufficient_material())
    {
        status=_12;
        status_string = "Insufficient material";
        return;
    }

    /* 50mvs rule */
    if(board.fifty >= 100)
    {
        status=_12;
        status_string = "Drawn by 50-move rule";
        return;
    }

    /* draw by 3fold repetition */
    if(check_repetition(3))
    {
        status = _12;
        status_string = "Drawn by 3-fold repetition";
        return;
    }

    Move mvs[250];
    if(board.find_moves(mvs)==0)      //mate or stalemate...
    {
        if(board.under_check)
        {
            if(board.color_to_move==WHITE)
            {
                status = _01;
                status_string = "Black mates";
            }
            else
            {
                status = _10;
                status_string = "White mates";
            }
        }
        else
        {
            status = _12;
            status_string = "Stalemate";
        }
        return;
    }
}

void Engine::retract_move()
{
    if(mv_done_num>1)
    {
        board.undo_move(mv_done[--mv_done_num], save_buf[--save_buf_num]);
        board.undo_move(mv_done[--mv_done_num], save_buf[--save_buf_num]);
    }
}

void Engine::undo()
{
    if(mv_done_num>0)
        board.undo_move(mv_done[--mv_done_num], save_buf[--save_buf_num]);
    status = PLAYING;
}

void Engine::new_game()
{
    mv_done_num     = 0;
    analysis_limit  = TIME_LIMIT;
    time_fixed      = 2;
    st_computer_color   = BLACK;
    status          = PLAYING;
    eng_status      = PLAYING;

    strcpy(opponent, "<unknown>");
    w_rating  = 0;
    b_rating  = 0;

    board.set_as_default();
    reset_hash();
}

void Engine::set_depth(int d)
{
    analysis_limit  = DEPTH_LIMIT;
    st_depth        = d;
}

void Engine::set_max_nodes(int d)
{
    analysis_limit  = NODES_LIMIT;
    st_nodes        = d;
}

void Engine::set_time_fixed(int t)
{
    analysis_limit  = TIME_LIMIT;
    time_fixed      = t;
}

void Engine::set_time_control(int mps, int basesec, int inc)
{
    analysis_limit  = TIME_LIMIT;
    time_fixed      = 0;
    time_mps        = mps;
    time_base       = basesec;
    time_inc        = inc;
    time_clock_csec = time_base*100;
}

void Engine::set_my_clock(int t)
{
    time_clock_csec = t;
}

void Engine::set_other_clock(int t)
{
    time_oth_clock_csec = t;
}

void Engine::print_board()
{
    board.print_board();
}

void Engine::read_board(char* brd, char* color, char* castle,
                        char* passing, int moves_to_draw, int num_moves)
{
    board.read_board(brd, color,castle,passing,moves_to_draw,num_moves);
    mv_done_num     = 0;
    reset_hash();
    if(thinking)
        longjmp(back, 1);
}

void Engine::read_board(char* str)
{
    board.read_board(str);
    mv_done_num     = 0;
    reset_hash();
    if(thinking)
        longjmp(back, 1);
}

void Engine::read_board(FILE* f)
{
    char str[256];
    fgets(str,256,f);
    board.read_board(str);
}

void Engine::write_board(FILE* f)
{
    fputs( board.to_fen(BigStr().data()), f );
}

bool Engine::check_time()
{
    processed_nodes++;

    if( eng_status == PLAYING &&
            analysis_limit == NODES_LIMIT &&
            processed_nodes > (unsigned int)st_nodes)
        longjmp(back, 1);

    if( (processed_nodes%4096) == 0 )
    {
        if( eng_status == PLAYING &&
                analysis_limit == TIME_LIMIT &&
                current_time()>=max_think_time )
            longjmp(back, 1);

        if(search_gui) search_gui->process_events();

        if(input_available())
        {
            int n = mv_done_num;
            do
            {
                process_input();

                if(n != mv_done_num)
                    longjmp(back, 1);

                if(eng_status == PLAYING && !ponder
                        && st_computer_color == board.other_color)
                    longjmp(back, 1);
            }
            while(input_available());
        }
    }
    return false;
}

//very primitive
void Engine::calc_best_time()
{
    if(time_fixed)
        time_best_csec = time_fixed * 100;
    else
    {
        int nummoves = (time_mps == 0) ? 30 :
                MAX((time_mps - (board.num_moves % time_mps)), 30);
        time_best_csec = time_clock_csec / nummoves + time_inc * 100;
        //time_best_csec = time_clock_csec / nummoves * (50 + rand()%300) / 200  + time_inc * 100;
    }
}

uint64_t Engine::perft(int lev)
{
    Move mv_stack[200];
    int num;
    num = board.find_moves(mv_stack);

    if(lev==1)
        return num;
    else
    {
        uint64_t retv=0;
        for(int i=0;i<num;i++)
        {
            board.do_move(mv_stack[i], save_buf[save_buf_num++]);
            retv+=perft(lev-1);
            board.undo_move(mv_stack[i], save_buf[--save_buf_num]);
        }
        return retv;
    }
}

void Engine::autotune()
{
    struct Tunable { const char* name; int& param; int start; int end; int step; };
    Tunable data[] = {
        { "v_search_threat_threshold", v_search_threat_threshold, 150, 720, 190 },
        { "v_search_threat_extension", v_search_threat_extension, 40, 120, 80 },
        { "v_search_null_reduction", v_search_null_reduction, 200, 400, 100 },
    };
    int numparams = sizeof(data)/sizeof(Tunable);

    for(int i=0;i<3;i++)
    for(int p=0;p<numparams;p++)
    {
        int oldv = data[p].param;
        int best = 0;
        int best_goodness = -1;
        int worst_goodness = 99999;

        for(data[p].param = data[p].start; data[p].param <= data[p].end; data[p].param += data[p].step)
        {
            output("\n");
            for(int k=0;k<numparams;k++)
                output("%s = %d%s\n", data[k].name, data[k].param, k==p ? " <--" : "");
            int goodness = run_epd_prog("self", 1, "epd/abbe.epd");
            if(goodness > best_goodness)
            {
                best_goodness = goodness;
                best = data[p].param;
            }
            if(goodness < worst_goodness)
                worst_goodness = goodness;
        }

        if(worst_goodness == best_goodness)
            data[p].param = oldv;
        else
            data[p].param = best;
    }

    output("\n");
    output("FINAL REPORT:\n");
    for(int k=0;k<numparams;k++)
        output("%s = %d\n", data[k].name, data[k].param);
    exit(0);
}
