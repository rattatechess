/***************************************************************************
                          check.cpp  -  description
                             -------------------
    begin                : Mon Sep 19 2005
    copyright            : (C) 2005 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "board.h"


void
Board::add_pawn(uint8_t col, uint8_t where)
{
    LinePawns *lp = &line_pawns[IS_WHITE(col)][X(where)];
    int row = IS_WHITE(col) ? Y(where) : 7-Y(where);
    int f = lp->count;

#ifdef DEBUG
    ASSERT(lp->count < 7);
    for(int i=0;i<lp->count;i++)
        ASSERT(lp->pos[i] != row);
#endif

    while(f && (lp->pos[f-1]>row))
    {
        lp->pos[f] = lp->pos[f-1];
        f--;
    }
    lp->pos[f] = row;
    lp->count++;
}

void
Board::del_pawn(uint8_t col, uint8_t where)
{
    LinePawns *lp = &line_pawns[IS_WHITE(col)][X(where)];
    int row = IS_WHITE(col) ? Y(where) : 7-Y(where);
    int f = 0;

    while(lp->pos[f] != row)
    {
        f++;
        ASSERT(f < lp->count);
    }
    while(f < lp->count)
    {
        f++;
        lp->pos[f-1] = lp->pos[f];
    }
    lp->count--;
}

void
Board::recalc_line_pawns()
{
    for(int i=0;i<2;i++)
    for(int j=0;j<8;j++)
        line_pawns[i][j].count = 0;

    int piece_pos = 128;
    for(int i=7;i>=0;i--)
    {
        piece_pos -= 8;
        for(int j=7;j>=0;j--)
        {
            piece_pos--;
            if(PIECE_OF(data[piece_pos])==PAWN)
                add_pawn(data[piece_pos], piece_pos);
        }
    }
}

void
Board::check_line_pawns()
{
    for(int i=0;i<2;i++)
    for(int j=0;j<8;j++)
    {
        LinePawns *lp = &line_pawns[i][j];
        for(int k=0;k<lp->count;k++)
            ASSERT(data[POS_XY(j, i ? lp->pos[k] : 7-lp->pos[k])] == (i?WP:BP));
        int n = 0;
        for(int k=0;k<8;k++)
        if(data[POS_XY(j, k)] == (i?WP:BP))
            n++;
        ASSERT(n == lp->count);
    }
}
