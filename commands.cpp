/***************************************************************************
                          commandparse.cpp  -  description
                             -------------------
    begin                : sab nov 2 2002
    copyright            : (C) 2002-2005 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "engine.h"
#include "commands.h"
#include "utils.h"
#include "search_gui.h"
#include <stdarg.h>
#include <signal.h>
#include <string.h>
#include <limits.h>

void Engine::Cmd::run(int argc, char *argv[])
{
    (Engine::eng()->*m_func)(argc, argv);
}

Engine::Variable::Variable(int (Engine::*ivar), int def_val, const char *d)
    : m_type(Integer)
    , m_desc(d)
    , m_int_var(ivar)
    , m_range_min(INT_MIN)
    , m_range_max(INT_MAX)
{
    Engine::eng()->*m_int_var = def_val;
}

Engine::Variable::Variable(int (Engine::*ivar), int def_val, int min, int max, const char *d)
    : m_type(Integer)
    , m_desc(d)
    , m_int_var(ivar)
    , m_range_min(min)
    , m_range_max(max)
{
    Engine::eng()->*m_int_var = def_val;
}

Engine::Variable::Variable(char* (Engine::*svar), const char* def_val, const char *d)
    : m_type(String)
    , m_desc(d)
    , m_string_var(svar)
{
    Engine::eng()->*m_string_var = strdup(def_val);
}

Engine::Variable::Variable(VarSetFunc set, VarGetFunc get, const char *d)
    : m_type(String)
    , m_desc(d)
    , m_custom_data(NULL)
    , m_custom_set(set)
    , m_custom_get(get)
{
}

Engine::Variable::Variable(VarSetFunc set, VarGetFunc get, void* data, const char *d)
    : m_type(String)
    , m_desc(d)
    , m_custom_data(data)
    , m_custom_set(set)
    , m_custom_get(get)
{
}

void Engine::Variable::from_string(const char* s)
{
    switch(m_type)
    {
        case Integer:
        {
            int v = atoi(s);
            if(v < m_range_min || v > m_range_max)
                printf("Error! value %d out of range (%d-%d)\n", v, m_range_min, m_range_max);
            else
                Engine::eng()->*m_int_var = v;
            break;
        }

        case String:
            if(Engine::eng()->*m_string_var)
                free(Engine::eng()->*m_string_var);
            Engine::eng()->*m_string_var = strdup(s);
            break;

        case Custom:
            (Engine::eng()->*m_custom_set)(m_custom_data, s);
            break;

        default:
            break;
    }
}

const char* Engine::Variable::to_string(char* buf64)
{
    switch(m_type)
    {
        case Integer:
            snprintf(buf64, 64, "%d", Engine::eng()->*m_int_var);
            break;

        case String:
            strncpy(buf64, Engine::eng()->*m_string_var, 64);
            break;

        case Custom:
            (Engine::eng()->*m_custom_get)(m_custom_data, buf64);
            break;

        default:
            buf64[0] = '\0';
            break;
    }

    return buf64;
}

static void print_description(int indent, const char* desc)
{
    int num;
    char** tokenized = tokenize(desc ? desc : "<No description>", "\n", &num, false);

    for(int i=0;i<num;i++)
    {
        for(int q=0;q<indent;q++)
            putchar(' ');
        puts(tokenized[i]);
    }
    putchar('\n');
    free(tokenized);
}

void Engine::cmd_quit(int argc, char *argv[])
{
    exit(0);
}

void Engine::cmd_ping(int argc, char *argv[])
{
    printf("pong %s\n",argv[0]);
}

void Engine::cmd_bench(int argc, char *argv[])
{
}

void Engine::cmd_perft(int argc, char *argv[])
{
    int t = current_time();
    uint64_t p = perft( atoi(argv[0]) );
    printf("%lld (time %.02f)\n", (long long)p,  (current_time()-t)*0.01 );
}

void Engine::cmd_perftall(int argc, char *argv[])
{
    int lev = atoi(argv[0]);
    Move mv_stack[200];
    int num;
    num = board.find_moves(mv_stack);

    printf("---------\n");
    for(int i=0;i<num;i++)
    {
        int p;
        board.do_move(mv_stack[i], save_buf[save_buf_num++]);
        p = perft(lev);
        board.undo_move(mv_stack[i], save_buf[--save_buf_num]);
        printf("%s: %d\n", board.move_to_alg(MoveStr().data(), mv_stack[i]), p);
    }
    printf("---------\n");
}

void Engine::cmd_sd(int argc, char *argv[])
{
    set_depth( atoi(argv[0]) );
}

void Engine::cmd_st(int argc, char *argv[])
{
    set_time_fixed( atoi(argv[0]) );
}

void Engine::cmd_sn(int argc, char *argv[])
{
    set_max_nodes( atoi(argv[0]) );
}

void Engine::cmd_otim(int argc, char *argv[])
{
    set_other_clock( atoi(argv[0]) );
}

void Engine::cmd_time(int argc, char *argv[])
{
    set_my_clock( atoi(argv[0]) );
}

void Engine::cmd_level(int argc, char *argv[])
{
    int basem,bases=0;
    sscanf(argv[1],"%d:%d",&basem,&bases);

    set_time_control( atoi(argv[0]) ,basem*60+bases, atoi(argv[2]) );
}

void Engine::cmd_help(int argc, char *argv[])
{
    for(CmdSet::iterator it = commands.begin(); it != commands.end(); ++it)
    {
        if(!it->second.max())
            printf("  %s\n", it->first.c_str() );
        else if(it->second.min() == it->second.max())
            printf("  %s (%d %s)\n", it->first.c_str(), it->second.min(), it->second.min()==1?"arg":"args" );
        else
            printf("  %s (%d-%d args)\n", it->first.c_str(), it->second.min(), it->second.max() );

        print_description(4, it->second.desc());
    }
}

void Engine::cmd_force(int argc, char *argv[])
{
    force();
}

void Engine::cmd_undo(int argc, char *argv[])
{
    undo();
    if(!io_xboard)
        print_board();
}

void Engine::cmd_remove(int argc, char *argv[])
{
    retract_move();
    if(!io_xboard)
        print_board();
}

void Engine::cmd_new(int argc, char *argv[])
{
    new_game();
    if(!io_xboard)
        print_board();
}

void Engine::cmd_white(int argc, char *argv[])
{
    board.color_to_move = WHITE;
    board.other_color = BLACK;
    start(BLACK);
}

void Engine::cmd_black(int argc, char *argv[])
{
    board.color_to_move = BLACK;
    board.other_color = WHITE;
    start(WHITE);
}

void Engine::cmd_go(int argc, char *argv[])
{
    start();
}

void Engine::cmd_setboard(int argc, char *argv[])
{
    read_board(argv[0], argv[1], argv[2], argv[3], atoi(argv[4]), atoi(argv[5]) );
    if(!io_xboard)
        print_board();
}

void Engine::cmd_edit(int argc, char *argv[])
{
    //output("Unimplemented!\n");
    char str[256];
    while(1)
    {
        if(!fgets(str, 256, stdin))
        exit(0);

    if(str[0]=='.')
        break;
    }
}

void Engine::cmd_load(int argc, char *argv[])
{
    FILE* f = fopen(argv[0],"r");
    if(f)
    {
        read_board(f);
        fclose(f);
        if(!io_xboard)
            print_board();
    }
    else
        output("Error, could not open \"%s\" for reading.\n", argv[0]);
}

void Engine::cmd_load_pgn(int argc, char *argv[])
{
    FILE* f = fopen(argv[0],"r");
    if(!f)
        output("Error, could not open \"%s\" for reading.\n", argv[0]);

    st_computer_color = 0;
    load_pgn(f);
    fclose(f);
    if(!io_xboard)
        print_board();
}

void Engine::cmd_epdtest(int argc, char *argv[])
{
    run_epd_prog(argv[0], atoi(argv[1]), argv[2], argc==4 && !strncmp(argv[3], "quiet", strlen(argv[3])));
}

void Engine::cmd_challenge(int argc, char *argv[])
{
    challenge(argv[0], atoi(argv[1]), argv[2]);
}

void Engine::cmd_create_book(int argc, char *argv[])
{
    create_book(argc, argv);
}

void Engine::cmd_open_book(int argc, char *argv[])
{
    open_book(argv[0]);
}

void Engine::cmd_close_book(int argc, char *argv[])
{
    close_book();
}

void Engine::cmd_open_lines(int argc, char *argv[])
{
    open_lines(argv[0]);
}

void Engine::cmd_close_lines(int argc, char *argv[])
{
    close_lines();
}

void Engine::cmd_open_eco(int argc, char *argv[])
{
    open_openings(argv[0]);
}

void Engine::cmd_close_eco(int argc, char *argv[])
{
    close_openings();
}

void Engine::cmd_save(int argc, char *argv[])
{
    FILE* f = fopen(argv[0],"w");
    if(f)
    {
        write_board(f);
        fclose(f);
    }
    else
        output("Error, could not open \"%s\" for writing.\n", argv[0]);

}

void Engine::cmd_test(int argc, char *argv[])
{
    printf("test");
    for(int i=0;i< argc;i++)
        printf(" \"%s\"", argv[i]);
    printf("\n");
}

void Engine::cmd_protover(int argc, char *argv[])
{
    static const char* features[] = {
            "san=1",
            "ping=1",
            "setboard=1",
            "sigint=0",
            "sigterm=0",
            "analyze=1",
            "draw=0",
            "colors=0",
            "variants=\"normal\"",
            "myname=\"RattateChess 1.1 Artin\"",
            "done=1"
    };
    for(unsigned int i=0; i<sizeof(features)/sizeof(const char*); i++)
        output("feature %s\n", features[i]);
}

void Engine::cmd_accepted(int argc, char *argv[])
{
    if(0 == strcmp("san",argv[0]))
        io_san = true;
}

void Engine::cmd_rejected(int argc, char *argv[])
{

}

void Engine::cmd_xboard(int argc, char *argv[])
{
    io_xboard = true;
    signal(SIGINT, SIG_IGN);
}

void Engine::cmd_analyze(int argc, char *argv[])
{
    eng_status = ANALYZING;
    if(thinking)
        longjmp(back, 1);
}

void Engine::cmd_exit(int argc, char *argv[])
{
    eng_status = FORCING;
    if(thinking)
        longjmp(back, 1);
}

void Engine::cmd_easy(int argc, char *argv[])
{
    ponder = false;
}

void Engine::cmd_hard(int argc, char *argv[])
{
    ponder = true;
}

void Engine::cmd_post(int argc, char *argv[])
{
    post = true;
}

void Engine::cmd_nopost(int argc, char *argv[])
{
    post = false;
}

void Engine::cmd_result(int argc, char *argv[])
{
    if(!strcmp(argv[0], "1-0"))
        status = _10;
    else if(!strcmp(argv[0], "0-1"))
        status = _01;
    else if(!strcmp(argv[0], "1/2-1/2"))
        status = _12;
    else
        output("Unknown result command!\n");

    if(st_computer_color)
    {
        FILE *f = fopen("ratta.games", "a");
        if(f)
        {
            char all_res[2048];
            all_res[0] = '\0';
            for(int i=0;i<argc;i++)
            {
                strcat(all_res, argv[i]);
                if(i!=argc-1)
                    strcat(all_res, " " );
            }
            save_pgn(f, argv[0], all_res);
            fclose(f);
        }
    }
}

void Engine::cmd_DOT(int argc, char *argv[])
{
    print_stat();
}

void Engine::cmd_QUESTION(int argc, char *argv[])
{
    move_now();
}

void Engine::cmd_name(int argc, char *argv[])
{
    strncpy(opponent, argv[0], 256);
    opponent[255] = '\0';
}

void Engine::cmd_rating(int argc, char *argv[])
{
    w_rating = atoi(argv[0]);
    b_rating = atoi(argv[1]);
}

void Engine::cmd__gon(int argc, char *argv[])
{
    if(search_gui)
        output("Gui already on!\n");
    else
        search_gui = new SearchGui(argc, argv);
}

void Engine::cmd__goff(int argc, char *argv[])
{
    if(!search_gui)
        output("Gui already off!\n");
    else
    {
        delete search_gui;
        search_gui = NULL;
    }
}

void Engine::cmd__check(int argc, char *argv[])
{
    Move m = board.move_from_string(argv[0]);
    if(!m.valid())
    {
        output("Not a legal move\n");
        return;
    }

    board.find_other_pins();
    if(board.move_is_check(m))
        output("Yes it is a check\n");
    else
        output("Not a check\n");
}

void Engine::cmd__attacks(int argc, char *argv[])
{
    uint8_t col = argv[1][0] == 'w' ? WHITE : BLACK;
    uint8_t pos = POS_XY(argv[0][0]-'a', argv[0][1]-'1' );
    Board::AttackList al[12];
    int alnum = board.list_attackers(pos, col, col == board.color_to_move ?
                        board.pins : board.oth_pins, al);
    for(int i=0;i<alnum;i++)
    {
        static char nam[] = { 0, 'r', 'b', 'q', 'n', 'p', 'k' };
        for(int j=0;j<al[i].count;j++)
            printf("%c ", nam[al[i].piece[j]]);
        printf("\n");
    }
}

void Engine::cmd__shat(int argc, char *argv[])
{
#if TRACK_ATTACKS
    board.print_attacks();
#else
    output("Unsupported.\n");
#endif
}

void Engine::cmd__see(int argc, char *argv[])
{
    Move m = board.move_from_string(argv[0]);
    if(!m.valid())
    {
        output("Not a legal move\n");
        return;
    }

    output("SEE val: %d\n", board.move_see_val(m));
}

void Engine::cmd__find_moves(int argc, char *argv[])
{
    Move mv[200];
    int n = board.find_moves(mv);

    for(int i=0;i<n;i++)
        output(" - %s\n", board.move_to_alg(MoveStr().data(), mv[i]) );
}

void Engine::cmd__gen_hash(int argc, char *argv[])
{
    Board::init_hash_keys( argv[0] );
}

void Engine::cmd_set(int argc, char *argv[])
{
    std::map<std::string, Variable>::iterator it = variables.find(argv[0]);
    if(it == variables.end())
        printf("  -> unknown variable '%s'!\n", argv[0]);
    else
    {
        it->second.from_string(argv[1]);
        printf("  %s -> %s\n", it->second.to_string(TmpStr().data()), it->first.c_str() );
    }
}

void Engine::cmd_var(int argc, char *argv[])
{
    if(argc == 0)
    for(VariableSet::iterator it = variables.begin(); it != variables.end(); ++it)
        printf("  %s = %s\n", it->first.c_str(), it->second.to_string(TmpStr().data()) );
    else
    for(int i=0;i<argc;i++)
    {
        std::map<std::string, Variable>::iterator it = variables.find(argv[i]);
        if(it == variables.end())
            printf("  -> unknown variable '%s'!\n", argv[i]);
        else
            printf("  %s = %s\n", it->first.c_str(), it->second.to_string(TmpStr().data()) );
    }
}

void Engine::cmd_varhelp(int argc, char *argv[])
{
    for(VariableSet::iterator it = variables.begin(); it != variables.end(); ++it)
    {
        printf("  %s = %s\n", it->first.c_str(), it->second.to_string(TmpStr().data()) );
        print_description(4, it->second.desc());
    }
}

void Engine::register_commands()
{
    commands["_see"] = Cmd( &Engine::cmd__see, 1,
                            "_see [move]\n"
                            "Calculates the SEE value of a move\n");
    commands["_shat"] = Cmd( &Engine::cmd__shat,
                            "Shows all attacks\n" );
    commands["_attacks"] = Cmd( &Engine::cmd__attacks, 2,
                            "_attacks [w|b] [square|a1|a2|...]\n"
                            "Shows all attackers of a square from a color\n" );
    commands["_check"] = Cmd( &Engine::cmd__check, 1 );
    commands["_find_moves"] = Cmd( &Engine::cmd__find_moves );
    commands["_gen_hash"] = Cmd( &Engine::cmd__gen_hash );
    commands["_gon"] = Cmd( &Engine::cmd__gon );
    commands["_goff"] = Cmd( &Engine::cmd__goff );
    //commands["exit"] = Cmd( &Engine::cmd_quit );
    commands["quit"] = Cmd( &Engine::cmd_quit );
    commands["ping"] = Cmd( &Engine::cmd_ping, 1 );
    commands["bench"] = Cmd( &Engine::cmd_bench );
    commands["perft"] = Cmd( &Engine::cmd_perft, 1 );
    commands["perftall"] = Cmd( &Engine::cmd_perftall, 1 );
    commands["sd"] = Cmd( &Engine::cmd_sd, 1 );
    commands["st"] = Cmd( &Engine::cmd_st, 1 );
    commands["sn"] = Cmd( &Engine::cmd_sn, 1 );
    commands["otim"] = Cmd( &Engine::cmd_otim, 1 );
    commands["time"] = Cmd( &Engine::cmd_time, 1 );
    commands["level"] = Cmd( &Engine::cmd_level, 3 );
    commands["help"] = Cmd( &Engine::cmd_help,
                            "Printf this help\n" );
    commands["force"] = Cmd( &Engine::cmd_force,
                            "Puts the engine in 'force' mode (ie edit game)\n" );
    commands["undo"] = Cmd( &Engine::cmd_undo );
    commands["remove"] = Cmd( &Engine::cmd_remove );
    commands["new"] = Cmd( &Engine::cmd_new );
    commands["white"] = Cmd( &Engine::cmd_white );
    commands["black"] = Cmd( &Engine::cmd_black );
    commands["go"] = Cmd( &Engine::cmd_go );
    commands["setboard"] = Cmd( &Engine::cmd_setboard, 6,
                            "Sets a FEN string as the current position\n" );
    commands["edit"] = Cmd( &Engine::cmd_edit,
                            "Puts the engine in (old style) position editing mode\n" );
    commands["load"] = Cmd( &Engine::cmd_load, 1 );
    commands["load_pgn"] = Cmd( &Engine::cmd_load_pgn, 1 );
    commands["epdtest"] = Cmd( &Engine::cmd_epdtest, 3, 4,
                            "Runs an epd test with a chess program and a given time out for each\n"
                            "problem. It requires 3 arguments: the first must be an xboard program\n"
                            "path or 'self', the second the time to think in millseconds, and the\n"
                            "third is the file containing the test positions\n");
    commands["challenge"] = Cmd( &Engine::cmd_challenge, 3 );
    commands["create_book"] = Cmd( &Engine::cmd_create_book, 1, 2,
                            "Creates a book file using as argument the path of a PGN file with\n"
                            "a database of games, and possibly the path of a file with ECO codes.\n"
                            "See also the 'vars' command and the variables named book_creation_*,\n"
                            "that you can use to change many options\n" );
    commands["open_book"] = Cmd( &Engine::cmd_open_book, 1,
                            "Loads a book file\n" );
    commands["close_book"] = Cmd( &Engine::cmd_close_book,
                            "Closes the book (and make the engine use no book)\n" );
    commands["open_lines"] = Cmd( &Engine::cmd_open_lines, 1,
                            "Loads a lines file (where you can select the openings to play)\n" );
    commands["close_lines"] = Cmd( &Engine::cmd_close_lines,
                            "Closes the lines file (and make the engine use no lines)\n" );
    commands["open_eco"] = Cmd( &Engine::cmd_open_eco, 1,
                            "Loads a eco file\n" );
    commands["close_eco"] = Cmd( &Engine::cmd_close_eco,
                            "Closes the eco (and make the engine use no eco)\n" );
    commands["save"] = Cmd( &Engine::cmd_save, 1 );
    commands["test"] = Cmd( &Engine::cmd_test );
    commands["protover"] = Cmd( &Engine::cmd_protover, 1 );
    commands["accepted"] = Cmd( &Engine::cmd_accepted, 1 );
    commands["rejected"] = Cmd( &Engine::cmd_rejected, 1 );
    commands["xboard"] = Cmd( &Engine::cmd_xboard );
    commands["winboard"] = Cmd( &Engine::cmd_xboard );
    commands["analyze"] = Cmd( &Engine::cmd_analyze );
    commands["exit"] = Cmd( &Engine::cmd_exit );
    commands["easy"] = Cmd( &Engine::cmd_easy );
    commands["hard"] = Cmd( &Engine::cmd_hard );
    commands["post"] = Cmd( &Engine::cmd_post,
                            "Enables printing the thinking\n" );
    commands["nopost"] = Cmd( &Engine::cmd_nopost,
                            "Disables printing the thinking\n");
    commands["name"] = Cmd( &Engine::cmd_name, 1 );
    commands["rating"] = Cmd( &Engine::cmd_rating, 2 );
    commands["result"] = Cmd( &Engine::cmd_result, 1, 999 );
    commands["var"] = Cmd( &Engine::cmd_var, 0, 999,
                            "Prints variable[s] values\n" );
    commands["set"] = Cmd( &Engine::cmd_set, 2,
                            "Set variable value\n" );
    commands["varhelp"] = Cmd( &Engine::cmd_varhelp,
                            "Prints variable values and with help information\n" );
    commands["."] = Cmd( &Engine::cmd_DOT,
                            "Print statistics (during analysis)\n" );
    commands["?"] = Cmd( &Engine::cmd_QUESTION,
                            "Move immediatly\n" );
}

void Engine::register_variables()
{
    variables["book_creation_max_depth"] = Variable( &Engine::v_book_creation_max_depth, 28, 1, 50,
                            "Maximum depth (in ply) for selecting positions to include in the book\n");
    variables["book_creation_max_alternatives"] = Variable( &Engine::v_book_creation_max_alternatives, 7, 1, INT_MAX,
                            "Maximum number of possible moves to store in the book for each position\n");
    variables["book_creation_min_games"] = Variable( &Engine::v_book_creation_min_games, 6, 1, INT_MAX,
                            "Minimum number of games in which a position must appear to be included\n" );
    variables["book_creation_min_probability"] = Variable( &Engine::v_book_creation_min_probability, 10, 0, 1000,
                            "Minimum probability that an possible move must have to be put in the book\n");
    variables["book_creation_weigh_win"] = Variable( &Engine::v_book_creation_weigh_win, 3, 0, INT_MAX,
                            "Weight for a move played in a won game (to make more likely winning moves)\n");
    variables["book_creation_weigh_draw"] = Variable( &Engine::v_book_creation_weigh_draw, 2, 0, INT_MAX,
                            "Weight for a move played in a drawn game (to make more likely winning moves)\n");
    variables["book_creation_weigh_lose"] = Variable( &Engine::v_book_creation_weigh_lose, 1, 0, INT_MAX,
                            "Weight for a move played in a lost game (to make more likely winning moves)\n");

    variables["search_null_reduction"] = Variable( &Engine::v_search_null_reduction, 300, 200, 400,
                            "Reduction for null move (in ply*100)\n");
    variables["search_threat_extension"] = Variable( &Engine::v_search_threat_extension, 100, 40, 120,
                            "Reduction for null move (in ply*100)\n");
    variables["search_threat_threshold"] = Variable( &Engine::v_search_threat_threshold, 340, 150, 720,
                            "Reduction for null move (in ply*100)\n");

    variables["eval_draw"] = Variable( &Engine::v_eval_draw, -20, 0, -100,
                            "Value of a position drawn by repetition, 50 moves rule or insufficient material\n");
}
