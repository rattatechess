/***************************************************************************
                          check.cpp  -  description
                             -------------------
    begin                : Mon Sep 19 2005
    copyright            : (C) 2005 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "board.h"

void print_move(const Move* m)
{
    printf("%c%c%c%c\n", X(m->from)+'a', Y(m->from)+'1',
           X(m->to)+'a', Y(m->to)+'1');

}
