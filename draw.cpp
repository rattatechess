/***************************************************************************
                      draw.cpp  -  Draw recognition
                             -------------------
    begin                : Sun Sep 28 2007
    copyright            : (C) 2007 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "engine.h"
#include "search.h"

/* check if the position can be considered draw for 50mvs rule or repetition */
bool Engine::check_draw()
{
    if(board.fifty >= 100) /* 50mvs rule */
        return true;

    return board.insufficient_material() || check_repetition(2);
}

bool Engine::check_repetition(int nfold)
{
    /* for the latest reversible moves check if the hash value
        (for this player) is the same         */
    int first_candidate = MAX(save_buf_num - board.fifty, 0);

    /* -4 because it is impossible that the previous position is
       equal to the  current one :) */
    for(int i=save_buf_num-4;i>=first_candidate;i-=2)
    {
        if(board.hash == save_buf[i].hash_key)
        {
            nfold--;
            if(nfold <= 1)
                return true;
        }
    }

    return false;
}

/* check if the position can be considered draw for 50mvs rule or repetition */
bool SearchRoot::check_draw(int curr_ply)
{
    if(board.fifty >= 100) /* 50mvs rule */
        return true;

    return board.insufficient_material() || check_repetition(curr_ply, 2);
}

bool SearchRoot::check_repetition(int curr_ply, int nfold)
{
    int first_candidate1 = MAX(curr_ply - board.fifty, 0);

    /* -4 because it is impossible that the previous position is
       equal to the  current one :) */
    for(int i=curr_ply-4;i>=first_candidate1;i-=2)
    {
        if(board.hash == stack[i].save_buf.hash_key)
        {
            nfold--;
            if(nfold <= 1)
                return true;
        }
    }

    int first_candidate2 = MAX(engine->save_buf_num + curr_ply - (board.fifty & ~1), 0);

    /* -4 because it is impossible that the previous position is
       equal to the  current one :) */
    for(int i=first_candidate2; i<engine->save_buf_num; i+=2)
    {
        if(board.hash == engine->save_buf[i].hash_key)
        {
            nfold--;
            if(nfold <= 1)
                return true;
        }
    }

    return false;
}

bool Board::insufficient_material()
{
    for(int i=0;i<2;i++)
    {
        int mt = (i ? 5 : -1);

        if(mat_tracking[PAWN+mt].count > 0
          || mat_tracking[QUEEN+mt].count > 0
          || mat_tracking[ROOK+mt].count > 0)
          return false;

        /* ok, unless the 2 bishop are on squares of the same color :) */
        if(mat_tracking[BISHOP+mt].count >= 2)
          return false;

        if(mat_tracking[KNIGHT+mt].count >= 1 &&
            mat_tracking[BISHOP+mt].count >= 1)
          return false;

        /* 3 knights can checkmate :) */
        if(mat_tracking[KNIGHT+mt].count >= 3)
          return false;
    }
    return true;
}
