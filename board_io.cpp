/***************************************************************************
                          board_io.cpp  -  description
                             -------------------
    begin                : Wed Mar 13 2002
    copyright            : (C) 2002-2005 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "board.h"
#include "engine.h"
#include <string.h>

/* print a fancy colored board */
void Board::print_board()
{
    static const char out_wpiece[] = "\e[1;33m";
    static const char out_bpiece[] = "\e[1;32m";
    static const char out_wsquare[]= "\e[1;47m";
    static const char out_bsquare[]= "\e[1;43m";

    find_check_and_pins();
    find_other_pins();

    for(int j=7;j>=0;j--)
    {
        for(int i=0;i<8;i++)
        {
        unsigned char piece = data[POS_XY(i, j)];

        printf("%s%s %c ",
                    piece == STONE ? "\e[1;40m" :
                    oth_pins[POS_XY(i, j)] ? "\e[1;44m" :
                    (CAN_MOVE(pins[POS_XY(i, j)]) ? ((j+i)&1 ? out_wsquare : out_bsquare):
                        (IS_OF_COLOR(piece,color_to_move)?"\e[1;45m":"\e[1;41m")),
                    COLOR_OF(piece)==WHITE ? out_wpiece : out_bpiece,
                    piece==STONE?' ':piecename[PIECE_OF(piece)]);
        }
        printf("\e[0m\n");
    }


    printf("#on move: %s     ", color_to_move==WHITE?"white":"black");
    printf("castling: %s%s%s%s     ",
           castle_passing_mask&WCANCASTLEKS ? "K" : " ",
           castle_passing_mask&WCANCASTLEQS ? "Q" : " ",
           castle_passing_mask&BCANCASTLEKS ? "k" : " ",
           castle_passing_mask&BCANCASTLEQS ? "q" : " " );
    printf("en-passant: %d\n", castle_passing_mask&NOT_PASSING?
                            -1: castle_passing_mask&0x07);
    printf("#static value....: %d\n", (int)evaluate(WHITE, -INF, INF) );
    printf("#hash key........: %s\n", hash.to_string(TmpStr().data()) );
    printf("#fen.............: %s\n", to_fen(BigStr().data()) );

    //print_attacks();
#if 0
    printf("pawn positions:\n");
    int max = 0;
    for(int i=0;i<2;i++)
        for(int w=0;w<8;w++)
            max = MAX(max, line_pawns[i][w].count);

    for(int q=max-1;q>=0;q--)
    {
        for(int i=0;i<2;i++)
        {
            for(int w=0;w<8;w++)
            {
                if(q<line_pawns[i][w].count)
                    printf("%d", line_pawns[i][w].pos[q]);
                else
                    printf(" ");
            }
            printf(" ");
        }
        printf("\n");
    }
    printf("-------- --------\n");

    static char name[] = "brqnpkRBQNPK";
    printf("piece positions:\n");
    for(int i=0;i<12;i++)
    {
        printf("%c:", name[i]);
        for(int j=0;j<mat_tracking[i].count;j++)
            printf(" %c%c",
                   'a' + X(mat_tracking[i].pos[j]),
                   '1' + Y(mat_tracking[i].pos[j]));
        printf("\n");
    }
#endif
}

char* Board::to_fen(char* str)
{
    char *retv = str;
    int space_counter = 0;

    for(int y=7; y>=0; y--)
    {
        for(int x=0; x<8; x++)
        {
            uint8_t piece = data[POS_XY(x,y)];

            if(piece == 0)
            {
                space_counter++;
                continue;
            }

            if(space_counter > 0)
            {
                str += sprintf(str, "%d", space_counter);
                space_counter = 0;
            }

            char p = (piece==STONE) ? '#' : Board::piecename[PIECE_OF(piece)];
            if(COLOR_OF(piece) == BLACK)
                p ^= 0x20;
            str += sprintf(str, "%c", p);
        }

        if(space_counter > 0)
        {
            str += sprintf(str, "%d", space_counter);
            space_counter = 0;
        }

        if(y != 0)
            str += sprintf(str,"/");
    }

    str += sprintf(str, " %c ", (color_to_move==WHITE) ? 'w':'b');

    if(castle_passing_mask & WCANCASTLEKS)
        str += sprintf(str, "K");
    if(castle_passing_mask & WCANCASTLEQS)
        str += sprintf(str, "Q");
    if(castle_passing_mask & BCANCASTLEKS)
        str += sprintf(str, "k");
    if(castle_passing_mask & BCANCASTLEQS)
        str += sprintf(str, "q");
    if(!(castle_passing_mask & 0xf0))
        str += sprintf(str,"-");

    if(!(castle_passing_mask&NOT_PASSING))
        str += sprintf(str, " %c%c",
                            (castle_passing_mask&0x07)+'a',
                            (color_to_move==WHITE)?'6':'3' );
    else
        str += sprintf(str," -");

    str += sprintf(str," %d %d",fifty,num_moves+1);

    return retv;
}


void Board::read_board(char* brd, char* color, char* castle,
                       char* passing, int moves_to_draw, int num_mv)
{
    char* pars = brd;
    int i = 0;
    int currl = 0x70;

    fifty = moves_to_draw;

    zero_a88(data);

    while(pars[0]!='\0')
    {
        char cc=*(pars++);
        if(cc>='1' && cc<='8')
        {
            currl += cc-'0';
            continue;
        }
        if(cc=='/')
        {
            i++;
            currl = 0x70 - i*0x10;
            continue;
        }
        if(cc=='#')
        {
            data[currl++] = STONE;
            continue;
        }
        for(int i=1;i<7;i++)
        {
            if(piecename[i]==cc)
            {
                if(i==KING)
                    king_pos[1] = currl;
                data[currl++]   = i|WHITE;
                continue;
            }
            else if((piecename[i]^0x20)==cc)
            {
                if(i==KING)
                    king_pos[0] = currl;
                data[currl++]   = i|BLACK;
                continue;
            }
        }
    }
    color_to_move = (*color=='w')?WHITE:BLACK;
    other_color   = OTHER_COLOR(color_to_move);

    pars = castle;
    castle_passing_mask = 0;
    while(pars[0]!='\0')
    {
        switch(*pars)
        {
            case 'K':
                castle_passing_mask|=WCANCASTLEKS;
                break;
            case 'Q':
                castle_passing_mask|=WCANCASTLEQS;
                break;
            case 'k':
                castle_passing_mask|=BCANCASTLEKS;
                break;
            case 'q':
                castle_passing_mask|=BCANCASTLEQS;
                break;
        }
        pars++;
    }
    if(passing[0]!='-')
        castle_passing_mask |= (passing[0]-'a');
    else
        castle_passing_mask |= NOT_PASSING;

    num_moves = MAX(num_mv-1, 0);

    under_check=0xff;

    recalc_line_pawns();
    recalc_mat_tracking();
    recalc_hash();
#if TRACK_ATTACKS
    recalc_attacks();
#endif //TRACK_ATTACKS
}

void Board::read_board(char* str)
{
    char brd[81];
    char color[2],castle[5],passing[3];
    int moves_to_draw;
    int numm;

    sscanf(str,"%s%s%s%s%d%d",
        brd,color,castle,passing,&moves_to_draw,&numm);

    read_board(brd,color,castle,passing,moves_to_draw,numm);
}
