/***************************************************************************
                          static.cpp  -  description
                             -------------------
    begin                : Wed Mar 13 2002
    copyright            : (C) 2002-2005 by Maurizio Monge
    email                : monge@linuz.sns.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "board.h"
#include "engine.h"
#include <stdio.h>

/*long long* Engine::hashvalues = NULL;
HASH* Engine::hashtable = NULL;
int Engine::hashmask = 0;*/

KnightMove* Board::knightmoves = NULL;
uint8_t Board::kingmoves[] = { LEFT, RIGHT, DOWN, UP,
                               (uint8_t)(LEFT+DOWN), (uint8_t)(RIGHT+UP), (uint8_t)(RIGHT+DOWN), (uint8_t)(LEFT+UP) };
uint8_t Board::bishmoves[] = { (uint8_t)(RIGHT+UP), (uint8_t)(RIGHT+DOWN), (uint8_t)(LEFT+DOWN), (uint8_t)(LEFT+UP) };
uint8_t Board::rookmoves[] = { LEFT, RIGHT, DOWN, UP };
uint8_t Board::up_dir[] = { DOWN, UP };
HashKey  Board::hash_key_toggle( 0x986bcb9c798532a7ULL, 0xa5949da9 );

char Board::piecename[] = { ' ', 'R', 'B', 'Q', 'N', 'P', 'K'};

int16_t Board::simple_values[] = { 0, 5, 3, 10, 3, 1, INF };

uint8_t Board::b_start[] =
{
    WR,WN,WB,WQ,WK,WB,WN,WR,SENDL,
    WP,WP,WP,WP,WP,WP,WP,WP,SENDL,
    __,__,__,__,__,__,__,__,SENDL,
    __,__,__,__,__,__,__,__,SENDL,
    __,__,__,__,__,__,__,__,SENDL,
    __,__,__,__,__,__,__,__,SENDL,
    BP,BP,BP,BP,BP,BP,BP,BP,SENDL,
    BR,BN,BB,BQ,BK,BB,BN,BR,SENDL
};

uint8_t Board::b_castle_adj[] =
{
    0xd0,0xf0,0xf0,0xf0,0xc0,0xf0,0xf0,0xe0,ENDL,
    0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,ENDL,
    0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,ENDL,
    0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,ENDL,
    0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,ENDL,
    0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,ENDL,
    0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,ENDL,
    0x70,0xf0,0xf0,0xf0,0x30,0xf0,0xf0,0xb0,ENDL
};

//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------

